﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using EventosCatolicos.Models;

namespace EventosCatolicos.Controllers
{
    public class CategoriasTratamientoController : Controller
    {
        private readonly ApplicationDbContext _db = new ApplicationDbContext();

        // GET: CategoriasTratamiento
        [Authorize(Roles = "Ver CategoriaTratamiento,IT")]
        public ActionResult Index()
        {
            return View(_db.CategoriaTratamientos.ToList());
        }

        // GET: CategoriasTratamiento/Details/5
        [Authorize(Roles = "Ver CategoriaTratamiento,IT")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CategoriaTratamiento categoriaTratamiento = _db.CategoriaTratamientos.Find(id);
            if (categoriaTratamiento == null)
            {
                return HttpNotFound();
            }
            return View(categoriaTratamiento);
        }

        // GET: CategoriasTratamiento/Create
        [Authorize(Roles = "Ver CategoriaTratamiento,IT")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: CategoriasTratamiento/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrar CategoriaTratamiento,IT")]
        public ActionResult Create([Bind(Include = "Id,Nombre")] CategoriaTratamiento categoriaTratamiento)
        {
            if (ModelState.IsValid)
            {
                _db.CategoriaTratamientos.Add(categoriaTratamiento);
                _db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(categoriaTratamiento);
        }

        // GET: CategoriasTratamiento/Edit/5
        [Authorize(Roles = "Administrar CategoriaTratamiento,IT")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CategoriaTratamiento categoriaTratamiento = _db.CategoriaTratamientos.Find(id);
            if (categoriaTratamiento == null)
            {
                return HttpNotFound();
            }
            return View(categoriaTratamiento);
        }

        // POST: CategoriasTratamiento/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrar CategoriaTratamiento,IT")]
        public ActionResult Edit([Bind(Include = "Id,Nombre")] CategoriaTratamiento categoriaTratamiento)
        {
            if (ModelState.IsValid)
            {
                _db.Entry(categoriaTratamiento).State = EntityState.Modified;
                _db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(categoriaTratamiento);
        }

        // GET: CategoriasTratamiento/Delete/5
        [Authorize(Roles = "Eliminar CategoriaTratamiento,IT")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CategoriaTratamiento categoriaTratamiento = _db.CategoriaTratamientos.Find(id);
            if (categoriaTratamiento == null)
            {
                return HttpNotFound();
            }
            return View(categoriaTratamiento);
        }

        // POST: CategoriasTratamiento/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Eliminar CategoriaTratamiento,IT")]
        public ActionResult DeleteConfirmed(int id)
        {
            CategoriaTratamiento categoriaTratamiento = _db.CategoriaTratamientos.Find(id);
            _db.CategoriaTratamientos.Remove(categoriaTratamiento);
            _db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
