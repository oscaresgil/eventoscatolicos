﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using EventosCatolicos.Models;
using EventosCatolicos.ViewModels;
using Microsoft.AspNet.Identity.Owin;

namespace EventosCatolicos.Controllers
{
    [Authorize(Roles = "IT,Admin")]
    public class GroupsAdminController : Controller
    {
        private readonly ApplicationDbContext _db =
            System.Web.HttpContext.Current.GetOwinContext().Get<ApplicationDbContext>();

        private ApplicationGroupManager _groupManager;
        public ApplicationGroupManager GroupManager
        {
            get
            {
                return _groupManager ?? HttpContext.GetOwinContext().Get<ApplicationGroupManager>();
            }
            private set
            {
                if (value == null) throw new ArgumentNullException("value");
                _groupManager = value;
            }
        }

        private ApplicationRoleManager _roleManager;
        public ApplicationRoleManager RoleManager
        {
            get
            {
                return _roleManager ?? HttpContext.GetOwinContext()
                    .Get<ApplicationRoleManager>();
            }
            private set
            {
                if (value == null) throw new ArgumentNullException("value");
                _roleManager = value;
            }
        }

        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext()
                    .GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public ActionResult Index()
        {
            //return View(GroupManager.Groups.ToList());
            var groupsList = GroupManager.Groups.Select(
                @group => new ApplicationGroupViewModel
                {
                    Id = @group.Id, 
                    Name = @group.Name, 
                    Description = @group.Description
                });
            return View(groupsList);
        }


        public async Task<ActionResult> Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var applicationGroup = await GroupManager.FindByIdAsync(id);
            if (applicationGroup == null)
            {
                return HttpNotFound("No se encontró el grupo");
            }
            var roles = await GroupManager.GetGroupRolesAsync(id);
            var users = await GroupManager.GetGroupUsersAsync(id);
            var detailsGroupViewModel = new DetailsGroupViewModel
            {
                Id = applicationGroup.Id,
                Name = applicationGroup.Name,
                Description = applicationGroup.Description,
                RolesList = roles.ToList(),
                UsersList = users.ToList()
            };
            detailsGroupViewModel.CountRoles = detailsGroupViewModel.RolesList.Count;
            detailsGroupViewModel.CountUsers = detailsGroupViewModel.UsersList.Count;
            return View(detailsGroupViewModel);
        }


        public async Task<ActionResult> Create()
        {
            //Get a SelectList of Roles to choose from in the View:
            var groupViewModel = new ManageGroupViewModel();
            var allRoles = await RoleManager.Roles.ToListAsync();
            foreach (var listItem in allRoles.Select(@role => new SelectListItem
            {
                Text = @role.Name, 
                Value = @role.Id
            }))
            groupViewModel.RolesList.Add(listItem);
            return View(groupViewModel);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(ManageGroupViewModel model)
        {
            if (ModelState.IsValid)
            {
                // Create the new Group:
                var applicationGroup = new ApplicationGroup
                {
                    Name = model.Name,
                    Description = model.Description
                };
                var result = await GroupManager.CreateGroupAsync(applicationGroup);
                if (result.Succeeded)
                {
                    var selectedRoles = model.RolesList.Where(m => m.Selected).Select(m => m.Text).ToArray();

                    // Add the roles selected:
                    await GroupManager
                        .SetGroupRolesAsync(applicationGroup.Id, selectedRoles);
                }
                return RedirectToAction("Index");
            }
            return RedirectToAction("Create");
        }


        public async Task<ActionResult> Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationGroup applicationgroup = await GroupManager.FindByIdAsync(id);
            if (applicationgroup == null)
            {
                return HttpNotFound();
            }

            // Get a list, not a DbSet or queryable:
            var allRoles = await RoleManager.Roles.ToListAsync();
            var groupRoles = await GroupManager.GetGroupRolesAsync(id);

            var model = new ManageGroupViewModel
            {
                Id = applicationgroup.Id,
                Name = applicationgroup.Name,
                Description = applicationgroup.Description
            };

            // load the roles/Roles for selection in the form:
            foreach (var listItem in allRoles.Select(@role => new SelectListItem
            {
                Text = @role.Name, 
                Value = @role.Id,
                Selected = groupRoles.Any(g => g.Id == @role.Id)
            }))
            {
                model.RolesList.Add(listItem);
            }
            return View(model);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(ManageGroupViewModel model)
        {
            var group = await GroupManager.FindByIdAsync(model.Id);
            if (group == null)
            {
                return HttpNotFound();
            }
            if (ModelState.IsValid)
            {
                group.Name = model.Name;
                group.Description = model.Description;
                await GroupManager.UpdateGroupAsync(group);

                var selectedRoles = model.RolesList.Where(m => m.Selected).Select(m => m.Text).ToArray();
                await GroupManager.SetGroupRolesAsync(group.Id, selectedRoles);
                return RedirectToAction("Index");
            }
            return View(model);
        }


        public async Task<ActionResult> Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var applicationGroup = await GroupManager.FindByIdAsync(id);
            if (applicationGroup == null)
            {
                return HttpNotFound();
            }
            var applicationGroupViewModel = new ApplicationGroupViewModel
            {
                Id = applicationGroup.Id,
                Name = applicationGroup.Name,
                Description = applicationGroup.Description
            };
            
            return View(applicationGroupViewModel);
        }


        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            await GroupManager.DeleteGroupAsync(id);
            return RedirectToAction("Index");
        }
        

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}