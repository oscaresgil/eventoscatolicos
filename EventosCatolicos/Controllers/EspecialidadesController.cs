﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using EventosCatolicos.Models;

namespace EventosCatolicos.Controllers
{
    public class EspecialidadesController : Controller
    {
        private readonly ApplicationDbContext _db = new ApplicationDbContext();

        // GET: Especialidades
        [Authorize(Roles = "Ver Especialidades, IT")]
        public ActionResult Index()
        {
            var especialidades = _db.Especialidades.Include(e => e.TipoEspecialidad);
            return View(especialidades.ToList());
        }

        // GET: Especialidades/Details/5
        [Authorize(Roles = "Ver Especialidades,IT")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Especialidad especialidad = _db.Especialidades.Find(id);
            if (especialidad == null)
            {
                return HttpNotFound();
            }
            return View(especialidad);
        }

        // GET: Especialidades/Create
        [Authorize(Roles = "Administrar Especialidades,IT")]
        public ActionResult Create()
        {
            ViewBag.TipoId = new SelectList(_db.TiposEspecialidad, "Id", "Nombre");
            return View();
        }

        // POST: Especialidades/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrar Especialidades,IT")]
        public ActionResult Create([Bind(Include = "Id,Nombre,TipoId")] Especialidad especialidad)
        {
            if (ModelState.IsValid)
            {
                _db.Especialidades.Add(especialidad);
                _db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.TipoId = new SelectList(_db.TiposEspecialidad, "Id", "Nombre", especialidad.TipoId);
            return View(especialidad);
        }

        // GET: Especialidades/Edit/5
        [Authorize(Roles = "Administrar Especialidades,IT")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Especialidad especialidad = _db.Especialidades.Find(id);
            if (especialidad == null)
            {
                return HttpNotFound();
            }
            ViewBag.TipoId = new SelectList(_db.TiposEspecialidad, "Id", "Nombre", especialidad.TipoId);
            return View(especialidad);
        }

        // POST: Especialidades/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrar Especialidades,IT")]
        public ActionResult Edit([Bind(Include = "Id,Nombre,TipoId")] Especialidad especialidad)
        {
            if (ModelState.IsValid)
            {
                _db.Entry(especialidad).State = EntityState.Modified;
                _db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.TipoId = new SelectList(_db.TiposEspecialidad, "Id", "Nombre", especialidad.TipoId);
            return View(especialidad);
        }

        // GET: Especialidades/Delete/5
        [Authorize(Roles = "Eliminar Especialidades,IT")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Especialidad especialidad = _db.Especialidades.Find(id);
            if (especialidad == null)
            {
                return HttpNotFound();
            }
            return View(especialidad);
        }

        // POST: Especialidades/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Eliminar Especialidades,IT")]
        public ActionResult DeleteConfirmed(int id)
        {
            Especialidad especialidad = _db.Especialidades.Find(id);
            _db.Especialidades.Remove(especialidad);
            _db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
