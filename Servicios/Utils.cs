﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventosCatolicos.Servicios
{
    public static class Utils
    {
        public static CultureInfo Culture = new CultureInfo("es-GT");

        public static string GetDate(DateTime? date)
        {
            return date != null ? date.Value.ToString("D", Culture) : String.Empty;
        }
    }
}
