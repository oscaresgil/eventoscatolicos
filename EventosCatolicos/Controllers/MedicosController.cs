﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using EventosCatolicos.Models;
using EventosCatolicos.Servicios;

namespace EventosCatolicos.Controllers
{
    public class MedicosController : Controller
    {
        private readonly ApplicationDbContext _db = new ApplicationDbContext();

        // GET: Medicos
        [Authorize(Roles = "Ver Medicos,IT")]
        public ActionResult Index()
        {
            var medicos = _db.Medicos.Include(m => m.ApplicationUser).Include(m => m.ApplicationUser1);
            return View(medicos.ToList());
        }

        // GET: Medicos/Details/5
        [Authorize(Roles = "Ver Medicos,IT")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Medico medico = _db.Medicos.Find(id);
            if (medico == null)
            {
                return HttpNotFound();
            }
            return View(medico);
        }

        // GET: Medicos/Create
        [Authorize(Roles = "Administrar Medicos,IT")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Medicos/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrar Medicos,IT")]
        public ActionResult Create([Bind(Include = "Id,UsuarioId,PrimerNombre,SegundoNombre,PrimerApellido,SegundoApellido,ApellidoCasada,Direccion,TelefonoCasa,TelefonoCelular,CorreoElectronico,HoraEntrada,HoraSalida,FechaAlta,Observaciones")] Medico medico)
        {
            if (ModelState.IsValid)
            {
                medico.CreatedBy = User.Identity.GetUserId();
                medico.ModifiedBy = User.Identity.GetUserId();
                medico.CreatedAt = System.DateTime.Now;
                medico.ModifiedAt = System.DateTime.Now;
                medico.CreatedFrom = RequestHelpers.GetClientIpAddress(Request);
                medico.ModifiedFrom = RequestHelpers.GetClientIpAddress(Request);
                _db.Medicos.Add(medico);
                _db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(medico);
        }

        // GET: Medicos/Edit/5
        [Authorize(Roles = "Administrar Medicos,IT")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Medico medico = _db.Medicos.Find(id);
            if (medico == null)
            {
                return HttpNotFound();
            }
            return View(medico);
        }

        // POST: Medicos/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrar Medicos,IT")]
        public ActionResult Edit([Bind(Include = "Id,UsuarioId,PrimerNombre,SegundoNombre,PrimerApellido,SegundoApellido,ApellidoCasada,Direccion,TelefonoCasa,TelefonoCelular,CorreoElectronico,HoraEntrada,HoraSalida,FechaAlta,Observaciones")] Medico medico)
        {
            if (ModelState.IsValid)
            {
                medico.ModifiedBy = User.Identity.GetUserId();
                medico.ModifiedAt = System.DateTime.Now;
                medico.ModifiedFrom = RequestHelpers.GetClientIpAddress(Request);
                _db.Entry(medico).State = EntityState.Modified;
                _db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(medico);
        }

        // GET: Medicos/Delete/5
        [Authorize(Roles = "Eliminar Medicos,IT")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Medico medico = _db.Medicos.Find(id);
            if (medico == null)
            {
                return HttpNotFound();
            }
            return View(medico);
        }

        // POST: Medicos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Eliminar Medicos,IT")]
        public ActionResult DeleteConfirmed(int id)
        {
            Medico medico = _db.Medicos.Find(id);
            _db.Medicos.Remove(medico);
            _db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
