﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using EventosCatolicos.Models;
using EventosCatolicos.ViewModels;

namespace EventosCatolicos.Controllers
{
    public class ClinicasController : Controller
    {
        private readonly ApplicationDbContext _db = new ApplicationDbContext();

        // GET: Clinicas
        [Authorize(Roles = "Ver Clinicas,IT")]
        public ActionResult Index()
        {
            return View(_db.Clinicas.ToList());
        }

        // GET: Clinicas/Details/5
        [Authorize(Roles = "Ver Clinicas,IT")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Clinica clinica = _db.Clinicas.Find(id);
            if (clinica == null)
            {
                return HttpNotFound();
            }
            return View(clinica);
        }

        // GET: Clinicas/Create
        [Authorize(Roles = "Administrar Clinicas,IT")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Clinicas/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrar Clinicas,IT")]
        public ActionResult Create([Bind(Include = "Id,Nombre")] Clinica clinica)
        {
            if (ModelState.IsValid)
            {
                _db.Clinicas.Add(clinica);
                _db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(clinica);
        }

        // GET: Clinicas/Edit/5
        [Authorize(Roles = "Administrar Clinicas,IT")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Clinica clinica = _db.Clinicas.Find(id);
            if (clinica == null)
            {
                return HttpNotFound();
            }
            return View(clinica);
        }

        // POST: Clinicas/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrar Clinicas,IT")]
        public ActionResult Edit([Bind(Include = "Id,Nombre")] Clinica clinica)
        {
            if (ModelState.IsValid)
            {
                _db.Entry(clinica).State = EntityState.Modified;
                _db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(clinica);
        }

        // GET: Clinicas/Delete/5
        [Authorize(Roles = "Eliminar Clinicas,IT")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Clinica clinica = _db.Clinicas.Find(id);
            if (clinica == null)
            {
                return HttpNotFound();
            }
            return View(clinica);
        }

        // POST: Clinicas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Eliminar Clinicas,IT")]
        public ActionResult DeleteConfirmed(int id)
        {
            Clinica clinica = _db.Clinicas.Find(id);
            _db.Clinicas.Remove(clinica);
            _db.SaveChanges();
            return RedirectToAction("Index");
        }

        // GET: Pacientes/PantallaArchivo
        /*
         * Controlador que genera las consultas que se mostrarán en pantalla
         * Muestra las consultas del día de hoy
         * 
         */
        [Authorize]
        public ActionResult PantallaClinica(int id)
        {
            Response.AddHeader("Refresh", "10");
            var estadoActivo = _db.EstadoConsultas.First(e => e.Nombre.Equals("Activo")).Id;
            var consultas = _db.Consultas
                .Where(c => c.FechaConsulta.Value == DateTime.Today &&
                    (c.EstadoId == estadoActivo) &&
                    c.ClinicaId == id)
                .OrderBy(c => c.CreatedAt)
                .Take(10)
                .AsEnumerable()
                .Select(c =>
                {
                    var paciente = _db.Pacientes.Find(c.PacienteId);
                    var model = new PantallaViewModel
                    {
                        Id = paciente.Id,
                        IdConsulta = c.Id,
                        Nombre = paciente.PrimerNombre + " " +
                                    paciente.SegundoNombre + " " +
                                    paciente.PrimerApellido + " " +
                                    paciente.SegundoApellido + " " +
                                    paciente.ApellidoCasada,
                        Medico = string.Format("{0} {1}", c.Medico.PrimerNombre, c.Medico.PrimerApellido)
                    };
                    return model;
                });
            var clinica = new PantallaClinicaViewModel
            {
                Clinica = _db.Clinicas.Find(id).Nombre,
                Consultas = consultas
            };

            ViewBag.ClinicaId = new SelectList(_db.Clinicas, "Id", "Nombre");
            return View(clinica);
        }

        public ActionResult SwitchClinica(PantallaNewClinicaViewModel model)
        {
            return RedirectToAction("PantallaClinica", new {id = model.ClinicaId});
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
