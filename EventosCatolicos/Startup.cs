﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(EventosCatolicos.Startup))]
namespace EventosCatolicos
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
