﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using EventosCatolicos.Models;

namespace EventosCatolicos.Controllers
{
    public class TallasController : Controller
    {
        private readonly ApplicationDbContext _db = new ApplicationDbContext();

        // GET: Tallas
        [Authorize(Roles = "Ver Tallas,IT")]
        public ActionResult Index()
        {
            return View(_db.Tallas.ToList());
        }

        // GET: Tallas/Details/5
        [Authorize(Roles = "Ver Tallas,IT")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Talla talla = _db.Tallas.Find(id);
            if (talla == null)
            {
                return HttpNotFound();
            }
            return View(talla);
        }

        // GET: Tallas/Create
        [Authorize(Roles = "Administrar Tallas,IT")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Tallas/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Adminsitrar Tallas,IT")]
        public ActionResult Create([Bind(Include = "Id,Nombre")] Talla talla)
        {
            if (ModelState.IsValid)
            {
                _db.Tallas.Add(talla);
                _db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(talla);
        }

        // GET: Tallas/Edit/5
        [Authorize(Roles = "Adminsitrar Tallas,IT")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Talla talla = _db.Tallas.Find(id);
            if (talla == null)
            {
                return HttpNotFound();
            }
            return View(talla);
        }

        // POST: Tallas/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Adminsitrar Tallas,IT")]
        public ActionResult Edit([Bind(Include = "Id,Nombre")] Talla talla)
        {
            if (ModelState.IsValid)
            {
                _db.Entry(talla).State = EntityState.Modified;
                _db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(talla);
        }

        // GET: Tallas/Delete/5
        [Authorize(Roles = "Eliminar Tallas,IT")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Talla talla = _db.Tallas.Find(id);
            if (talla == null)
            {
                return HttpNotFound();
            }
            return View(talla);
        }

        // POST: Tallas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Eliminar Tallas,IT")]
        public ActionResult DeleteConfirmed(int id)
        {
            Talla talla = _db.Tallas.Find(id);
            _db.Tallas.Remove(talla);
            _db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
