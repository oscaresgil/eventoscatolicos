﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using EventosCatolicos.Models;

namespace EventosCatolicos.Controllers
{
    public class TratamientosController : Controller
    {
        private readonly ApplicationDbContext _db = new ApplicationDbContext();

        // GET: Tratamientos
        [Authorize(Roles = "Ver Tratamientos,IT")]
        public ActionResult Index()
        {
            var tratamientos = _db.Tratamientos.Include(t => t.CategoriaTratamiento);
            return View(tratamientos.ToList());
        }

        // GET: Tratamientos/Details/5
        [Authorize(Roles = "Ver Tratamientos,IT")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Tratamiento tratamiento = _db.Tratamientos.Find(id);
            if (tratamiento == null)
            {
                return HttpNotFound();
            }
            return View(tratamiento);
        }

        // GET: Tratamientos/Create
        [Authorize(Roles = "Administrar Tratamientos,IT")]
        public ActionResult Create()
        {
            ViewBag.CategoriaId = new SelectList(_db.CategoriaTratamientos, "Id", "Nombre");
            return View();
        }

        // POST: Tratamientos/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrar Tratamientos,IT")]
        public ActionResult Create([Bind(Include = "Id,Nombre,Descripcion,CategoriaId")] Tratamiento tratamiento)
        {
            if (ModelState.IsValid)
            {
                _db.Tratamientos.Add(tratamiento);
                _db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CategoriaId = new SelectList(_db.CategoriaTratamientos, "Id", "Nombre", tratamiento.CategoriaId);
            return View(tratamiento);
        }

        // GET: Tratamientos/Edit/5
        [Authorize(Roles = "Administrar Tratamientos,IT")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Tratamiento tratamiento = _db.Tratamientos.Find(id);
            if (tratamiento == null)
            {
                return HttpNotFound();
            }
            ViewBag.CategoriaId = new SelectList(_db.CategoriaTratamientos, "Id", "Nombre", tratamiento.CategoriaId);
            return View(tratamiento);
        }

        // POST: Tratamientos/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrar Tratamientos,IT")]
        public ActionResult Edit([Bind(Include = "Id,Nombre,Descripcion,CategoriaId")] Tratamiento tratamiento)
        {
            if (ModelState.IsValid)
            {
                _db.Entry(tratamiento).State = EntityState.Modified;
                _db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CategoriaId = new SelectList(_db.CategoriaTratamientos, "Id", "Nombre", tratamiento.CategoriaId);
            return View(tratamiento);
        }

        // GET: Tratamientos/Delete/5
        [Authorize(Roles = "Eliminar Tratamientos,IT")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Tratamiento tratamiento = _db.Tratamientos.Find(id);
            if (tratamiento == null)
            {
                return HttpNotFound();
            }
            return View(tratamiento);
        }

        // POST: Tratamientos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Eliminar Tratamientos,IT")]
        public ActionResult DeleteConfirmed(int id)
        {
            Tratamiento tratamiento = _db.Tratamientos.Find(id);
            _db.Tratamientos.Remove(tratamiento);
            _db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
