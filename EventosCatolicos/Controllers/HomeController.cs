﻿using System.Linq;
using System.Web.Mvc;
using EventosCatolicos.Models;

namespace EventosCatolicos.Controllers
{
    public class HomeController : Controller
    {
        private readonly ApplicationDbContext _db = new ApplicationDbContext();

        [Authorize]
        public ActionResult Index()
        {
            var clinica = _db.ClinicaGeneral.First(c => c.Nombre.Equals("Eventos Católicos"));
            return View(clinica);
        }
    }
}