namespace EventosCatolicos.Models
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("DetallesRecetario")]
    public class DetallesRecetario
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Display(Name = "Consulta")]
        public int ConsultaId { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Display(Name = "Recetario")]
        public int RecetarioId { get; set; }

        [StringLength(256, ErrorMessage = "Sobrepas� el tama�o m�ximo de caracteres.")]
        public string Dosis { get; set; }

        [StringLength(256, ErrorMessage = "Sobrepas� el tama�o m�ximo de caracteres.")]
        public string Duracion { get; set; }

        public virtual Consulta Consulta { get; set; }

        public virtual Recetario Recetario { get; set; }
    }
}
