namespace EventosCatolicos.Models
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("DetallesDiagnostico")]
    public class DetallesDiagnostico
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Display(Name = "Consulta")]
        public int ConsultaId { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Display(Name = "Diagn�stico")]
        public int DiagnosticoId { get; set; }

        [StringLength(256, ErrorMessage = "Sobrepas� el tama�o m�ximo de caracteres.")]
        public string Observacion { get; set; }

        public virtual Consulta Consulta { get; set; }

        public virtual Diagnostico Diagnostico { get; set; }
    }
}
