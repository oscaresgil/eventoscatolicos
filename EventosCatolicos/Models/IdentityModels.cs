﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Diagnostics.CodeAnalysis;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
// ReSharper disable VirtualMemberCallInContructor

namespace EventosCatolicos.Models
{
    // You will not likely need to customize there, but it is necessary/easier to create our own 
    // project-specific implementations, so here they are:
    public class ApplicationUserLogin : IdentityUserLogin<string>
    {
    }

    public class ApplicationUserClaim : IdentityUserClaim<string>
    {
    }

    public class ApplicationUserRole : IdentityUserRole<string>
    {
    }

    // Must be expressed in terms of our custom Role and other types:
    public class ApplicationUser
        : IdentityUser<string, ApplicationUserLogin,
            ApplicationUserRole, ApplicationUserClaim>
    {
        public ApplicationUser()
        {
            Id = Guid.NewGuid().ToString();

            // Add any custom User properties/code here
            ApplicationUserGroups = new HashSet<ApplicationUserGroup>();
            ApplicationUsers1 = new HashSet<ApplicationUser>();
            ApplicationUsers2 = new HashSet<ApplicationUser>();
            Consultas = new HashSet<Consulta>();
            Consultas1 = new HashSet<Consulta>();
            Medicos = new HashSet<Medico>();
            Medicos1 = new HashSet<Medico>();
            Medicos2 = new HashSet<Medico>();
            Pacientes = new HashSet<Paciente>();
            Pacientes1 = new HashSet<Paciente>();
            Recetarios = new HashSet<Recetario>();
            Recetarios1 = new HashSet<Recetario>();
        }

        [StringLength(256, ErrorMessage = "Sobrepasó el tamaño máximo de caracteres.")]
        [Required]
        public string PrimerNombre { get; set; }

        [StringLength(256, ErrorMessage = "Sobrepasó el tamaño máximo de caracteres.")]
        public string SegundoNombre { get; set; }

        [StringLength(256, ErrorMessage = "Sobrepasó el tamaño máximo de caracteres.")]
        [Required]
        public string PrimerApellido { get; set; }

        [StringLength(256, ErrorMessage = "Sobrepasó el tamaño máximo de caracteres.")]
        public string SegundoApellido { get; set; }

        [StringLength(128, ErrorMessage = "Sobrepasó el tamaño máximo de caracteres.")]
        public virtual string CreatedBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? CreatedAt { get; set; }

        [StringLength(50, ErrorMessage = "Sobrepasó el tamaño máximo de caracteres.")]
        public string CreatedFrom { get; set; }

        [StringLength(128, ErrorMessage = "Sobrepasó el tamaño máximo de caracteres.")]
        public virtual string ModifiedBy { get; set; }  

        [Column(TypeName = "datetime2")]
        public DateTime? ModifiedAt { get; set; }

        [StringLength(50, ErrorMessage = "Sobrepasó el tamaño máximo de caracteres.")]
        public string ModifiedFrom { get; set; }

        [SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ApplicationUserGroup> ApplicationUserGroups { get; set; }

        [SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ApplicationUser> ApplicationUsers1 { get; set; }

        [ForeignKey("CreatedBy")]
        public virtual ApplicationUser ApplicationUser1 { get; set; }

        [SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ApplicationUser> ApplicationUsers2 { get; set; }

        [ForeignKey("ModifiedBy")]
        public virtual ApplicationUser ApplicationUser2 { get; set; }

        [SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Consulta> Consultas { get; set; }

        [SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Consulta> Consultas1 { get; set; }

        [SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Medico> Medicos { get; set; }

        [SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Medico> Medicos1 { get; set; }

        [SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Medico> Medicos2 { get; set; }

        [SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Paciente> Pacientes { get; set; }

        [SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Paciente> Pacientes1 { get; set; }

        [SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Recetario> Recetarios { get; set; }

        [SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Recetario> Recetarios1 { get; set; }


        public async Task<ClaimsIdentity>
            GenerateUserIdentityAsync(ApplicationUserManager manager)
        {
            var userIdentity = await manager
                .CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            return userIdentity;
        }
    }


    // Must be expressed in terms of our custom UserRole:
    public class ApplicationRole : IdentityRole<string, ApplicationUserRole>
    {
        public ApplicationRole()
        {
            Id = Guid.NewGuid().ToString();
        }

        public ApplicationRole(string name)
            : this()
        {
            Name = name;
        }

        // Add any custom Role properties/code here
    }

    public class ApplicationGroup
    {
        public ApplicationGroup()
        {
            Id = Guid.NewGuid().ToString();
            ApplicationRoles = new List<ApplicationGroupRole>();
            ApplicationUsers = new List<ApplicationUserGroup>();
        }
        
        public ApplicationGroup(string name)
            : this()
        {
            Name = name;
        }
        public ApplicationGroup(string name, string description)
            : this(name)
        {
            Description = description;
        }

        public string Id { get; set; }
        [Display(Name = "Nombre")]
        public string Name { get; set; }
        [Display(Name = "Descripción")]
        public string Description { get; set; }
        public virtual ICollection<ApplicationGroupRole> ApplicationRoles { get; set; }
        public virtual ICollection<ApplicationUserGroup> ApplicationUsers { get; set; }
    }

    public class ApplicationUserGroup
    {
        public string ApplicationUserId { get; set; }
        public string ApplicationGroupId { get; set; }
    }

    public class ApplicationGroupRole
    {
        public string ApplicationGroupId { get; set; }
        public string ApplicationRoleId { get; set; }
    }


    // Must be expressed in terms of our custom types:
    public class ApplicationDbContext
        : IdentityDbContext<ApplicationUser, ApplicationRole,
            string, ApplicationUserLogin, ApplicationUserRole, ApplicationUserClaim>
    {
        static ApplicationDbContext()
        {
            Database.SetInitializer(new ApplicationDbInitializer());
        }

        public ApplicationDbContext()
            : base("DefaultConnection")
        {
        }

        // Add additional items here as needed

        public virtual DbSet<ApplicationGroup> ApplicationGroups { get; set; }
        public virtual DbSet<Antecedente> Antecedentes { get; set; }
        public virtual DbSet<Archivo> Archivos { get; set; }
        public virtual DbSet<AreaRecetario> AreaRecetarios { get; set; }
        public virtual DbSet<CategoriaAntecedente> CategoriaAntecedentes { get; set; }
        public virtual DbSet<CategoriaDiagnostico> CategoriaDiagnosticos { get; set; }
        public virtual DbSet<CategoriaTratamiento> CategoriaTratamientos { get; set; }
        public virtual DbSet<Clinica> Clinicas { get; set; }
        public virtual DbSet<ClinicaGeneral> ClinicaGeneral { get; set; }
        public virtual DbSet<Consulta> Consultas { get; set; }
        public virtual DbSet<Departamento> Departamentos { get; set; }
        public virtual DbSet<DetallesAntecedente> DetallesAntecedentes { get; set; }
        public virtual DbSet<DetallesDiagnostico> DetallesDiagnosticos { get; set; }
        public virtual DbSet<DetallesExamen> DetallesExamen { get; set; }
        public virtual DbSet<DetallesRecetario> DetallesRecetarios { get; set; }
        public virtual DbSet<DetallesSintoma> DetallesSintomas { get; set; }
        public virtual DbSet<DetallesTratamiento> DetallesTratamientos { get; set; }
        public virtual DbSet<Diagnostico> Diagnosticos { get; set; }
        public virtual DbSet<Especialidad> Especialidades { get; set; }
        public virtual DbSet<EstadoCivil> EstadosCiviles { get; set; }
        public virtual DbSet<EstadoConsulta> EstadoConsultas { get; set; }
        public virtual DbSet<Examen> Examenes { get; set; }
        public virtual DbSet<Medico> Medicos { get; set; }
        public virtual DbSet<Municipio> Municipios { get; set; }
        public virtual DbSet<Nacionalidad> Nacionalidades { get; set; }
        public virtual DbSet<Paciente> Pacientes { get; set; }
        public virtual DbSet<Recetario> Recetarios { get; set; }
        public virtual DbSet<Profesion> Profesiones { get; set; }
        public virtual DbSet<Sexo> Sexos { get; set; }
        public virtual DbSet<Sintoma> Sintomas { get; set; }
        public virtual DbSet<Talla> Tallas { get; set; }
        public virtual DbSet<TipoDireccion> TiposDireccion { get; set; }
        public virtual DbSet<TipoEspecialidad> TiposEspecialidad { get; set; }
        public virtual DbSet<TipoIdentificacion> TiposIdentificacion { get; set; }
        public virtual DbSet<TipoSangre> TiposSangre { get; set; }
        public virtual DbSet<Tratamiento> Tratamientos { get; set; }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Properties<DateTime>()
                .Configure(c => c.HasColumnType("datetime2"));

            // Map Users to Groups:
            modelBuilder.Entity<ApplicationGroup>()
                .HasMany(g => g.ApplicationUsers)
                .WithRequired()
                .HasForeignKey(ag => ag.ApplicationGroupId);
            modelBuilder.Entity<ApplicationUserGroup>()
                .HasKey(r =>
                    new
                    {
                        r.ApplicationUserId,
                        r.ApplicationGroupId
                    }).ToTable("ApplicationUserGroups");

            // Map Roles to Groups:
            modelBuilder.Entity<ApplicationGroup>()
                .HasMany(g => g.ApplicationRoles)
                .WithRequired()
                .HasForeignKey(ap => ap.ApplicationGroupId);
            modelBuilder.Entity<ApplicationGroupRole>().HasKey(gr =>
                new
                {
                    gr.ApplicationRoleId,
                    gr.ApplicationGroupId
                }).ToTable("ApplicationGroupRoles");

            /*modelBuilder.Properties().Where(e => e.Name)*/
            modelBuilder.Entity<Antecedente>()
                .HasMany(e => e.DetallesAntecedentes)
                .WithRequired(e => e.Antecedente)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Archivo>()
                .HasMany(e => e.Pacientes)
                .WithOptional(e => e.Archivo)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AreaRecetario>()
                .HasMany(e => e.Recetarios)
                .WithOptional(e => e.AreaRecetario)
                .HasForeignKey(e => e.AreaId);

            modelBuilder.Entity<ApplicationUser>()
                .HasMany(e => e.ApplicationUsers1)
                .WithOptional(e => e.ApplicationUser1)
                .HasForeignKey(e => e.CreatedBy);

            modelBuilder.Entity<ApplicationUser>()
                .HasMany(e => e.ApplicationUsers2)
                .WithOptional(e => e.ApplicationUser2)
                .HasForeignKey(e => e.ModifiedBy);

            modelBuilder.Entity<ApplicationUser>()
                .HasMany(e => e.Consultas)
                .WithOptional(e => e.ApplicationUser)
                .HasForeignKey(e => e.CreatedBy)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ApplicationUser>()
                .HasMany(e => e.Consultas1)
                .WithOptional(e => e.ApplicationUser1)
                .HasForeignKey(e => e.ModifiedBy)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ApplicationUser>()
                .HasMany(e => e.Medicos)
                .WithOptional(e => e.ApplicationUser)
                .HasForeignKey(e => e.UsuarioId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ApplicationUser>()
                .HasMany(e => e.Medicos1)
                .WithOptional(e => e.ApplicationUser1)
                .HasForeignKey(e => e.CreatedBy)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ApplicationUser>()
                .HasMany(e => e.Medicos2)
                .WithOptional(e => e.ApplicationUser2)
                .HasForeignKey(e => e.ModifiedBy)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ApplicationUser>()
                .HasMany(e => e.Pacientes)
                .WithOptional(e => e.ApplicationUser)
                .HasForeignKey(e => e.CreatedBy)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ApplicationUser>()
                .HasMany(e => e.Pacientes1)
                .WithOptional(e => e.ApplicationUser1)
                .HasForeignKey(e => e.ModifiedBy)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ApplicationUser>()
                .HasMany(e => e.Recetarios)
                .WithOptional(e => e.ApplicationUser)
                .HasForeignKey(e => e.CreatedBy)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ApplicationUser>()
                .HasMany(e => e.Recetarios1)
                .WithOptional(e => e.ApplicationUser1)
                .HasForeignKey(e => e.ModifiedBy)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CategoriaAntecedente>()
                .HasMany(e => e.Antecedentes)
                .WithRequired(e => e.CategoriaAntecedente)
                .HasForeignKey(e => e.CategoriaId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CategoriaDiagnostico>()
                .HasMany(e => e.Diagnosticos)
                .WithRequired(e => e.CategoriaDiagnostico)
                .HasForeignKey(e => e.CategoriaId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CategoriaTratamiento>()
                .HasMany(e => e.Tratamientos)
                .WithRequired(e => e.CategoriaTratamiento)
                .HasForeignKey(e => e.CategoriaId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Consulta>()
                .HasMany(e => e.DetallesAntecedentes)
                .WithRequired(e => e.Consulta)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<Consulta>()
                .HasMany(e => e.DetallesDiagnosticos)
                .WithRequired(e => e.Consulta)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<Consulta>()
                .HasMany(e => e.DetallesRecetarios)
                .WithRequired(e => e.Consulta)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<Consulta>()
                .HasMany(e => e.DetallesSintomas)
                .WithRequired(e => e.Consulta)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<Consulta>()
                .HasMany(e => e.DetallesTratamientos)
                .WithRequired(e => e.Consulta)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<Consulta>()
                .HasMany(e => e.DetallesExamenes)
                .WithRequired(e => e.Consulta)
                .WillCascadeOnDelete(true);

            /*modelBuilder.Entity<Consulta>()
                .HasMany(e => e.Examenes)
                .WithMany(e => e.Consultas)
                .Map(m => m.ToTable("DetallesExamen").MapLeftKey("ConsultaId").MapRightKey("ExamenId"));
            */

            modelBuilder.Entity<Departamento>()
                .HasMany(e => e.Municipios)
                .WithOptional(e => e.Departamento)
                .HasForeignKey(e => e.DepartamentoId);

            modelBuilder.Entity<Diagnostico>()
                .HasMany(e => e.DetallesDiagnosticos)
                .WithRequired(e => e.Diagnostico)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Especialidad>()
                .HasMany(e => e.Examen)
                .WithRequired(e => e.Especialidad)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Especialidad>()
                .HasMany(e => e.Medicos)
                .WithMany(e => e.Especialidades)
                .Map(m => m.ToTable("DetallesMedico").MapLeftKey("EspecialidadId").MapRightKey("MedicoId"));

            modelBuilder.Entity<EstadoCivil>()
                .HasMany(e => e.Pacientes)
                .WithOptional(e => e.EstadoCivil)
                .HasForeignKey(e => e.EstadoCivilId);

            modelBuilder.Entity<EstadoCivil>()
                .HasMany(e => e.Pacientes1)
                .WithOptional(e => e.EstadoCivil1)
                .HasForeignKey(e => e.RepEstadoCivilId);

            modelBuilder.Entity<EstadoConsulta>()
                .HasMany(e => e.Consultas)
                .WithRequired(e => e.EstadoConsulta)
                .HasForeignKey(e => e.EstadoId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Municipio>()
                .HasMany(e => e.Pacientes)
                .WithOptional(e => e.Municipio)
                .HasForeignKey(e => e.MunicipioId);

            modelBuilder.Entity<Municipio>()
                .HasMany(e => e.Pacientes1)
                .WithOptional(e => e.Municipio1)
                .HasForeignKey(e => e.RepMunicipioId);

            modelBuilder.Entity<Nacionalidad>()
                .HasMany(e => e.Pacientes)
                .WithOptional(e => e.Nacionalidad)
                .HasForeignKey(e => e.NacionalidadId);

            modelBuilder.Entity<Nacionalidad>()
                .HasMany(e => e.Pacientes1)
                .WithOptional(e => e.Nacionalidad1)
                .HasForeignKey(e => e.RepNacionalidadId);

            modelBuilder.Entity<Recetario>()
                .Property(e => e.PrecioPublico)
                .HasPrecision(19, 4);

            modelBuilder.Entity<Recetario>()
                .Property(e => e.PrecioEspecial)
                .HasPrecision(19, 4);

            modelBuilder.Entity<Recetario>()
                .HasMany(e => e.DetallesRecetarios)
                .WithRequired(e => e.Recetario)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Sexo>()
                .HasMany(e => e.Pacientes)
                .WithOptional(e => e.Sexo)
                .HasForeignKey(e => e.SexoId);

            modelBuilder.Entity<Sexo>()
                .HasMany(e => e.Pacientes1)
                .WithOptional(e => e.Sexo1)
                .HasForeignKey(e => e.RepSexoId);

            modelBuilder.Entity<Sintoma>()
                .HasMany(e => e.DetallesSintomas)
                .WithRequired(e => e.Sintoma)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TipoDireccion>()
                .HasMany(e => e.Pacientes)
                .WithOptional(e => e.TipoDireccion)
                .HasForeignKey(e => e.TipoDireccionId);

            modelBuilder.Entity<TipoDireccion>()
                .HasMany(e => e.Pacientes1)
                .WithOptional(e => e.TipoDireccion1)
                .HasForeignKey(e => e.RepTipoDireccionId);

            modelBuilder.Entity<TipoEspecialidad>()
                .HasMany(e => e.Especialidades)
                .WithRequired(e => e.TipoEspecialidad)
                .HasForeignKey(e => e.TipoId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TipoIdentificacion>()
                .HasMany(e => e.Pacientes)
                .WithOptional(e => e.TipoIdentificacion)
                .HasForeignKey(e => e.TipoIdentificacionId);

            modelBuilder.Entity<TipoIdentificacion>()
                .HasMany(e => e.Pacientes1)
                .WithOptional(e => e.TipoIdentificacion1)
                .HasForeignKey(e => e.RepTipoIdentificacionId);

            modelBuilder.Entity<Tratamiento>()
                .HasMany(e => e.DetallesTratamientos)
                .WithRequired(e => e.Tratamiento)
                .WillCascadeOnDelete(false);
        }
    }

    // Most likely won't need to customize these either, but they were needed because we implemented
    // custom versions of all the other types:
    public class ApplicationUserStore
        : UserStore<ApplicationUser, ApplicationRole, string,
            ApplicationUserLogin, ApplicationUserRole,
            ApplicationUserClaim>
    {
        public ApplicationUserStore()
            : this(new IdentityDbContext())
        {
            DisposeContext = true;
        }

        public ApplicationUserStore(DbContext context)
            : base(context)
        {
        }
    }


    public class ApplicationRoleStore
        : RoleStore<ApplicationRole, string, ApplicationUserRole>
    {
        public ApplicationRoleStore()
            : base(new IdentityDbContext())
        {
            DisposeContext = true;
        }

        public ApplicationRoleStore(DbContext context)
            : base(context)
        {
        }
    }
}