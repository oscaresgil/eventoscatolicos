// ReSharper disable VirtualMemberCallInContructor
namespace EventosCatolicos.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("Recetario")]
    public class Recetario
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Recetario()
        {
            DetallesRecetarios = new HashSet<DetallesRecetario>();
        }

        public int Id { get; set; }

        [Required(ErrorMessage = "Por favor ingrese un nombre.")]
        [StringLength(256, ErrorMessage = "Sobrepas� el tama�o m�ximo de caracteres.")]
        public string Nombre { get; set; }

        [StringLength(256, ErrorMessage = "Sobrepas� el tama�o m�ximo de caracteres.")]
        [Display(Name = "Descripci�n")]
        public string Descripcion { get; set; }

        [Display(Name = "Especialidad")]
        public int? EspecialidadId { get; set; }

        [Display(Name = "�rea")]
        public int? AreaId { get; set; }

        [Column(TypeName = "money")]
        [Display(Name = "Precio al P�blico")]
        public decimal? PrecioPublico { get; set; }

        [Column(TypeName = "money")]
        [Display(Name = "Precio Especial")]
        public decimal? PrecioEspecial { get; set; }

        [Column(TypeName = "datetime2")]
        [Display(Name = "Fecha de Alta")]
        public DateTime? FechaAlta { get; set; }

        [Column(TypeName = "datetime2")]
        [Display(Name = "Fecha de Baja")]
        public DateTime? FechaBaja { get; set; }

        [StringLength(256, ErrorMessage = "Sobrepas� el tama�o m�ximo de caracteres.")]
        [Display(Name = "Observaci�n")]
        public string Observacion { get; set; }

        [StringLength(128, ErrorMessage = "Sobrepas� el tama�o m�ximo de caracteres.")]
        [Display(Name = "Creado Por")]
        public string CreatedBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime CreatedAt { get; set; }

        [StringLength(50, ErrorMessage = "Sobrepas� el tama�o m�ximo de caracteres.")]
        [Display(Name = "Creado Desde")]
        public string CreatedFrom { get; set; }

        [StringLength(128, ErrorMessage = "Sobrepas� el tama�o m�ximo de caracteres.")]
        [Display(Name = "Modificado Por")]
        public string ModifiedBy { get; set; }

        [Column(TypeName = "datetime2")]
        [Display(Name = "Modificado En")]
        public DateTime ModifiedAt { get; set; }

        [StringLength(50, ErrorMessage = "Sobrepas� el tama�o m�ximo de caracteres.")]
        [Display(Name = "Modificado Desde")]
        public string ModifiedFrom { get; set; }

        public virtual AreaRecetario AreaRecetario { get; set; }

        public virtual ApplicationUser ApplicationUser { get; set; }

        public virtual ApplicationUser ApplicationUser1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DetallesRecetario> DetallesRecetarios { get; set; }

        public virtual Especialidad Especialidad { get; set; }
    }
}
