﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using EventosCatolicos.Models;

namespace EventosCatolicos.Controllers
{
    public class TiposSangreController : Controller
    {
        private readonly ApplicationDbContext _db = new ApplicationDbContext();

        // GET: TiposSangre
        [Authorize(Roles = "Ver TiposSangre,IT")]
        public ActionResult Index()
        {
            return View(_db.TiposSangre.ToList());
        }

        // GET: TiposSangre/Details/5
        [Authorize(Roles = "Ver TiposSangre,IT")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TipoSangre tipoSangre = _db.TiposSangre.Find(id);
            if (tipoSangre == null)
            {
                return HttpNotFound();
            }
            return View(tipoSangre);
        }

        // GET: TiposSangre/Create
        [Authorize(Roles = "Administrar TiposSangre,IT")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: TiposSangre/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrar TiposSangre,IT")]
        public ActionResult Create([Bind(Include = "Id,Nombre")] TipoSangre tipoSangre)
        {
            if (ModelState.IsValid)
            {
                _db.TiposSangre.Add(tipoSangre);
                _db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tipoSangre);
        }

        // GET: TiposSangre/Edit/5
        [Authorize(Roles = "Administrar TiposSangre,IT")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TipoSangre tipoSangre = _db.TiposSangre.Find(id);
            if (tipoSangre == null)
            {
                return HttpNotFound();
            }
            return View(tipoSangre);
        }

        // POST: TiposSangre/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrar TiposSangre,IT")]
        public ActionResult Edit([Bind(Include = "Id,Nombre")] TipoSangre tipoSangre)
        {
            if (ModelState.IsValid)
            {
                _db.Entry(tipoSangre).State = EntityState.Modified;
                _db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tipoSangre);
        }

        // GET: TiposSangre/Delete/5
        [Authorize(Roles = "Eliminar TiposSangre,IT")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TipoSangre tipoSangre = _db.TiposSangre.Find(id);
            if (tipoSangre == null)
            {
                return HttpNotFound();
            }
            return View(tipoSangre);
        }

        // POST: TiposSangre/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TipoSangre tipoSangre = _db.TiposSangre.Find(id);
            _db.TiposSangre.Remove(tipoSangre);
            _db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
