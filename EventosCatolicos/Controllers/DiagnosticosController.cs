﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using EventosCatolicos.Models;

namespace EventosCatolicos.Controllers
{
    public class DiagnosticosController : Controller
    {
        private readonly ApplicationDbContext _db = new ApplicationDbContext();

        // GET: Diagnosticos
        [Authorize(Roles = "Ver Diagnosticos,IT")]
        public ActionResult Index()
        {
            var diagnosticos = _db.Diagnosticos.Include(d => d.CategoriaDiagnostico);
            return View(diagnosticos.ToList());
        }

        // GET: Diagnosticos/Details/5
        [Authorize(Roles = "Ver Diagnosticos,IT")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Diagnostico diagnostico = _db.Diagnosticos.Find(id);
            if (diagnostico == null)
            {
                return HttpNotFound();
            }
            return View(diagnostico);
        }

        // GET: Diagnosticos/Create
        [Authorize(Roles = "Administrar Diagnosticos,IT")]
        public ActionResult Create()
        {
            ViewBag.CategoriaId = new SelectList(_db.CategoriaDiagnosticos, "Id", "Nombre");
            return View();
        }

        // POST: Diagnosticos/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrar Diagnosticos,IT")]
        public ActionResult Create([Bind(Include = "Id,Nombre,Descripcion,CategoriaId")] Diagnostico diagnostico)
        {
            if (ModelState.IsValid)
            {
                _db.Diagnosticos.Add(diagnostico);
                _db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CategoriaId = new SelectList(_db.CategoriaDiagnosticos, "Id", "Nombre", diagnostico.CategoriaId);
            return View(diagnostico);
        }

        // GET: Diagnosticos/Edit/5
        [Authorize(Roles = "Administrar Diagnosticos,IT")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Diagnostico diagnostico = _db.Diagnosticos.Find(id);
            if (diagnostico == null)
            {
                return HttpNotFound();
            }
            ViewBag.CategoriaId = new SelectList(_db.CategoriaDiagnosticos, "Id", "Nombre", diagnostico.CategoriaId);
            return View(diagnostico);
        }

        // POST: Diagnosticos/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrar Diagnosticos,IT")]
        public ActionResult Edit([Bind(Include = "Id,Nombre,Descripcion,CategoriaId")] Diagnostico diagnostico)
        {
            if (ModelState.IsValid)
            {
                _db.Entry(diagnostico).State = EntityState.Modified;
                _db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CategoriaId = new SelectList(_db.CategoriaDiagnosticos, "Id", "Nombre", diagnostico.CategoriaId);
            return View(diagnostico);
        }

        // GET: Diagnosticos/Delete/5
        [Authorize(Roles = "Eliminar Diagnosticos,IT")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Diagnostico diagnostico = _db.Diagnosticos.Find(id);
            if (diagnostico == null)
            {
                return HttpNotFound();
            }
            return View(diagnostico);
        }

        // POST: Diagnosticos/Delete/5
        [Authorize(Roles = "Eliminar Diagnosticos,IT")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Diagnostico diagnostico = _db.Diagnosticos.Find(id);
            _db.Diagnosticos.Remove(diagnostico);
            _db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
