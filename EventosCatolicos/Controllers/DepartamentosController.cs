﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using EventosCatolicos.Models;

namespace EventosCatolicos.Controllers
{
    public class DepartamentosController : Controller
    {
        private readonly ApplicationDbContext _db = new ApplicationDbContext();

        // GET: Departamentos
        [Authorize(Roles = "Ver Departamentos,IT")]
        public ActionResult Index()
        {
            return View(_db.Departamentos.ToList());
        }

        // GET: Departamentos/Details/5
        [Authorize(Roles = "Ver Departamentos,IT")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Departamento departamento = _db.Departamentos.Find(id);
            if (departamento == null)
            {
                return HttpNotFound();
            }
            return View(departamento);
        }

        // GET: Departamentos/Create
        [Authorize(Roles = "Administrar Departamentos,IT")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Departamentos/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrar Departamentos,IT")]
        public ActionResult Create([Bind(Include = "Id,Nombre")] Departamento departamento)
        {
            if (ModelState.IsValid)
            {
                _db.Departamentos.Add(departamento);
                _db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(departamento);
        }

        // GET: Departamentos/Edit/5
        [Authorize(Roles = "Administrar Departamentos,IT")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Departamento departamento = _db.Departamentos.Find(id);
            if (departamento == null)
            {
                return HttpNotFound();
            }
            return View(departamento);
        }

        // POST: Departamentos/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrar Departamentos,IT")]
        public ActionResult Edit([Bind(Include = "Id,Nombre")] Departamento departamento)
        {
            if (ModelState.IsValid)
            {
                _db.Entry(departamento).State = EntityState.Modified;
                _db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(departamento);
        }

        // GET: Departamentos/Delete/5
        [Authorize(Roles = "Eliminar Departamentos,IT")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Departamento departamento = _db.Departamentos.Find(id);
            if (departamento == null)
            {
                return HttpNotFound();
            }
            return View(departamento);
        }

        // POST: Departamentos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Eliminar Departamentos,IT")]
        public ActionResult DeleteConfirmed(int id)
        {
            Departamento departamento = _db.Departamentos.Find(id);
            _db.Departamentos.Remove(departamento);
            _db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
