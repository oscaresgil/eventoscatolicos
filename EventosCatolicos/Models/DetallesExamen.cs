﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace EventosCatolicos.Models
{
    public class DetallesExamen
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Display(Name = "Consulta")]
        public int ConsultaId { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Display(Name = "Exámen")]
        public int ExamenId { get; set; }

        public virtual Consulta Consulta { get; set; }

        public virtual Examen Examen { get; set; }
    }
}