﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using EventosCatolicos.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security;

namespace EventosCatolicos
{
    public class EmailService : IIdentityMessageService
    {
        public Task SendAsync(IdentityMessage message)
        {
            // Conecte su servicio de correo electrónico aquí para enviar correo electrónico.
            return Task.FromResult(0);
        }
    }

    public class SmsService : IIdentityMessageService
    {
        public Task SendAsync(IdentityMessage message)
        {
            // Conecte el servicio SMS aquí para enviar un mensaje de texto.
            return Task.FromResult(0);
        }
    }

    // Configure el administrador de usuarios de aplicación que se usa en esta aplicación. UserManager se define en ASP.NET Identity y se usa en la aplicación.
    public class ApplicationUserManager : UserManager<ApplicationUser, string>
    {
        public ApplicationUserManager(IUserStore<ApplicationUser, string> store)
            : base(store)
        {
        }

        public static ApplicationUserManager Create(IdentityFactoryOptions<ApplicationUserManager> options,
            IOwinContext context)
        {
            var manager =
                new ApplicationUserManager(
                    new UserStore
                        <ApplicationUser, ApplicationRole, string, ApplicationUserLogin, ApplicationUserRole,
                            ApplicationUserClaim>(context.Get<ApplicationDbContext>()));
            // Configure la lógica de validación de nombres de usuario
            manager.UserValidator = new UserValidator<ApplicationUser>(manager)
            {
                AllowOnlyAlphanumericUserNames = true,
                RequireUniqueEmail = true
            };

            // Configure la lógica de validación de contraseñas
            manager.PasswordValidator = new PasswordValidator
            {
                RequiredLength = 6,
                RequireNonLetterOrDigit = false,
                RequireDigit = false,
                RequireLowercase = false,
                RequireUppercase = false
            };

            // Configurar valores predeterminados para bloqueo de usuario
            manager.UserLockoutEnabledByDefault = true;
            manager.DefaultAccountLockoutTimeSpan = TimeSpan.FromMinutes(5);
            manager.MaxFailedAccessAttemptsBeforeLockout = 5;

            // Registre proveedores de autenticación en dos fases. Esta aplicación usa los pasos Teléfono y Correo electrónico para recibir un código para comprobar el usuario
            // Puede escribir su propio proveedor y conectarlo aquí.
            manager.RegisterTwoFactorProvider("Código telefónico", new PhoneNumberTokenProvider<ApplicationUser>
            {
                MessageFormat = "Su código de seguridad es {0}"
            });
            manager.RegisterTwoFactorProvider("Código de correo electrónico", new EmailTokenProvider<ApplicationUser>
            {
                Subject = "Código de seguridad",
                BodyFormat = "Su código de seguridad es {0}"
            });
            manager.EmailService = new EmailService();
            manager.SmsService = new SmsService();
            var dataProtectionProvider = options.DataProtectionProvider;
            if (dataProtectionProvider != null)
            {
                manager.UserTokenProvider =
                    new DataProtectorTokenProvider<ApplicationUser>(dataProtectionProvider.Create("ASP.NET Identity"));
            }
            return manager;
        }
    }

    // Configure the RoleManager used in the application. RoleManager is defined in the ASP.NET Identity core assembly
    public class ApplicationRoleManager : RoleManager<ApplicationRole>
    {
        public ApplicationRoleManager(IRoleStore<ApplicationRole, string> roleStore)
            : base(roleStore)
        {
        }

        public static ApplicationRoleManager Create(IdentityFactoryOptions<ApplicationRoleManager> options,
            IOwinContext context)
        {
            return new ApplicationRoleManager(new ApplicationRoleStore(context.Get<ApplicationDbContext>()));
        }
    }

    // This is useful if you do not want to tear down the database each time you run the application.
    // public class ApplicationDbInitializer : DropCreateDatabaseAlways<ApplicationDbContext>
    // This example shows you how to create a new database if the Model changes
    public class ApplicationDbInitializer : DropCreateDatabaseIfModelChanges<ApplicationDbContext>
    {
        protected override void Seed(ApplicationDbContext context)
        {
            InitializeIdentityForEf(context);
            base.Seed(context);
        }

        //Create User=Admin@Admin.com with password=Admin@123456 in the Admin role        
        public static void InitializeIdentityForEf(ApplicationDbContext db)
        {
            var userManager = HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
            var roleManager = HttpContext.Current.GetOwinContext().Get<ApplicationRoleManager>();
            var groupManager = HttpContext.Current.GetOwinContext().Get<ApplicationGroupManager>();

            roleManager.Create(new ApplicationRole("Admin"));

            //index y detalles
            roleManager.Create(new ApplicationRole("Ver Antecedentes"));
            roleManager.Create(new ApplicationRole("Ver Archivo"));
            roleManager.Create(new ApplicationRole("Ver AreasRecetario"));
            roleManager.Create(new ApplicationRole("Ver CategoriaAntecedente"));
            roleManager.Create(new ApplicationRole("Ver CategoriaDiagnostico"));
            roleManager.Create(new ApplicationRole("Ver CategoriaTratamiento"));
            roleManager.Create(new ApplicationRole("Ver Clinicas"));
            roleManager.Create(new ApplicationRole("Ver Consultas"));
            roleManager.Create(new ApplicationRole("Ver Departamentos"));
            roleManager.Create(new ApplicationRole("Ver Diagnosticos"));
            roleManager.Create(new ApplicationRole("Ver Especialidades"));
            roleManager.Create(new ApplicationRole("Ver Especialidades"));
            roleManager.Create(new ApplicationRole("Ver EstadosCiviles"));
            roleManager.Create(new ApplicationRole("Ver Examenes"));
            roleManager.Create(new ApplicationRole("Ver GroupsAdmin"));
            roleManager.Create(new ApplicationRole("Ver Medicos"));
            roleManager.Create(new ApplicationRole("Ver Municipios"));
            roleManager.Create(new ApplicationRole("Ver Nacionalidades"));
            roleManager.Create(new ApplicationRole("Ver Pacientes"));
            roleManager.Create(new ApplicationRole("Ver Recetarios"));
            roleManager.Create(new ApplicationRole("Ver Profesiones"));
            roleManager.Create(new ApplicationRole("Ver RolesAdmin"));
            roleManager.Create(new ApplicationRole("Ver Sexos"));
            roleManager.Create(new ApplicationRole("Ver Sintomas"));
            roleManager.Create(new ApplicationRole("Ver Tallas"));
            roleManager.Create(new ApplicationRole("Ver TiposDireccion"));
            roleManager.Create(new ApplicationRole("Ver TiposEspecialidad"));
            roleManager.Create(new ApplicationRole("Ver TiposIdentificacion"));
            roleManager.Create(new ApplicationRole("Ver TiposSangre"));
            roleManager.Create(new ApplicationRole("Ver Tratamiento"));
            /////////////////////////////////////////////////////////

            //Editar y crear
            roleManager.Create(new ApplicationRole("Administrar Antecedentes"));
            roleManager.Create(new ApplicationRole("Administrar Archivo"));
            roleManager.Create(new ApplicationRole("Administrar AreasRecetario"));
            roleManager.Create(new ApplicationRole("Administrar CategoriaAntecedente"));
            roleManager.Create(new ApplicationRole("Administrar CategoriaDiagnostico"));
            roleManager.Create(new ApplicationRole("Administrar CategoriaTratamiento"));
            roleManager.Create(new ApplicationRole("Administrar Clinicas"));
            roleManager.Create(new ApplicationRole("Administrar Consultas"));
            roleManager.Create(new ApplicationRole("Administrar Departamentos"));
            roleManager.Create(new ApplicationRole("Administrar Diagnosticos"));
            roleManager.Create(new ApplicationRole("Administrar Especialidades"));
            roleManager.Create(new ApplicationRole("Administrar Especialidades"));
            roleManager.Create(new ApplicationRole("Administrar EstadosCiviles"));
            roleManager.Create(new ApplicationRole("Administrar Examenes"));
            roleManager.Create(new ApplicationRole("Administrar GroupsAdmin"));
            roleManager.Create(new ApplicationRole("Administrar Medicos"));
            roleManager.Create(new ApplicationRole("Administrar Municipios"));
            roleManager.Create(new ApplicationRole("Administrar Nacionalidades"));
            roleManager.Create(new ApplicationRole("Administrar Pacientes"));
            roleManager.Create(new ApplicationRole("Administrar Recetarios"));
            roleManager.Create(new ApplicationRole("Administrar Profesiones"));
            roleManager.Create(new ApplicationRole("Administrar RolesAdmin"));
            roleManager.Create(new ApplicationRole("Administrar Sexos"));
            roleManager.Create(new ApplicationRole("Administrar Sintomas"));
            roleManager.Create(new ApplicationRole("Administrar Tallas"));
            roleManager.Create(new ApplicationRole("Administrar TiposDireccion"));
            roleManager.Create(new ApplicationRole("Administrar TiposEspecialidad"));
            roleManager.Create(new ApplicationRole("Administrar TiposIdentificacion"));
            roleManager.Create(new ApplicationRole("Administrar TiposSangre"));
            roleManager.Create(new ApplicationRole("Administrar Tratamiento"));
            //////////////////////////////////////////////////

            roleManager.Create(new ApplicationRole("Eliminar Antecedentes"));
            roleManager.Create(new ApplicationRole("Eliminar Archivo"));
            roleManager.Create(new ApplicationRole("Eliminar AreasRecetario"));
            roleManager.Create(new ApplicationRole("Eliminar CategoriaAntecedente"));
            roleManager.Create(new ApplicationRole("Eliminar CategoriaDiagnostico"));
            roleManager.Create(new ApplicationRole("Eliminar CategoriaTratamiento"));
            roleManager.Create(new ApplicationRole("Eliminar Clinicas"));
            roleManager.Create(new ApplicationRole("Eliminar Consultas"));
            roleManager.Create(new ApplicationRole("Eliminar Departamentos"));
            roleManager.Create(new ApplicationRole("Eliminar Diagnosticos"));
            roleManager.Create(new ApplicationRole("Eliminar Especialidades"));
            roleManager.Create(new ApplicationRole("Eliminar Especialidades"));
            roleManager.Create(new ApplicationRole("Eliminar EstadosCiviles"));
            roleManager.Create(new ApplicationRole("Eliminar Examenes"));
            roleManager.Create(new ApplicationRole("Eliminar GroupsAdmin"));
            roleManager.Create(new ApplicationRole("Eliminar Medicos"));
            roleManager.Create(new ApplicationRole("Eliminar Municipios"));
            roleManager.Create(new ApplicationRole("Eliminar Nacionalidades"));
            roleManager.Create(new ApplicationRole("Eliminar Pacientes"));
            roleManager.Create(new ApplicationRole("Eliminar Recetarios"));
            roleManager.Create(new ApplicationRole("Eliminar Profesiones"));
            roleManager.Create(new ApplicationRole("Eliminar RolesAdmin"));
            roleManager.Create(new ApplicationRole("Eliminar Sexos"));
            roleManager.Create(new ApplicationRole("Eliminar Sintomas"));
            roleManager.Create(new ApplicationRole("Eliminar Tallas"));
            roleManager.Create(new ApplicationRole("Eliminar TiposDireccion"));
            roleManager.Create(new ApplicationRole("Eliminar TiposEspecialidad"));
            roleManager.Create(new ApplicationRole("Eliminar TiposIdentificacion"));
            roleManager.Create(new ApplicationRole("Eliminar TiposSangre"));
            roleManager.Create(new ApplicationRole("Eliminar Tratamiento"));
            //////////////////////////////////////////////////////


            var newGroup = new ApplicationGroup("Archivo", "Ver consultas, pacientes, Administrar Archivos");
            groupManager.CreateGroup(newGroup);
            groupManager.SetGroupRoles(newGroup.Id, "Ver Archivo", "Administrar Archivo");

            newGroup = new ApplicationGroup("Medico", "Antecedentes, Diagnostico,Tratamiento,Consultas");
            groupManager.CreateGroup(newGroup);
            groupManager.SetGroupRoles(newGroup.Id, "Crear antecedente", "Llenar consultas", "Crear diagnostico",
                "Ver antecedente", "Ver consulta",
                "Ver diagnostico", "Ver antecedentes");

            newGroup = new ApplicationGroup("Recepción", "Registrar pacientes, Consultas,Tratamiento,Consultas");
            groupManager.CreateGroup(newGroup);
            groupManager.SetGroupRoles(newGroup.Id, "Crear antecedente", "Crear consultas", "Crear diagnostico",
                "Ver antecedente", "Ver consulta",
                "Ver diagnostico", "Ver antecedentes");

            newGroup = new ApplicationGroup("Administrador", "Crear Usuarios, Crear Grupos, Asignar Permisos.");
            groupManager.CreateGroup(newGroup);
            groupManager.SetGroupRoles(newGroup.Id, "Admin");


            new[]
            {
                "Antecedentes mórbidos", "Antecedentes ginecoobstétricos", "Hábitos",
                "Antecedentes sobre uso de medicamentos",
                "Alergias", "Antecedentes sociales y personales", "Antecedentes familiares", "Inmunizaciones"
            }.ToList()
                .ForEach(
                    @antecedentes => db.CategoriaAntecedentes.Add(new CategoriaAntecedente {Nombre = @antecedentes}));
            /*new string[][]
                       {
                       new string[]{"Antecedente 1","Llenado de antecedente","1"},new string[]{"Antecedente 2","Llenado de antecedente","2"},
                       }.ToList().ForEach(@name => db.Antecedentes.Add(new Antecedente { Nombre = @name[0], Descripcion = @name[1], CategoriaId =  Int32.Parse(@name[2])}));*/

            //ARCHIVOS
            new[]
            {
                "Medicina General", "Medicina Recetada", "Medicina"
            }.ToList().ForEach(@name => db.AreaRecetarios.Add(new AreaRecetario {Nombre = @name}));

            //AREAS PRODUCTO
            new[]
            {
                new[] {"Archivo 1", "Secretaria"}, new[] {"Archivo 2", "recepcion medico"}
            }.ToList().ForEach(@name => db.Archivos.Add(new Archivo {Nombre = @name[0], Localizacion = @name[1]}));

            //CATEGORIA DIAGNOSTICO
            new[]
            {
                "Diagnostico diferencial", "Diagnostico clinico", "Diagnostico patologico"
            }.ToList().ForEach(@name => db.CategoriaDiagnosticos.Add(new CategoriaDiagnostico {Nombre = @name}));

            new[]
            {
                "Tratamiento curativo", "Tratamiento específico", "Tratamiento conservador", "Tratamiento alternativo",
                "Tratamiento radical", "Tratamiento sintomático"
            }.ToList().ForEach(@name => db.CategoriaTratamientos.Add(new CategoriaTratamiento {Nombre = @name}));


            //Llenado de deparamentos
            new[]
            {
                "Guatemala", "Alta Verapaz", "Baja Verapaz", "Chimaltenango", "Chiquimula", "Petén", "El Progreso",
                "Quiché", "Escuintla",
                "Huehuetenango", "Izabal", "Jalapa", "Jutiapa", "Quetzaltenango", "Retalhuleu", "Sacatepéquez",
                "San Marcos", "Santa Rosa",
                "Sololá", "Suchitepéquez", "Totonicapán", "Zacapa", "Otros"
            }.ToList().ForEach(@name => db.Departamentos.Add(new Departamento {Nombre = @name}));

            //Llenando catalogo tipo sangre
            new[]
            {
                "AB+", "AB-", "A+", "A-", "B+", "B-", "O+", "O-"
            }.ToList().ForEach(@name => db.TiposSangre.Add(new TipoSangre {Nombre = @name}));

            //Llenando catalogo estado civil
            new[]
            {
                "Soltero/a", "Casado/a", "Divorciado/a", "Viudo/a"
            }.ToList().ForEach(@name => db.EstadosCiviles.Add(new EstadoCivil {Nombre = @name}));

            //llenando catalogo de nacionalidad
            new[]
            {
                "guatemalteco/a", "salvadoreño/a", "hondureño/a", "nicaragüense/a", "mexicano/a"
            }.ToList().ForEach(@name => db.Nacionalidades.Add(new Nacionalidad {Nombre = @name}));

            //llenandoo catalogo de sexo
            new[]
            {
                "Masculino", "Femenino"
            }.ToList().ForEach(@name => db.Sexos.Add(new Sexo {Nombre = @name}));

            //llenando catalogo tipo identificacion
            new[]
            {
                "DPI", "Cedula", "Pasaporte"
            }.ToList().ForEach(@name => db.TiposIdentificacion.Add(new TipoIdentificacion {Nombre = @name}));

            //llenando catalogo de profesiones--> falta
            new[]
            {
                "Estudiante", "Estudiante Universitario", "Otro"
            }.ToList().ForEach(@name => db.Profesiones.Add(new Profesion {Nombre = @name}));

            //llenando catalogo de talla 
            new[]
            {
                "XS", "S", "M", "L", "XL"
            }.ToList().ForEach(@name => db.Tallas.Add(new Talla {Nombre = @name}));

            //llenando catalogo de municipio
            new[]
            {
                //1-Guatemala. Municipios:
                new[] {"Guatemala", "1"}, new[] {"Santa Catarina Pinula", "1"}, new[] {"San José Pinula", "1"},
                new[] {"San José del Golfo", "1"}, new[] {"Palencia", "1"}, new[] {"Chinautla", "1"},
                new[] {"San Pedro Ayampuc", "1"}, new[] {"Mixco", "1"}, new[] {"San Pedro Sacatepequez", "1"},
                new[] {"San Juan Sacatepequez", "1"}, new[] {"San Raymundo", "1"}, new[] {"Chuarrancho", "1"},
                new[] {"Fraijanes", "1"}, new[] {"Amatitlán", "1"}, new[] {"Villa Nueva", "1"},
                new[] {"Villa Canales", "1"}, new[] {"San Miguel Petapa", "1"},
                //2-Alta Verapaz.
                new[] {"Cobán", "2"}, new[] {"Santa Cruz Verapaz", "2"}, new[] {"San Cristobal Verapaz", "2"},
                new[] {"Tactíc", "2"}, new[] {"Tamahú", "2"}, new[] {"San Miguel Tucurú", "2"},
                new[] {"Panzos", "2"}, new[] {"Senahú", "2"}, new[] {"San Pedro Carchá", "2"},
                new[] {"SanJuan Chamelco", "2"}, new[] {"Lanquín", "2"}, new[] {"Santa María Cahabón", "2"},
                new[] {"Chisec", "2"}, new[] {"Chahal", "2"}, new[] {"Fray Bartolomé de las Casas", "2"},
                new[] {"Santa Catarina La Tinta", "2"},
                //3-Baja Verapaz.
                new[] {"Salamá", "3"}, new[] {"San Miguel Chicaj", "3"}, new[] {"Rabinal", "3"}, new[] {"Cubulco", "3"},
                new[] {"Granados", "3"},
                new[] {"Santa Cruz El Chol", "3"}, new[] {"San Jerónimo", "3"}, new[] {"Purulhá", "3"},
                //4-Chimaltenango:
                new[] {"Chimaltenango", "4"}, new[] {"San José Poaquil", "4"}, new[] {"San Martín Jilotepeque", "4"},
                new[] {"San Juan Comalapa", "4"}, new[] {"Santa Apolonia", "4"}, new[] {"Tecpán Guatemala", "4"},
                new[] {"Patzun", "4"}, new[] {"San Miguel Pochuta", "4"}, new[] {"Patzicia", "4"},
                new[] {"Santa Cruz Balanyá", "4"}, new[] {"Acatenango", "4"}, new[] {"San Pedro Yepocapa", "4"},
                new[] {"San Andrés Itzapa", "4"}, new[] {"Parramos", "4"}, new[] {"Zaragoza", "4"},
                new[] {"El Tejar", "4"},
                //5-Chiquimula
                new[] {"Chiquimula", "5"}, new[] {"San José La Arada", "5"}, new[] {"San Juan Hermita", "5"},
                new[] {"Jocotán", "5"}, new[] {"Camotán", "5"}, new[] {"Olopa", "5"}, new[] {"Esquipulas", "5"},
                new[] {"Concepción Las Minas", "5"}, new[] {"Quezaltepeque", "5"},
                new[] {"San Jacinto", "5"}, new[] {"Ipala", "5"},
                //6-El Petén
                new[] {"Flores", "6"}, new[] {"San José", "6"}, new[] {"San Benito", "6"}, new[] {"San Andrés", "6"},
                new[] {"La Libertad", "6"}, new[] {"San Francisco", "6"}, new[] {"Santa Ana", "6"},
                new[] {"Dolores", "6"}, new[] {"San Luis", "6"}, new[] {"Sayaxche", "6"},
                new[] {"Melchor de Mencos", "6"}, new[] {"Poptún", "6"},
                //7-El Progreso.
                new[] {"Guastatoya", "7"}, new[] {"Morazán", "7"}, new[] {"San Agustín Acasaguastlan", "7"},
                new[] {"San Cristóbal Acasaguastlan", "7"}, new[] {"El Jícaro", "7"}, new[] {"Sansare", "7"},
                new[] {"Sanarate", "7"}, new[] {"San Antonio La Paz", "7"},
                //8-El Quiché. Municipios:
                new[] {"Santa Cruz del Quiche", "8"}, new[] {"Chiche", "8"}, new[] {"Chinique", "8"},
                new[] {"Zacualpa", "8"},
                new[] {"Chajul", "8"}, new[] {"Santo Tomás Chichicstenango", "8"}, new[] {"Patzité", "8"},
                new[] {"San Antonio Ilotenango", "8"}, new[] {"San Pedro Jocopilas", "8"}, new[] {"Cunén", "8"},
                new[] {"San Juan Cotzal", "8"}, new[] {"Joyabaj", "8"}, new[] {"Santa María Nebaj", "8"},
                new[] {"San Andrés Sajcabajá", "8"}, new[] {"San Miguel Uspatán", "8"}, new[] {"Sacapulas", "8"},
                new[] {"San Bartolomé Jocotenango", "8"}, new[] {"Canilla", "8"}, new[] {"Chicaman", "8"},
                new[] {"Playa Grnade - Ixcán", "8"}, new[] {"Pachalúm", "8"},
                //9-Escuintla. Municipios:
                new[] {"Escuintla", "9"}, new[] {"Santa Lucía Cotzumalguapa", "9"}, new[] {"La Democracia", "9"},
                new[] {"Siquinalá", "9"}, new[] {"Masagua", "9"}, new[] {"Pueblo Nuevo Tiquisate", "9"},
                new[] {"La Gomera", "9"}, new[] {"Guanagazapa", "9"}, new[] {"Puerto de San José", "9"},
                new[] {"Iztapa", "9"}, new[] {"Palín", "9"}, new[] {"San Vicente Pacaya", "9"},
                new[] {"Nueva Concepción", "9"},

                //10-Huehuetenango. Municipios:
                new[] {"Huehuetenango", "10"}, new[] {"Chiantla", "10"}, new[] {"Malacatancito", "10"},
                new[] {"Cuilco", "10"}, new[] {"Nentón", "10"}, new[] {"San Pedro Necta", "10"},
                new[] {"Jacaltenango", "10"}, new[] {"San Pedro Soloma", "10"},
                new[] {"San Ildelfonso Ixtahuac´n", "10"},
                new[] {"Santa Bárbara", "10"}, new[] {"La Libertad", "10"}, new[] {"La Democracia", "10"},
                new[] {"San Miguel Acatán", "10"}, new[] {"San Rafael La Independencia", "10"},
                new[] {"Todos Santos Chuchcumatán", "10"}, new[] {"San Juan Atitán", "10"},
                new[] {"Santa Eulalia", "10"}, new[] {"San Mateo Ixtatán", "10"}, new[] {"Colotenango", "10"},
                new[] {"San Sebastián Huehuetenango", "10"}, new[] {"Tectitán", "10"},
                new[] {"Concepción Huista", "10"}, new[] {"San Juan Ixcoy", "10"}, new[] {"San Antonio Huista", "10"},
                new[] {"San Sebastián Coatán", "10"}, new[] {"Santa Cruz Barillas", "10"}, new[] {"Aguacatán", "10"},
                new[] {"San Rafael Petzal", "10"}, new[] {"San Gaspar Ixchil", "10"},
                new[] {"Santiago Chimaltenango", "10"},
                new[] {"Santa Ana Huista", "10"},
                //11-Izabal. Municipios:
                new[] {"Puerto Barrios", "11"}, new[] {"Livingston", "11"}, new[] {"El Estor", "11"},
                new[] {"Morales", "11"}, new[] {"Los Amates", "11"},
                //12-Jalapa. Municipios:
                new[] {"Jalapa", "12"}, new[] {"San Pedro Pinula", "12"}, new[] {"San Luis Jilotepeque", "12"},
                new[] {"San Manuel Chaparrón", "12"}, new[] {"San Carlos Alzatate", "12"}, new[] {"Monjas", "12"},
                new[] {"Mataquescuintla", "12"},
                //13-Jutiapa. Municipios:
                new[] {"Jutiapa", "13"}, new[] {"El Progreso", "13"}, new[] {"Santa Catarina Mita", "13"},
                new[] {"Agua Blanca", "13"}, new[] {"Asunción Mita", "13"}, new[] {"Yupiltepeque", "13"},
                new[] {"Atescatempa", "13"}, new[] {"Jerez", "13"}, new[] {"El Adelanto", "13"},
                new[] {"Zapotitlán", "13"}, new[] {"Comapa", "13"}, new[] {"Jalpatagua", "13"},
                new[] {"Conguaco", "13"}, new[] {"Moyuta", "13"}, new[] {"Pasaco", "13"},
                new[] {"San José Acatempa", "13"}, new[] {"Quezada", "13"},
                //14-Quetzaltenango. Municipios:
                new[] {"Quetzaltenango", "14"}, new[] {"Salcajá", "14"}, new[] {"Olintepeque", "14"},
                new[] {"San Carlos Sija", "14"}, new[] {"Sibilia", "14"}, new[] {"Cabrican", "14"},
                new[] {"Cajola", "14"}, new[] {"San Miguel Siguilça", "14"}, new[] {"San Juan Ostuncalco", "14"},
                new[] {"San Mateo", "14"}, new[] {"Concepción Chiquirichapa", "14"},
                new[] {"San Martín Sacatepequez", "14"},
                new[] {"Almolonga", "14"}, new[] {"Cantel", "14"}, new[] {"Huitán", "14"}, new[] {"Zunil", "14"},
                new[] {"Colomba", "14"}, new[] {"San Francisco La Unión", "14"}, new[] {"El Palmar", "14"},
                new[] {"Coatepeque", "14"}, new[] {"Génova", "14"}, new[] {"Flores Costa Cuca", "14"},
                new[] {"La Esperanza", "14"}, new[] {"Palestina de los Altos", "14"},
                //15-Retalhuleu. Municipios:
                new[] {"Retalhuelu", "15"}, new[] {"San Sebastián", "15"}, new[] {"Santa Cruz Mulúa", "15"},
                new[] {"San Martín Zapotitlán", "15"}, new[] {"San Felipe Retalhuleu", "15"},
                new[] {"San Andrés Villa Seca", "15"}, new[] {"Champerico", "15"},
                new[] {"Nuevo San Carlos", "15"}, new[] {"El Asintal", "15"},
                //16-Sacatepéquez. Municipios:
                new[] {"Antigua Guatemala", "16"}, new[] {"Jocotenango", "16"}, new[] {"Pastores", "16"},
                new[] {"Sumpango", "16"}, new[] {"Santo Domingo Xenacoj", "16"},
                new[] {"Santiago Sacatepequez", "16"}, new[] {"San Bartolomé Milpas Altas", "16"},
                new[] {"San Lucas Sacatepequez", "16"}, new[] {"Santa Lucía Milpas Altas", "16"},
                new[] {"Magdalena Milpas Altas", "16"}, new[] {"Santa María de Jesús", "16"},
                new[] {"Ciudad Vieja", "16"}, new[] {"San Miguel Dueñas", "16"}, new[] {"San Juan Alotenango", "16"},
                new[] {"San Antonio Aguas Calientes", "16"}, new[] {"Santa Catarina Barahona", "16"},
                //17-San Marcos. Municipios:
                new[] {"San Marcos", "17"}, new[] {"San Pedro Sacatepéquez", "17"}, new[] {"Comitancillo", "17"},
                new[] {"San Antonio Sacatepéquez", "17"}, new[] {"San Miguel Ixtahuacan", "17"},
                new[] {"Concepción Tutuapa", "17"}, new[] {"Tacaná", "17"}, new[] {"Sibinal", "17"},
                new[] {"Tajumulco", "17"}, new[] {"Tejutla", "17"}, new[] {"San Rafael Pié de la Cuesta", "17"},
                new[] {"Nuevo Progreso", "17"}, new[] {"El Tumbador", "17"}, new[] {"San José El Rodeo", "17"},
                new[] {"Malacatán", "17"}, new[] {"Catarina", "17"}, new[] {"Ayutla", "17"}, new[] {"Ocos", "17"},
                new[] {"San Pablo", "17"}, new[] {"El Quetzal", "17"}, new[] {"La Reforma", "17"},
                new[] {"Pajapita", "17"}, new[] {"Ixchiguan", "17"}, new[] {"San José Ojetenán", "17"},
                new[] {"San Cristóbal Cucho", "17"}, new[] {"Sipacapa", "17"},
                new[] {"Esquipulas Palo Gordo", "17"}, new[] {"Río Blanco", "17"}, new[] {"San Lorenzo", "17"},
                //18-Santa Rosa. Municipios:
                new[] {"Cuilapa", "18"}, new[] {"Berberena", "18"}, new[] {"San Rosa de Lima", "18"},
                new[] {"Casillas", "18"},
                new[] {"San Rafael Las Flores", "18"}, new[] {"Oratorio", "18"}, new[] {"San Juan Tecuaco", "18"},
                new[] {"Chiquimulilla", "18"}, new[] {"Taxisco", "18"}, new[] {"Santa María Ixhuatan", "18"},
                new[] {"Guazacapán", "18"}, new[] {"Santa Cruz Naranjo", "18"}, new[] {"Pueblo Nuevo Viñas", "18"},
                new[] {"Nueva Santa Rosa", "18"},
                //19-Sololá. Municipios:
                new[] {"Sololá", "19"}, new[] {"San José Chacaya", "19"}, new[] {"Santa María Visitación", "19"},
                new[] {"Santa Lucía Utatlán", "19"}, new[] {"Nahualá", "19"}, new[] {"Santa Catarina Ixtahuacán", "19"},
                new[] {"Santa Clara La Laguna", "19"}, new[] {"Concepción", "19"}, new[] {"San Andrés Semetabaj", "19"},
                new[] {"Panajachel", "19"}, new[] {"Santa Catarina Palopó", "19"}, new[] {"San Antonio Palopó", "19"},
                new[] {"San Lucas Tolimán", "19"}, new[] {"Santa Cruz La Laguna", "19"},
                new[] {"San Pablo La Laguna", "19"},
                new[] {"San Marcos La Laguna", "19"}, new[] {"San Juan La Laguna", "19"},
                new[] {"San Pedro La Laguna", "19"},
                new[] {"Santiago Atitlán", "19"},
                //20-Suchitepéquez. Municipios:
                new[] {"Mazatenango", "20"}, new[] {"Cuyotenango", "20"}, new[] {"San Francisco Zapotitlán", "20"},
                new[] {"San Bernardino", "20"}, new[] {"San José El Ídolo", "20"},
                new[] {"Santo Domingo Suchitepequez", "20"},
                new[] {"San Lorenzo", "20"}, new[] {"Samayac", "20"}, new[] {"San Pablo Jocopilas", "20"},
                new[] {"San Antonio Suchitepéquez", "20"}, new[] {"San Miguel Panán", "20"}, new[] {"San Gabriel", "20"},
                new[] {"Chicacao", "20"}, new[] {"Patulul", "20"}, new[] {"Santa Bárbara", "20"},
                new[] {"San Juan Bautista", "20"},
                new[] {"Santo Tomás La Unión", "20"}, new[] {"Zunilito", "20"},
                new[] {"Pueblo Nuevo Suchitepéquez", "20"},
                new[] {"Río Bravo", "20"},
                //21-Totonicapán. Municipios:
                new[] {"Totonicapán", "21"}, new[] {"San Cristóbal Totonicapán", "21"},
                new[] {"San Francisco El Alto", "21"},
                new[] {"San Andrés Xecul", "21"}, new[] {"Momostenango", "21"}, new[] {"Santa María Chiquimula", "21"},
                new[] {"Santa Lucía La Reforma", "21"}, new[] {"San Bartolo Aguas Calientes", "21"},
                //22-Zacapa. Municipios:
                new[] {"Zacapa", "22"}, new[] {"Estanzuela", "22"}, new[] {"Río Hondo", "22"}, new[] {"Gualán", "22"},
                new[] {"Teculután", "22"}, new[] {"Usumatlán", "22"}, new[] {"Cabañas", "22"}, new[] {"San Diego", "22"},
                new[] {"La Unión", "22"}, new[] {"Huite", "22"}
            }.ToList()
                .ForEach(
                    @name => db.Municipios.Add(new Municipio {Nombre = @name[0], DepartamentoId = int.Parse(@name[1])}));

            //ToList().ForEach(@name => db.Municipios.Add(new Municipio { Nombre = @name, DepartamentoId = 1 }));
            var tipoespecialidad = db.TiposEspecialidad.Add(new TipoEspecialidad {Nombre = "Todo en Salud"});
            db.Especialidades.Add(new Especialidad {Nombre = "Para la mujer", TipoEspecialidad = tipoespecialidad});
            db.Especialidades.Add(new Especialidad {Nombre = "Odontologia", TipoEspecialidad = tipoespecialidad});
            db.Especialidades.Add(new Especialidad {Nombre = "Pediatria", TipoEspecialidad = tipoespecialidad});
            db.Especialidades.Add(new Especialidad {Nombre = "Clinicas de los ojos", TipoEspecialidad = tipoespecialidad});
            db.Especialidades.Add(new Especialidad {Nombre = "Nutricionista", TipoEspecialidad = tipoespecialidad});
            db.Especialidades.Add(new Especialidad {Nombre = "Psicologia", TipoEspecialidad = tipoespecialidad});
            db.Especialidades.Add(new Especialidad {Nombre = "Flebologia", TipoEspecialidad = tipoespecialidad});
            db.Especialidades.Add(new Especialidad {Nombre = "Gastroenterologia", TipoEspecialidad = tipoespecialidad});

            tipoespecialidad = db.TiposEspecialidad.Add(new TipoEspecialidad {Nombre = "Medical spa"});
            db.Especialidades.Add(new Especialidad {Nombre = "Depilacion Definitiva", TipoEspecialidad = tipoespecialidad});
            db.Especialidades.Add(new Especialidad {Nombre = "Eliminacion de tatuajes", TipoEspecialidad = tipoespecialidad});
            db.Especialidades.Add(new Especialidad {Nombre = "Tratamientos laser", TipoEspecialidad = tipoespecialidad});
            db.Especialidades.Add(new Especialidad {Nombre = "Eliminacion de Hongos en las uñas", TipoEspecialidad = tipoespecialidad});
            db.Especialidades.Add(new Especialidad {Nombre = "Eliminacion de arañitas", TipoEspecialidad = tipoespecialidad});
            db.Especialidades.Add(new Especialidad {Nombre = "Acne y manchas", TipoEspecialidad = tipoespecialidad});
            db.Especialidades.Add(new Especialidad {Nombre = "Microdermoabracion y Fotorejuvenecimiento", TipoEspecialidad = tipoespecialidad});
            db.Especialidades.Add(new Especialidad {Nombre = "Peeling clinico", TipoEspecialidad = tipoespecialidad});
            db.Especialidades.Add(new Especialidad {Nombre = "Limpiezas faciales", TipoEspecialidad = tipoespecialidad});
            db.Especialidades.Add(new Especialidad {Nombre = "Luz led (regenerador de piel", TipoEspecialidad = tipoespecialidad});
            db.Especialidades.Add(new Especialidad {Nombre = "Pedicure clinico", TipoEspecialidad = tipoespecialidad});
            db.Especialidades.Add(new Especialidad {Nombre = "Manicure clinico y estetico", TipoEspecialidad = tipoespecialidad});
            db.Especialidades.Add(new Especialidad {Nombre = "Desintoxicacion corporal", TipoEspecialidad = tipoespecialidad});
            db.Especialidades.Add(new Especialidad {Nombre = "Masajes relajantes", TipoEspecialidad = tipoespecialidad});
            db.Especialidades.Add(new Especialidad {Nombre = "Sillon relajante", TipoEspecialidad = tipoespecialidad});
            db.Especialidades.Add(new Especialidad {Nombre = "Camillas de termoterapia", TipoEspecialidad = tipoespecialidad});
            db.Especialidades.Add(new Especialidad {Nombre = "Oxiginador y relajante cerebral", TipoEspecialidad = tipoespecialidad});
            db.Especialidades.Add(new Especialidad {Nombre = "Metodos reductores", TipoEspecialidad = tipoespecialidad});
            db.Especialidades.Add(new Especialidad {Nombre = "Cavitacion", TipoEspecialidad = tipoespecialidad});
            db.Especialidades.Add(new Especialidad {Nombre = "Mesoterapia", TipoEspecialidad = tipoespecialidad});
            db.Especialidades.Add(new Especialidad {Nombre = "Masaje reductor", TipoEspecialidad = tipoespecialidad});

            tipoespecialidad = db.TiposEspecialidad.Add(new TipoEspecialidad {Nombre = "Centro de Diagnostico"});
            db.Especialidades.Add(new Especialidad {Nombre = "Rayos X", TipoEspecialidad = tipoespecialidad});
            db.Especialidades.Add(new Especialidad {Nombre = "Mamografia", TipoEspecialidad = tipoespecialidad});
            db.Especialidades.Add(new Especialidad {Nombre = "Colposcopia", TipoEspecialidad = tipoespecialidad});
            db.Especialidades.Add(new Especialidad {Nombre = "Audiometria", TipoEspecialidad = tipoespecialidad});
            db.Especialidades.Add(new Especialidad {Nombre = "Holter", TipoEspecialidad = tipoespecialidad});
            db.Especialidades.Add(new Especialidad {Nombre = "Ecocardiograma", TipoEspecialidad = tipoespecialidad});
            db.Especialidades.Add(new Especialidad {Nombre = "Electrocardiograma", TipoEspecialidad = tipoespecialidad});
            db.Especialidades.Add(new Especialidad {Nombre = "Electroencefalograma", TipoEspecialidad = tipoespecialidad});
            db.Especialidades.Add(new Especialidad {Nombre = "Ultrasonido:Pelvico/Obstetrico/General", TipoEspecialidad = tipoespecialidad});
            db.Especialidades.Add(new Especialidad {Nombre = "Endoscopia", TipoEspecialidad = tipoespecialidad});
            db.Especialidades.Add(new Especialidad {Nombre = "Dopler venoso", TipoEspecialidad = tipoespecialidad});
            db.Especialidades.Add(new Especialidad {Nombre = "Ultrasondo de mamas", TipoEspecialidad = tipoespecialidad});

            tipoespecialidad = db.TiposEspecialidad.Add(new TipoEspecialidad {Nombre = "Centro del Diabetico"});
            db.Especialidades.Add(new Especialidad {Nombre = "Control de glucosa", TipoEspecialidad = tipoespecialidad});
            db.Especialidades.Add(new Especialidad {Nombre = "Dietas nutricional adecuada", TipoEspecialidad = tipoespecialidad});
            db.Especialidades.Add(new Especialidad {Nombre = "Control de la vista", TipoEspecialidad = tipoespecialidad});
            db.Especialidades.Add(new Especialidad {Nombre = "Pie diabetico", TipoEspecialidad = tipoespecialidad});
            db.Especialidades.Add(new Especialidad {Nombre = "Tratamientos", TipoEspecialidad = tipoespecialidad});
            db.Especialidades.Add(new Especialidad {Nombre = "Glucometro", TipoEspecialidad = tipoespecialidad});
            db.Especialidades.Add(new Especialidad {Nombre = "Alimentos para diabeticos", TipoEspecialidad = tipoespecialidad});

            var estado = db.EstadoConsultas.Add(new EstadoConsulta {Nombre = "Activo"});
            db.EstadoConsultas.Add(new EstadoConsulta {Nombre = "Atendiendo"});
            db.EstadoConsultas.Add(new EstadoConsulta {Nombre = "Congelado"});
            db.EstadoConsultas.Add(new EstadoConsulta {Nombre = "Finalizado"});
            db.EstadoConsultas.Add(new EstadoConsulta {Nombre = "Inactivo"});

            // Datos Generales de la Clínica
            const string nombreClinica = "Eventos Católicos";
            const string direccionClinica = "10ma Avenida A 2-43 Zona 1";
            const string telefonoClinica = "2382-0202";
            const string correoClinica = "info@todoensalud.com.gt";

            db.ClinicaGeneral.Add(new ClinicaGeneral
            {
                Nombre = nombreClinica,
                Direccion = direccionClinica,
                CorreoElectronico = correoClinica,
                Telefono = telefonoClinica
            });


            // Usuario con todos los permisos (IT)
            const string primerNombre = "Oscar";
            const string segundoNombre = "Estuardo";
            const string primerApellido = "Gil";
            const string segundoApellido = "Sanchez";
            const string email = "oscaresgil@gmail.com";
            const string userName = "oscaresgil";
            const string password = "Eventos123";
            const string roleName = "IT";

            var role = new ApplicationRole(roleName);
            roleManager.Create(role);

            var applicationUser = new ApplicationUser
            {
                PrimerNombre = primerNombre,
                SegundoNombre = segundoNombre,
                PrimerApellido = primerApellido,
                SegundoApellido = segundoApellido,
                UserName = userName,
                Email = email
            };
            userManager.Create(applicationUser, password);
            userManager.SetLockoutEnabled(applicationUser.Id, false);

            newGroup = new ApplicationGroup("IT", "Acceso total (Grupos, roles, logs).");
            groupManager.CreateGroup(newGroup);
            groupManager.SetUserGroups(applicationUser.Id, newGroup.Id);
            groupManager.SetGroupRoles(newGroup.Id, roleName);

            // Datos de Prueba
            var jose =
                db.Pacientes.Add(new Paciente
                {
                    PrimerNombre = "Jose",
                    PrimerApellido = "Lopez",
                    FechaNacimiento = DateTime.Today
                });

            var clinica1 = db.Clinicas.Add(new Clinica{Nombre = "Clinica 1"});
            db.Clinicas.Add(new Clinica{Nombre = "Zona 5"});

            db.Medicos.Add(new Medico {PrimerNombre = "No asignado", PrimerApellido = "N"});
            var medico =
                db.Medicos.Add(new Medico
                {
                    PrimerNombre = "Oscar",
                    PrimerApellido = "Gil",
                    ApplicationUser = applicationUser
                });

            db.Consultas.Add(new Consulta
            {
                Paciente = jose,
                Medico = medico,
                EstadoConsulta = estado,
                Clinica = clinica1,
                FechaConsulta = DateTime.Today,
                EstadoArchivo = 0,
                ApplicationUser = applicationUser,
                ApplicationUser1 = applicationUser,
                CreatedAt = DateTime.Now
            });

            db.Sintomas.Add(new Sintoma {Nombre = "Sintoma1"});
            /*db.Antecedentes.Add(new Antecedente {Nombre = "Antecedente1", CategoriaId = 1});
            db.Diagnosticos.Add(new Diagnostico {Nombre = "Diagnostico1", CategoriaId = 1});
            db.Tratamientos.Add(new Tratamiento {Nombre = "Tratamiento1", CategoriaId = 1});
            db.Recetarios.Add(new Recetario {Nombre = "Recetario1"});
            db.Examenes.Add(new Examen {Nombre = "Examen1", EspecialidadId = 1});*/
        }
    }

    // Configure el administrador de inicios de sesión que se usa en esta aplicación.
    public class ApplicationSignInManager : SignInManager<ApplicationUser, string>
    {
        public ApplicationSignInManager(ApplicationUserManager userManager, IAuthenticationManager authenticationManager)
            : base(userManager, authenticationManager)
        {
        }

        public override Task<ClaimsIdentity> CreateUserIdentityAsync(ApplicationUser user)
        {
            return user.GenerateUserIdentityAsync((ApplicationUserManager) UserManager);
        }

        public static ApplicationSignInManager Create(IdentityFactoryOptions<ApplicationSignInManager> options,
            IOwinContext context)
        {
            return new ApplicationSignInManager(context.GetUserManager<ApplicationUserManager>(), context.Authentication);
        }
    }
}