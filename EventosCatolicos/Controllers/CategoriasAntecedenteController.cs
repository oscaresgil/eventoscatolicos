﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using EventosCatolicos.Models;

namespace EventosCatolicos.Controllers
{
    public class CategoriasAntecedenteController : Controller
    {
        private readonly ApplicationDbContext _db = new ApplicationDbContext();

        // GET: CategoriasAntecedente
        [Authorize(Roles = "Ver CategoriaAntecedentes,IT")]
        public ActionResult Index()
        {
            return View(_db.CategoriaAntecedentes.ToList());
        }

        // GET: CategoriasAntecedente/Details/5
        [Authorize(Roles = "Ver CategoriaAntecedentes,IT")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CategoriaAntecedente categoriaAntecedente = _db.CategoriaAntecedentes.Find(id);
            if (categoriaAntecedente == null)
            {
                return HttpNotFound();
            }
            return View(categoriaAntecedente);
        }

        // GET: CategoriasAntecedente/Create
        [Authorize(Roles = "Administrar CategoriaAntecedentes,IT")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: CategoriasAntecedente/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrar CategoriaAntecedentes,IT")]
        public ActionResult Create([Bind(Include = "Id,Nombre")] CategoriaAntecedente categoriaAntecedente)
        {
            if (ModelState.IsValid)
            {
                _db.CategoriaAntecedentes.Add(categoriaAntecedente);
                _db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(categoriaAntecedente);
        }

        // GET: CategoriasAntecedente/Edit/5
        [Authorize(Roles = "Administrar CategoriaAntecedentes,IT")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CategoriaAntecedente categoriaAntecedente = _db.CategoriaAntecedentes.Find(id);
            if (categoriaAntecedente == null)
            {
                return HttpNotFound();
            }
            return View(categoriaAntecedente);
        }

        // POST: CategoriasAntecedente/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrar CategoriaAntecedentes,IT")]
        public ActionResult Edit([Bind(Include = "Id,Nombre")] CategoriaAntecedente categoriaAntecedente)
        {
            if (ModelState.IsValid)
            {
                _db.Entry(categoriaAntecedente).State = EntityState.Modified;
                _db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(categoriaAntecedente);
        }

        // GET: CategoriasAntecedente/Delete/5
        [Authorize(Roles = "Eliminar CategoriaAntecedentes,IT")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CategoriaAntecedente categoriaAntecedente = _db.CategoriaAntecedentes.Find(id);
            if (categoriaAntecedente == null)
            {
                return HttpNotFound();
            }
            return View(categoriaAntecedente);
        }

        // POST: CategoriasAntecedente/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Eliminar CategoriaAntecedentes,IT")]
        public ActionResult DeleteConfirmed(int id)
        {
            CategoriaAntecedente categoriaAntecedente = _db.CategoriaAntecedentes.Find(id);
            _db.CategoriaAntecedentes.Remove(categoriaAntecedente);
            _db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
