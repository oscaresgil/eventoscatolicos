﻿using Microsoft.AspNet.Identity.Owin;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using EventosCatolicos.Models;
using EventosCatolicos.ViewModels;

namespace EventosCatolicos.Controllers
{
    [Authorize(Roles = "IT,Admin")]
    public class RolesAdminController : Controller
    {
        public RolesAdminController()
        {
        }

        public RolesAdminController(ApplicationRoleManager roleManager, ApplicationGroupManager groupManager)
        {
            RoleManager = roleManager;
            GroupManager = groupManager;
        }

        private ApplicationRoleManager _roleManager;
        public ApplicationRoleManager RoleManager
        {
            get
            {
                return _roleManager ?? HttpContext.GetOwinContext().Get<ApplicationRoleManager>();
            }
            private set
            {
                _roleManager = value;
            }
        }

        private ApplicationGroupManager _groupManager;
        public ApplicationGroupManager GroupManager
        {
            get
            {
                return _groupManager ?? HttpContext.GetOwinContext().Get<ApplicationGroupManager>();
            }
            private set
            {
                _groupManager = value;
            }
        }

        //
        // GET: /Roles/
        public ActionResult Index()
        {
            //return View(GroupManager.Groups.ToList());
            var rolesList = RoleManager.Roles.Select(
                @role => new ApplicationRoleViewModel
                {
                    Id = @role.Id,
                    Name = @role.Name
                });
            return View(rolesList);
        }

        //
        // GET: /Roles/Details/5
        public async Task<ActionResult> Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var applicationRole = await RoleManager.FindByIdAsync(id);
            if (applicationRole == null)
            {
                return HttpNotFound("No se encontró el permiso");
            }
            var applicationGroupRolesViewModel = new GroupsInRoleViewModel
            {
                Id = applicationRole.Id,
                Name = applicationRole.Name,
                GroupsList = await GroupManager.GetGroupsInRoleAsync(id)
            };
            applicationGroupRolesViewModel.Count = applicationGroupRolesViewModel.GroupsList.Count();
            return View(applicationGroupRolesViewModel);
        }

        
    }
}