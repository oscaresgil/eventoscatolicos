﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using EventosCatolicos.Models;

namespace EventosCatolicos.Controllers
{
    public class TiposEspecialidadController : Controller
    {
        private readonly ApplicationDbContext _db = new ApplicationDbContext();

        // GET: TiposEspecialidad
        [Authorize(Roles = "Ver TiposEspecialidad, IT")]
        public ActionResult Index()
        {
            return View(_db.TiposEspecialidad.ToList());
        }

        // GET: TiposEspecialidad/Details/5
        [Authorize(Roles = "Ver TiposEspecialidad,IT")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TipoEspecialidad tipoEspecialidad = _db.TiposEspecialidad.Find(id);
            if (tipoEspecialidad == null)
            {
                return HttpNotFound();
            }
            return View(tipoEspecialidad);
        }

        // GET: TiposEspecialidad/Create
        [Authorize(Roles = "Administrar TiposEspecialidad,IT")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: TiposEspecialidad/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrar TiposEspecialidad,IT")]
        public ActionResult Create([Bind(Include = "Id,Nombre")] TipoEspecialidad tipoEspecialidad)
        {
            if (ModelState.IsValid)
            {
                _db.TiposEspecialidad.Add(tipoEspecialidad);
                _db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tipoEspecialidad);
        }

        // GET: TiposEspecialidad/Edit/5
        [Authorize(Roles = "Administrar TiposEspecialidad,IT")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TipoEspecialidad tipoEspecialidad = _db.TiposEspecialidad.Find(id);
            if (tipoEspecialidad == null)
            {
                return HttpNotFound();
            }
            return View(tipoEspecialidad);
        }

        // POST: TiposEspecialidad/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrar TiposEspecialidad,IT")]
        public ActionResult Edit([Bind(Include = "Id,Nombre")] TipoEspecialidad tipoEspecialidad)
        {
            if (ModelState.IsValid)
            {
                _db.Entry(tipoEspecialidad).State = EntityState.Modified;
                _db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tipoEspecialidad);
        }

        // GET: TiposEspecialidad/Delete/5
        [Authorize(Roles = "Eliminar TiposEspecialidad,IT")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TipoEspecialidad tipoEspecialidad = _db.TiposEspecialidad.Find(id);
            if (tipoEspecialidad == null)
            {
                return HttpNotFound();
            }
            return View(tipoEspecialidad);
        }

        // POST: TiposEspecialidad/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Eliminar TiposEspecialidad,IT")]
        public ActionResult DeleteConfirmed(int id)
        {
            TipoEspecialidad tipoEspecialidad = _db.TiposEspecialidad.Find(id);
            _db.TiposEspecialidad.Remove(tipoEspecialidad);
            _db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
