﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using EventosCatolicos.Models;

namespace EventosCatolicos.Controllers
{
    public class NacionalidadesController : Controller
    {
        private readonly ApplicationDbContext _db = new ApplicationDbContext();

        // GET: Nacionalidades
        [Authorize(Roles = "Ver Nacionalidades,IT")]
        public ActionResult Index()
        {
            return View(_db.Nacionalidades.ToList());
        }

        // GET: Nacionalidades/Details/5
        [Authorize(Roles = "Ver Nacionalidades,IT")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Nacionalidad nacionalidad = _db.Nacionalidades.Find(id);
            if (nacionalidad == null)
            {
                return HttpNotFound();
            }
            return View(nacionalidad);
        }

        // GET: Nacionalidades/Create
        [Authorize(Roles = "Administrar Nacionalidades,IT")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Nacionalidades/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrar Nacionalidades,IT")]
        public ActionResult Create([Bind(Include = "Id,Nombre")] Nacionalidad nacionalidad)
        {
            if (ModelState.IsValid)
            {
                _db.Nacionalidades.Add(nacionalidad);
                _db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(nacionalidad);
        }

        // GET: Nacionalidades/Edit/5
        [Authorize(Roles = "Administrar Nacionalidades,IT")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Nacionalidad nacionalidad = _db.Nacionalidades.Find(id);
            if (nacionalidad == null)
            {
                return HttpNotFound();
            }
            return View(nacionalidad);
        }

        // POST: Nacionalidades/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrar Nacionalidades,IT")]
        public ActionResult Edit([Bind(Include = "Id,Nombre")] Nacionalidad nacionalidad)
        {
            if (ModelState.IsValid)
            {
                _db.Entry(nacionalidad).State = EntityState.Modified;
                _db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(nacionalidad);
        }

        // GET: Nacionalidades/Delete/5
        [Authorize(Roles = "Eliminar Nacionalidades,IT")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Nacionalidad nacionalidad = _db.Nacionalidades.Find(id);
            if (nacionalidad == null)
            {
                return HttpNotFound();
            }
            return View(nacionalidad);
        }

        // POST: Nacionalidades/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Eliminar Nacionalidades,IT")]
        public ActionResult DeleteConfirmed(int id)
        {
            Nacionalidad nacionalidad = _db.Nacionalidades.Find(id);
            _db.Nacionalidades.Remove(nacionalidad);
            _db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
