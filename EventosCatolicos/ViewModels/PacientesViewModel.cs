﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace EventosCatolicos.ViewModels
{
    public class PacientesViewModel
    {
        [Display(Name = "Código Paciente")]
        public int Id { get; set; }

        [Required]
        [StringLength(256)]
        [Display(Name = "Primer nombre")]
        public string PrimerNombre { get; set; }

        [StringLength(256)]
        [Display(Name = "Segundo nombre")]
        public string SegundoNombre { get; set; }

        [Required]
        [StringLength(256)]
        [Display(Name = "Primer apellido")]
        public string PrimerApellido { get; set; }

        [StringLength(256)]
        [Display(Name = "Segundo apellido")]
        public string SegundoApellido { get; set; }

        [StringLength(256)]
        [Display(Name = "Apellido casada")]
        public string ApellidoCasada { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime FechaNacimiento { get; set; }

        [Display(Name = "Codigo tipo de identificacion")]
        public string TipoIdentificacion { get; set; }

        [StringLength(50)]
        [Display(Name = "No. Identificacion")]
        public string NoIdentificacion { get; set; }

        [Display(Name = "Nacionalidad")]
        public string Nacionalidad { get; set; }

        [Display(Name = "Archivo")]
        public string Archivo { get; set; }

        [Display(Name = "Sexo")]
        public string Sexo { get; set; }

        [Display(Name = "Estado Civíl")]
        public string EstadoCivil { get; set; }

        [StringLength(256)]
        [Display(Name = "Correo electrónico")]
        public string CorreoElectronico { get; set; }
    }

    public class PantallaViewModel
    {
        [Display(Name = "Código Paciente")]
        public int Id { get; set; }

        [Display(Name = "Código Consulta")]
        public int IdConsulta { get; set; }

        [Required]
        [StringLength(256)]
        [Display(Name = "Primer nombre")]
        public string Nombre { get; set; }
        
        [StringLength(256)]
        [Display(Name = "Medico")]
        public string Medico { get; set; }

    }

    public class PantallaClinicaViewModel
    {
        public PantallaClinicaViewModel() 
        {
            Consultas = new List<PantallaViewModel>();
        }

        [Display(Name = "Clínica")]
        public string Clinica { get; set; }

        public IEnumerable<PantallaViewModel> Consultas;
    }

    public class PantallaNewClinicaViewModel
    {

        [Display(Name = "Clinica")]
        public string ClinicaId { get; set; }
    }

    public class PantallaArchivoViewModel : PantallaViewModel
    {

        [Display(Name = "Archivo")]
        public string Archivo { get; set; }

        [Display(Name = "Clinica")]
        public string Clinica { get; set; }
    }
}