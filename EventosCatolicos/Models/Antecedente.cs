namespace EventosCatolicos.Models
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("Antecedente")]
    public class Antecedente
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Antecedente()
        {
            DetallesAntecedentes = new HashSet<DetallesAntecedente>();
        }

        [Required(ErrorMessage = "Debe ingresar un antecedente v�lido")]
        public int Id { get; set; }

        [Required(ErrorMessage = "Por favor ingrese un nombre.")]
        [StringLength(256, ErrorMessage = "Sobrepas� el tama�o m�ximo de caracteres.")]
        public string Nombre { get; set; }

        [StringLength(500, ErrorMessage = "Sobrepas� el tama�o m�ximo de caracteres.")]
        [Display(Name = "Descripci�n")]
        public string Descripcion { get; set; }

        [Display(Name = "Categor�a")]
        public int CategoriaId { get; set; }

        public CategoriaAntecedente CategoriaAntecedente { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public ICollection<DetallesAntecedente> DetallesAntecedentes { get; set; }
    }
}
