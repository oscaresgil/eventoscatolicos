﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using EventosCatolicos.Models;

namespace EventosCatolicos.Controllers
{
    public class AreasRecetarioController : Controller
    {
        private readonly ApplicationDbContext _db = new ApplicationDbContext();

        // GET: AreasRecetario
        [Authorize(Roles = "Ver AreasRecetario,IT")]
        public ActionResult Index()
        {
            return View(_db.AreaRecetarios.ToList());
        }

        // GET: AreasRecetario/Details/5
        [Authorize(Roles = "Ver AreasRecetario,IT")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AreaRecetario areaRecetario = _db.AreaRecetarios.Find(id);
            if (areaRecetario == null)
            {
                return HttpNotFound();
            }
            return View(areaRecetario);
        }

        // GET: AreasRecetario/Create
        [Authorize(Roles = "Administrar AreasRecetario,IT")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: AreasRecetario/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrar AreasRecetario,IT")]
        public ActionResult Create([Bind(Include = "Id,Nombre")] AreaRecetario areaRecetario)
        {
            if (ModelState.IsValid)
            {
                _db.AreaRecetarios.Add(areaRecetario);
                _db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(areaRecetario);
        }

        // GET: AreasRecetario/Edit/5
        [Authorize(Roles = "Administrar AreasRecetario,IT")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AreaRecetario areaRecetario = _db.AreaRecetarios.Find(id);
            if (areaRecetario == null)
            {
                return HttpNotFound();
            }
            return View(areaRecetario);
        }

        // POST: AreasRecetario/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrar AreasRecetario,IT")]
        public ActionResult Edit([Bind(Include = "Id,Nombre")] AreaRecetario areaRecetario)
        {
            if (ModelState.IsValid)
            {
                _db.Entry(areaRecetario).State = EntityState.Modified;
                _db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(areaRecetario);
        }

        // GET: AreasRecetario/Delete/5
        [Authorize(Roles = "Eliminar AreasRecetario,IT")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AreaRecetario areaRecetario = _db.AreaRecetarios.Find(id);
            if (areaRecetario == null)
            {
                return HttpNotFound();
            }
            return View(areaRecetario);
        }

        // POST: AreasRecetario/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Eliminar AreasRecetario,IT")]
        public ActionResult DeleteConfirmed(int id)
        {
            AreaRecetario areaRecetario = _db.AreaRecetarios.Find(id);
            _db.AreaRecetarios.Remove(areaRecetario);
            _db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
