﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventosCatolicos.Servicios
{
    public static class clsOpciones
    {
        private static String sIndice = "Índice";
        private static String sCrear = "Crear";
        private static String sEditar = "Editar";
        private static String sDetalle = "Detalles";
        private static String sEliminar = "Eliminar";
        private static String sTituloCrear = "Crear nuevo";
        private static String sTituloModificar = "Modificar";
        private static String sTituloEliminar = "Eliminar";
        private static String sTituloDetalles = "Detalles";
        private static String sMensajeEliminar = "¿Esta seguro que desea eliminar";
        private static String sMensajeRegresar = "Regresar a la lista";
        private static String sIconoDetalles = " glyphicon glyphicon-list-alt";
        private static String sIconoEliminar = "glyphicon glyphicon-trash";
        private static String sIconoEditar = "glyphicon glyphicon-edit";
        private static String sIconoAtender = "glyphicon glyphicon-ok";
        private static String sIconoEditarAtender = "glyphicon glyphicon-check";

        public static String SIconoDetalles
        {
            get { return clsOpciones.sIconoDetalles; }
            set { clsOpciones.sIconoDetalles = value; }
        }
        public static String SIconoEliminar
        {
            get { return clsOpciones.sIconoEliminar; }
            set { clsOpciones.sIconoEliminar = value; }
        }
        public static String SIconoEditar
        {
            get { return clsOpciones.sIconoEditar; }
            set { clsOpciones.sIconoEditar = value; }
        }
        public static String SIconoAtender
        {
            get { return clsOpciones.sIconoAtender; }
            set { clsOpciones.sIconoAtender = value; }
        }
        public static String SIconoEditarAtender
        {
            get { return clsOpciones.sIconoEditarAtender; }
            set { clsOpciones.sIconoEditarAtender = value; }
        }
        public static String SMensajeRegresar
        {
            get { return clsOpciones.sMensajeRegresar; }
            set { clsOpciones.sMensajeRegresar = value; }
        }

        public static String SMensajeEliminar
        {
            get { return clsOpciones.sMensajeEliminar; }
            set { clsOpciones.sMensajeEliminar = value; }
        }

        public static String STituloEliminar
        {
            get { return clsOpciones.sTituloEliminar; }
            set { clsOpciones.sTituloEliminar = value; }
        }

        public static String STituloModificar
        {
            get { return clsOpciones.sTituloModificar; }
            set { clsOpciones.sTituloModificar = value; }
        }

        public static String STituloCrear
        {
            get { return clsOpciones.sTituloCrear; }
            set { clsOpciones.sTituloCrear = value; }
        }

        public static String SEliminar
        {
            get { return clsOpciones.sEliminar; }
            set { clsOpciones.sEliminar = value; }
        }

        public static String SDetalle
        {
            get { return clsOpciones.sDetalle; }
            set { clsOpciones.sDetalle = value; }
        }
        public static String SEditar
        {
            get { return sEditar; }
            set { sEditar = value; }
        }
        public static String SCrear
        {
            get { return sCrear; }
            set { sCrear = value; }
        }

        public static String SIndice
        {
            get { return clsOpciones.sIndice; }
            set { clsOpciones.sIndice = value; }
        }
    }
}
