// ReSharper disable VirtualMemberCallInContructor
namespace EventosCatolicos.Models
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("Departamento")]
    public class Departamento
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Departamento()
        {
/*           Pacientes = new HashSet<Paciente>();
            Pacientes1 = new HashSet<Paciente>();*/
            Municipios = new HashSet<Municipio>();
        }   
        [Display(Name = "C�digo")]
        public int Id { get; set; }

        [Required(ErrorMessage = "Por favor ingrese un nombre.")]
        [StringLength(256, ErrorMessage = "Sobrepas� el tama�o m�ximo de caracteres.")]
        public string Nombre { get; set; }

/*        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Paciente> Pacientes { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Paciente> Pacientes1 { get; set; }
*/        
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Municipio> Municipios { get; set; }
    }
}
