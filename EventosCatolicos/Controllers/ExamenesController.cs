﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using EventosCatolicos.Models;

namespace EventosCatolicos.Controllers
{
    public class ExamenesController : Controller
    {
        private readonly ApplicationDbContext _db = new ApplicationDbContext();

        // GET: Examenes
        [Authorize(Roles = "Ver Examenes, IT")]
        public ActionResult Index()
        {
            var examenes = _db.Examenes.Include(e => e.Especialidad);
            return View(examenes.ToList());
        }

        // GET: Examenes/Details/5
        [Authorize(Roles = "Ver Examenes,IT")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Examen examen = _db.Examenes.Find(id);
            if (examen == null)
            {
                return HttpNotFound();
            }
            return View(examen);
        }

        // GET: Examenes/Create
        [Authorize(Roles = "Administrar Examenes,IT")]
        public ActionResult Create()
        {
            ViewBag.EspecialidadId = new SelectList(_db.Especialidades, "Id", "Nombre");
            return View();
        }

        // POST: Examenes/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrar Examenes,IT")]
        public ActionResult Create([Bind(Include = "Id,Nombre,Descripcion,EspecialidadId")] Examen examen)
        {
            if (ModelState.IsValid)
            {
                _db.Examenes.Add(examen);
                _db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.EspecialidadId = new SelectList(_db.Especialidades, "Id", "Nombre", examen.EspecialidadId);
            return View(examen);
        }

        // GET: Examenes/Edit/5
        [Authorize(Roles = "Administrar Examenes,IT")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Examen examen = _db.Examenes.Find(id);
            if (examen == null)
            {
                return HttpNotFound();
            }
            ViewBag.EspecialidadId = new SelectList(_db.Especialidades, "Id", "Nombre", examen.EspecialidadId);
            return View(examen);
        }

        // POST: Examenes/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrar Examenes,IT")]
        public ActionResult Edit([Bind(Include = "Id,Nombre,Descripcion,EspecialidadId")] Examen examen)
        {
            if (ModelState.IsValid)
            {
                _db.Entry(examen).State = EntityState.Modified;
                _db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.EspecialidadId = new SelectList(_db.Especialidades, "Id", "Nombre", examen.EspecialidadId);
            return View(examen);
        }

        // GET: Examenes/Delete/5
        [Authorize(Roles = "Administrar Eliminar,IT")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Examen examen = _db.Examenes.Find(id);
            if (examen == null)
            {
                return HttpNotFound();
            }
            return View(examen);
        }

        // POST: Examenes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrar Eliminar,IT")]
        public ActionResult DeleteConfirmed(int id)
        {
            Examen examen = _db.Examenes.Find(id);
            _db.Examenes.Remove(examen);
            _db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
