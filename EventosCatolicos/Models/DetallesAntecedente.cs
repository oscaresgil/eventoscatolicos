namespace EventosCatolicos.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("DetallesAntecedente")]
    public class DetallesAntecedente
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Display(Name = "Consulta")]
        public int ConsultaId { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Display(Name = "Antecedente")]
        public int AntecedenteId { get; set; }

        [StringLength(500, ErrorMessage = "Sobrepas� el tama�o m�ximo de caracteres.")]
        public string Observacion { get; set; }

        public virtual Antecedente Antecedente { get; set; }

        public virtual Consulta Consulta { get; set; }
    }
}
