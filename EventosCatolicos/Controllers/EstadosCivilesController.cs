﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using EventosCatolicos.Models;

namespace EventosCatolicos.Controllers
{
    public class EstadosCivilesController : Controller
    {
        private readonly ApplicationDbContext _db = new ApplicationDbContext();

        // GET: EstadosCiviles
        [Authorize(Roles = "Ver EstadosCiviles,IT")]
        public ActionResult Index()
        {
            return View(_db.EstadosCiviles.ToList());
        }

        // GET: EstadosCiviles/Details/5
        [Authorize(Roles = "Ver EstadosCiviles,IT")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EstadoCivil estadoCivil = _db.EstadosCiviles.Find(id);
            if (estadoCivil == null)
            {
                return HttpNotFound();
            }
            return View(estadoCivil);
        }

        // GET: EstadosCiviles/Create
        [Authorize(Roles = "Administrar EstadosCiviles,IT")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: EstadosCiviles/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrar EstadosCiviles,IT")]
        public ActionResult Create([Bind(Include = "Id,Nombre")] EstadoCivil estadoCivil)
        {
            if (ModelState.IsValid)
            {
                _db.EstadosCiviles.Add(estadoCivil);
                _db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(estadoCivil);
        }

        // GET: EstadosCiviles/Edit/5
        [Authorize(Roles = "Administrar EstadosCiviles,IT")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EstadoCivil estadoCivil = _db.EstadosCiviles.Find(id);
            if (estadoCivil == null)
            {
                return HttpNotFound();
            }
            return View(estadoCivil);
        }

        // POST: EstadosCiviles/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrar EstadosCiviles,IT")]
        public ActionResult Edit([Bind(Include = "Id,Nombre")] EstadoCivil estadoCivil)
        {
            if (ModelState.IsValid)
            {
                _db.Entry(estadoCivil).State = EntityState.Modified;
                _db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(estadoCivil);
        }

        // GET: EstadosCiviles/Delete/5
        [Authorize(Roles = "Eliminar EstadosCiviles,IT")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EstadoCivil estadoCivil = _db.EstadosCiviles.Find(id);
            if (estadoCivil == null)
            {
                return HttpNotFound();
            }
            return View(estadoCivil);
        }

        // POST: EstadosCiviles/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Eliminar EstadosCiviles,IT")]
        public ActionResult DeleteConfirmed(int id)
        {
            EstadoCivil estadoCivil = _db.EstadosCiviles.Find(id);
            _db.EstadosCiviles.Remove(estadoCivil);
            _db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
