﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using EventosCatolicos.Models;

namespace EventosCatolicos.Controllers
{
    public class TiposDireccionController : Controller
    {
        private readonly ApplicationDbContext _db = new ApplicationDbContext();

        // GET: TiposDireccion
        [Authorize(Roles = "Ver TiposDireccion,IT")]
        public ActionResult Index()
        {
            return View(_db.TiposDireccion.ToList());
        }

        // GET: TiposDireccion/Details/5
        [Authorize(Roles = "Ver TiposDireccion,IT")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TipoDireccion tipoDireccion = _db.TiposDireccion.Find(id);
            if (tipoDireccion == null)
            {
                return HttpNotFound();
            }
            return View(tipoDireccion);
        }

        // GET: TiposDireccion/Create
        [Authorize(Roles = "Administrar TiposDireccion,IT")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: TiposDireccion/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrar TiposDireccion,IT")]
        public ActionResult Create([Bind(Include = "Id,Nombre")] TipoDireccion tipoDireccion)
        {
            if (ModelState.IsValid)
            {
                _db.TiposDireccion.Add(tipoDireccion);
                _db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tipoDireccion);
        }

        // GET: TiposDireccion/Edit/5
        [Authorize(Roles = "Administrar TiposDireccion,IT")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TipoDireccion tipoDireccion = _db.TiposDireccion.Find(id);
            if (tipoDireccion == null)
            {
                return HttpNotFound();
            }
            return View(tipoDireccion);
        }

        // POST: TiposDireccion/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrar TiposDireccion,IT")]
        public ActionResult Edit([Bind(Include = "Id,Nombre")] TipoDireccion tipoDireccion)
        {
            if (ModelState.IsValid)
            {
                _db.Entry(tipoDireccion).State = EntityState.Modified;
                _db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tipoDireccion);
        }

        // GET: TiposDireccion/Delete/5
        [Authorize(Roles = "Eliminar TiposDireccion,IT")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TipoDireccion tipoDireccion = _db.TiposDireccion.Find(id);
            if (tipoDireccion == null)
            {
                return HttpNotFound();
            }
            return View(tipoDireccion);
        }

        // POST: TiposDireccion/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Eliminar TiposDireccion,IT")]
        public ActionResult DeleteConfirmed(int id)
        {
            TipoDireccion tipoDireccion = _db.TiposDireccion.Find(id);
            _db.TiposDireccion.Remove(tipoDireccion);
            _db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
