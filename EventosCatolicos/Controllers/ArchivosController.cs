﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using EventosCatolicos.Models;

namespace EventosCatolicos.Controllers
{
    public class ArchivosController : Controller
    {
        private readonly ApplicationDbContext _db = new ApplicationDbContext();

        // GET: Archivos
         [Authorize(Roles = "Ver Archivo,IT")]
        public ActionResult Index()
        {
            return View(_db.Archivos.ToList());
        }

        // GET: Archivos/Details/5
        [Authorize(Roles = "Ver Archivo,IT")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Archivo archivo = _db.Archivos.Find(id);
            if (archivo == null)
            {
                return HttpNotFound();
            }
            return View(archivo);
        }

        // GET: Archivos/Create
         [Authorize(Roles = "Adminsitrar Archivo,IT")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Archivos/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrar Archivo,IT")]
        public ActionResult Create([Bind(Include = "Id,Nombre,Localizacion")] Archivo archivo)
        {
            if (ModelState.IsValid)
            {
                _db.Archivos.Add(archivo);
                _db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(archivo);
        }

        // GET: Archivos/Edit/5
        [Authorize(Roles = "Administrar Archivo,IT")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Archivo archivo = _db.Archivos.Find(id);
            if (archivo == null)
            {
                return HttpNotFound();
            }
            return View(archivo);
        }

        // POST: Archivos/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrar Archivo,IT")]
        public ActionResult Edit([Bind(Include = "Id,Nombre,Localizacion")] Archivo archivo)
        {
            if (ModelState.IsValid)
            {
                _db.Entry(archivo).State = EntityState.Modified;
                _db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(archivo);
        }

        // GET: Archivos/Delete/5
        [Authorize(Roles = "Eliminar Archivo,IT")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Archivo archivo = _db.Archivos.Find(id);
            if (archivo == null)
            {
                return HttpNotFound();
            }
            return View(archivo);
        }

        // POST: Archivos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Eliminar Archivo,IT")]
        public ActionResult DeleteConfirmed(int id)
        {
            Archivo archivo = _db.Archivos.Find(id);
            _db.Archivos.Remove(archivo);
            _db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
