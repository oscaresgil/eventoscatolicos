// ReSharper disable VirtualMemberCallInContructor
namespace EventosCatolicos.Models
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("Tratamiento")]
    public class Tratamiento
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Tratamiento()
        {
            DetallesTratamientos = new HashSet<DetallesTratamiento>();
        }

        public int Id { get; set; }

        [Required(ErrorMessage = "Por favor ingrese un nombre")]
        [StringLength(256, ErrorMessage = "Sobrepas� el tama�o m�ximo de caracteres.")]
        public string Nombre { get; set; }

        [StringLength(500, ErrorMessage = "Sobrepas� el tama�o m�ximo de caracteres.")]
        public string Descripcion { get; set; }

        public int CategoriaId { get; set; }

        public virtual CategoriaTratamiento CategoriaTratamiento { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DetallesTratamiento> DetallesTratamientos { get; set; }
    }
}
