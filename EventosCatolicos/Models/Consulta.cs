// ReSharper disable VirtualMemberCallInContructor
namespace EventosCatolicos.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("Consulta")]
    public class Consulta
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Consulta()
        {
            DetallesAntecedentes = new HashSet<DetallesAntecedente>();
            DetallesDiagnosticos = new HashSet<DetallesDiagnostico>();
            DetallesRecetarios = new HashSet<DetallesRecetario>();
            DetallesSintomas = new HashSet<DetallesSintoma>();
            DetallesTratamientos = new HashSet<DetallesTratamiento>();
            DetallesExamenes = new HashSet<DetallesExamen>();
        }

        [Display(Name = "C�digo")]
        public int Id { get; set; }

        [Display(Name = "C�digo de Cobro")]
        public int CobroId { get; set; }

        [Display(Name = "C�digo de Paciente")]
        public int PacienteId { get; set; }

        [Column(TypeName = "datetime2")]
        [Display(Name = "Fecha de Consulta")]
        public DateTime? FechaConsulta { get; set; }

        [Display(Name = "C�digo de Cl�nica")]
        public int? ClinicaId { get; set; }

        [Display(Name = "C�digo de M�dico")]
        public int? MedicoId { get; set; }

        [Display(Name = "C�digo Estado")]
        public int EstadoId { get; set; }

        public int EstadoArchivo { get; set; }

        [StringLength(256, ErrorMessage = "Sobrepas� el tama�o m�ximo de caracteres.")]
        public string Motivo { get; set; }

        public string Observaciones { get; set; }

        [Display(Name = "Presi�n Arterial")]
        public double? PresionArterial { get; set; }

        public double? Peso { get; set; }

        public double? Temperatura { get; set; }

        [Display(Name = "Frecuencia Card�aca")]
        public double? FrecuenciaCardiaca { get; set; }

        [Display(Name = "Frecuencia Respiratoria")]
        public double? FrecuenciaRespiratoria { get; set; }

        [Display(Name = "Masa Corporal")]
        public double? MasaCorporal { get; set; }

        [Column(TypeName = "datetime2")]
        [Display(Name = "Fecha Cita")]
        public DateTime? ProximaCita { get; set; }

        [StringLength(128)]
        [Display(Name = "Creado Por")]
        public string CreatedBy { get; set; }

        [Column(TypeName = "datetime2")]
        [Display(Name = "Fecha de Creaci�n")]
        public DateTime CreatedAt { get; set; }

        [StringLength(50)]
        [Display(Name = "Creado Por")]
        public string CreatedFrom { get; set; }

        [StringLength(128)]
        [Display(Name = "Modificado Por")]
        public string ModifiedBy { get; set; }

        [Column(TypeName = "datetime2")]
        [Display(Name = "Fecha de Modificaci�n")]
        public DateTime ModifiedAt { get; set; }

        [StringLength(50)]
        [Display(Name = "Modificado Desde")]
        public string ModifiedFrom { get; set; }

        public virtual ApplicationUser ApplicationUser { get; set; }

        public virtual ApplicationUser ApplicationUser1 { get; set; }

        public virtual Clinica Clinica { get; set; }

        public virtual EstadoConsulta EstadoConsulta { get; set; }

        public virtual Medico Medico { get; set; }

        public virtual Paciente Paciente { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DetallesAntecedente> DetallesAntecedentes { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DetallesDiagnostico> DetallesDiagnosticos { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DetallesRecetario> DetallesRecetarios { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DetallesSintoma> DetallesSintomas { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DetallesTratamiento> DetallesTratamientos { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DetallesExamen> DetallesExamenes { get; set; }
    }
}
