﻿using System;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using EventosCatolicos.Models;
using EventosCatolicos.Servicios;
using EventosCatolicos.ViewModels;
using Microsoft.AspNet.Identity.Owin;
using WebGrease.Css.Extensions;

namespace EventosCatolicos.Controllers
{
    public class ConsultasController : Controller
    {
        private readonly ApplicationDbContext _db = new ApplicationDbContext();

        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext()
                    .GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        // GET: Consultas
        [Authorize(Roles = "Ver consultas,IT")]
        public ActionResult Index()
        {
            var consultasModel = _db.Consultas.Include(c => c.Clinica).Include(c => c.EstadoConsulta).Include(c => c.Medico).Include(c => c.Paciente);
            var consultas = consultasModel.Select(
                consulta => new IndexConsultaViewModel
                {
                    Id = consulta.Id,
                    CobroId = consulta.CobroId,
                    PacienteId = consulta.PacienteId,
                    Paciente = consulta.Paciente.PrimerNombre + " "
                        + consulta.Paciente.PrimerApellido,
                    FechaConsulta = consulta.FechaConsulta,
                    Clinica = consulta.Clinica.Nombre,
                    Medico = consulta.Medico.PrimerNombre + " "
                        + consulta.Medico.PrimerApellido,
                    Estado = consulta.EstadoConsulta.Nombre,
                    ProximaCita = consulta.ProximaCita,
                });
            return View(consultas.ToList());
        }

        // GET: Consultas/Details/5
        [Authorize(Roles = "Ver consultas,IT")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Consulta consulta = _db.Consultas.Find(id);
            if (consulta == null)
            {
                return HttpNotFound();
            }
            ConsultaViewModel model = new ConsultaViewModel
            {
                Id = consulta.Id,
                CobroId = consulta.CobroId,
                PacienteId = consulta.PacienteId,
                Paciente = consulta.Paciente != null ?
                            consulta.Paciente.PrimerNombre + " " +
                            consulta.Paciente.SegundoNombre + " " +
                            consulta.Paciente.PrimerApellido + " " +
                            consulta.Paciente.SegundoApellido
                            : "No definido",
                FechaConsultaView = Utils.GetDate(consulta.FechaConsulta),
                ClinicaId = consulta.ClinicaId,
                MedicoId = consulta.MedicoId,
                Medico = consulta.Medico != null ?
                            consulta.Medico.PrimerNombre + " " +
                            consulta.Medico.PrimerApellido
                            : "No definido",
                Estado = consulta.EstadoConsulta != null ? consulta.EstadoConsulta.Nombre : "",
                Motivo = consulta.Motivo,
                ProximaCitaView = Utils.GetDate(consulta.FechaConsulta),
                Observaciones = consulta.Observaciones,
                Peso = consulta.Peso,
                Temperatura = consulta.Temperatura,
                FrecuenciaCardiaca = consulta.FrecuenciaCardiaca,
                FrecuenciaRespiratoria = consulta.FrecuenciaRespiratoria,
                MasaCorporal = consulta.MasaCorporal,
                DetallesSintomas = consulta.DetallesSintomas.ToList(),
                DetallesAntecedentes = consulta.DetallesAntecedentes.ToList(),
                DetallesDiagnosticos = consulta.DetallesDiagnosticos.ToList(),
                DetallesTratamientos = consulta.DetallesTratamientos.ToList(),
                DetallesRecetarios = consulta.DetallesRecetarios.ToList(),
                DetallesExamenes = consulta.DetallesExamenes.ToList()
            };
            return View(model);
        }

        // GET: Consultas/Create
        [Authorize(Roles = "Crear consultas,IT")]
        public ActionResult Create()
        {
            ViewBag.ClinicaId = new SelectList(_db.Clinicas, "Id", "Nombre");
            ViewBag.EstadoId = new SelectList(_db.EstadoConsultas, "Id", "Nombre");

            return View(new GenerateConsultaViewModel());
        }

        // POST: Consultas/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Crear consultas,IT")]
        public ActionResult Create(GenerateConsultaViewModel model)
        {
            if (ModelState.IsValid)
            {
                Consulta consulta = new Consulta
                {
                    PacienteId = model.PacienteId,
                    MedicoId = model.MedicoId,
                    EstadoId = model.EstadoId,
                    ClinicaId = model.ClinicaId,
                    CobroId = model.CobroId,
                    FechaConsulta = model.FechaConsulta,
                    ProximaCita = model.ProximaCita,
                    CreatedBy = User.Identity.GetUserId(),
                    ModifiedBy = User.Identity.GetUserId(),
                    CreatedAt = DateTime.Now,
                    ModifiedAt = DateTime.Now,
                    CreatedFrom = RequestHelpers.GetClientIpAddress(Request),
                    ModifiedFrom = RequestHelpers.GetClientIpAddress(Request),
                    EstadoArchivo = 0
                };
                _db.Consultas.Add(consulta);
                _db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ClinicaId = new SelectList(_db.Clinicas, "Id", "Nombre", model.ClinicaId);
            ViewBag.EstadoId = new SelectList(_db.EstadoConsultas, "Id", "Nombre", model.EstadoId);
            return View(new ConsultaViewModel());
        }

        // GET: Consultas/Fill
        [Authorize(Roles = "Llenar consultas,IT")]
        public ActionResult Fill(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Consulta consulta = _db.Consultas.Find(id);
            if (consulta == null)
            {
                return HttpNotFound();
            }
            ConsultaViewModel model = new ConsultaViewModel
            {
                Id = consulta.Id,
                PacienteId = consulta.PacienteId,
                Paciente = consulta.Paciente.PrimerNombre + " " +
                            consulta.Paciente.SegundoNombre + " " +
                            consulta.Paciente.PrimerApellido + " " + 
                            consulta.Paciente.SegundoApellido,
                MedicoId = consulta.MedicoId,
                EstadoId = consulta.EstadoId,
                ClinicaId = consulta.ClinicaId,
                CobroId = consulta.CobroId,
                FechaConsulta = consulta.FechaConsulta,
                ProximaCita = consulta.ProximaCita
            };
            ViewBag.ClinicaId = new SelectList(_db.Clinicas, "Id", "Nombre", _db.Clinicas.Find(consulta.ClinicaId));
            ViewBag.EstadoId = new SelectList(_db.EstadoConsultas, "Id", "Nombre", _db.EstadoConsultas.Find(consulta.EstadoId));
            return View(model);
        }

        // POST: Consultas/Fill
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Llenar consultas,IT")]
        public ActionResult Fill(ConsultaViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (model.Id == 0)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Consulta consulta = _db.Consultas.Find(model.Id);
                if (consulta == null)
                {
                    return HttpNotFound();
                }
                consulta.Motivo = model.Motivo;
                consulta.ProximaCita = model.ProximaCita;
                consulta.Observaciones = model.Observaciones;
                consulta.Peso = model.Peso;
                consulta.Temperatura = model.Temperatura;
                consulta.FrecuenciaCardiaca = model.FrecuenciaCardiaca;
                consulta.FrecuenciaRespiratoria = model.FrecuenciaRespiratoria;
                consulta.MasaCorporal = model.MasaCorporal;
                consulta.ModifiedBy = User.Identity.GetUserId();
                consulta.ModifiedAt = DateTime.Now;
                consulta.ModifiedFrom = RequestHelpers.GetClientIpAddress(Request);
                consulta.EstadoId = _db.EstadoConsultas.First(e => e.Nombre.Equals("Finalizado")).Id;
                var consultaStored = _db.Consultas.Add(consulta);

                model.DetallesSintomas.ForEach(sintoma =>
                {
                    sintoma.ConsultaId = consultaStored.Id;
                    _db.DetallesSintomas.Add(sintoma);
                });

                model.DetallesAntecedentes.ForEach(antecedente =>
                {
                    antecedente.ConsultaId = consultaStored.Id;
                    _db.DetallesAntecedentes.Add(antecedente);
                });

                model.DetallesDiagnosticos.ForEach(diagnostico =>
                {
                    diagnostico.ConsultaId = consultaStored.Id;
                    _db.DetallesDiagnosticos.Add(diagnostico);
                });

                model.DetallesTratamientos.ForEach(tratamiento =>
                {
                    tratamiento.ConsultaId = consultaStored.Id;
                    _db.DetallesTratamientos.Add(tratamiento);
                });

                model.DetallesRecetarios.ForEach(recetario =>
                {
                    recetario.ConsultaId = consultaStored.Id;
                    _db.DetallesRecetarios.Add(recetario);
                });

                model.DetallesExamenes.ForEach(examen =>
                {
                    examen.ConsultaId = consultaStored.Id;
                    _db.DetallesExamen.Add(examen);
                });

                _db.Entry(consulta).State = EntityState.Modified;
                _db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ClinicaId = new SelectList(_db.Clinicas, "Id", "Nombre", model.ClinicaId);
            ViewBag.EstadoId = new SelectList(_db.EstadoConsultas, "Id", "Nombre", model.EstadoId);
            return View(new ConsultaViewModel());
        }

        // GET: Consultas/Edit/5
        [Authorize(Roles = "Crear consultas,IT")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Consulta consulta = _db.Consultas.Find(id);
            if (consulta == null)
            {
                return HttpNotFound();
            }
            GenerateConsultaViewModel model = new ConsultaViewModel
            {
                Id = consulta.Id,
                PacienteId = consulta.PacienteId,
                Paciente = consulta.Paciente != null ?
                            consulta.Paciente.PrimerNombre + " " +
                            consulta.Paciente.SegundoNombre + " " +
                            consulta.Paciente.PrimerApellido + " " +
                            consulta.Paciente.SegundoApellido
                            : "No definido",
                MedicoId = consulta.MedicoId,
                Medico = consulta.Medico != null ? 
                            consulta.Medico.PrimerNombre + " " +
                            consulta.Medico.PrimerApellido
                            : "No definido",
                EstadoId = consulta.EstadoId,
                ClinicaId = consulta.ClinicaId,
                CobroId = consulta.CobroId,
                FechaConsulta = consulta.FechaConsulta,
                ProximaCita = consulta.ProximaCita
            };
            ViewBag.ClinicaId = new SelectList(_db.Clinicas, "Id", "Nombre", _db.Clinicas.Find(consulta.ClinicaId));
            ViewBag.EstadoId = new SelectList(_db.EstadoConsultas, "Id", "Nombre", _db.EstadoConsultas.Find(consulta.EstadoId));
            return View(model);
        }

        // POST: Consultas/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Crear consultas,IT")]
        public ActionResult Edit(GenerateConsultaViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (model.Id == 0)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Consulta consulta = _db.Consultas.Find(model.Id);
                if (consulta == null)
                {
                    return HttpNotFound();
                }
                consulta.PacienteId = model.PacienteId;
                consulta.MedicoId = model.MedicoId;
                consulta.EstadoId = model.EstadoId;
                consulta.ClinicaId = model.ClinicaId;
                consulta.CobroId = model.CobroId;
                consulta.FechaConsulta = model.FechaConsulta;
                consulta.ProximaCita = model.ProximaCita;
                consulta.ModifiedBy = User.Identity.GetUserId();
                consulta.ModifiedAt = DateTime.Now;
                consulta.ModifiedFrom = RequestHelpers.GetClientIpAddress(Request);

                _db.Entry(consulta).State = EntityState.Modified;
                _db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ClinicaId = new SelectList(_db.Clinicas, "Id", "Nombre", model.ClinicaId);
            ViewBag.EstadoId = new SelectList(_db.EstadoConsultas, "Id", "Nombre", model.EstadoId);
            return View(new ConsultaViewModel());
        }

        // GET: Consultas/Edit/5
        [Authorize(Roles = "Llenar consultas,IT")]
        public ActionResult EditFill(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Consulta consulta = _db.Consultas.Find(id);
            if (consulta == null)
            {
                return HttpNotFound();
            }
            ConsultaViewModel model = new ConsultaViewModel
            {
                Id = consulta.Id,
                PacienteId = consulta.PacienteId,
                Paciente = consulta.Paciente.PrimerNombre + " " +
                            consulta.Paciente.SegundoNombre + " " +
                            consulta.Paciente.PrimerApellido + " " +
                            consulta.Paciente.SegundoApellido,
                CobroId = consulta.CobroId,
                EstadoId = consulta.EstadoId,
                ClinicaId = consulta.ClinicaId,
                MedicoId = consulta.MedicoId,
                FechaConsulta = consulta.FechaConsulta,
                ProximaCita = consulta.ProximaCita,
                Motivo = consulta.Motivo,
                Observaciones = consulta.Observaciones,
                PresionArterial = consulta.PresionArterial,
                Peso = consulta.Peso,
                Temperatura = consulta.Temperatura,
                FrecuenciaCardiaca = consulta.FrecuenciaCardiaca,
                FrecuenciaRespiratoria = consulta.FrecuenciaRespiratoria,
                MasaCorporal = consulta.MasaCorporal
            };
            ViewBag.ClinicaId = new SelectList(_db.Clinicas, "Id", "Nombre", consulta.ClinicaId);
            ViewBag.EstadoId = new SelectList(_db.EstadoConsultas, "Id", "Nombre", consulta.EstadoId);
            
            consulta.DetallesSintomas.ForEach(sintoma => model.DetallesSintomas.Add(sintoma));
            consulta.DetallesAntecedentes.ForEach(antecedente => model.DetallesAntecedentes.Add(antecedente));
            consulta.DetallesDiagnosticos.ForEach(diagnostico => model.DetallesDiagnosticos.Add(diagnostico));
            consulta.DetallesTratamientos.ForEach(tratamiento => model.DetallesTratamientos.Add(tratamiento));
            consulta.DetallesRecetarios.ForEach(recetario => model.DetallesRecetarios.Add(recetario));
            consulta.DetallesExamenes.ForEach(examen => model.DetallesExamenes.Add(examen));
            return View(model);
        }

        // POST: Consultas/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Llenar consultas,IT")]
        public ActionResult EditFill(ConsultaViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (model.Id == 0)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Consulta consulta = _db.Consultas.Find(model.Id);
                if (consulta == null)
                {
                    return HttpNotFound();
                }
                consulta.Motivo = model.Motivo;
                consulta.ProximaCita = model.ProximaCita;
                consulta.Observaciones = model.Observaciones;
                consulta.Peso = model.Peso;
                consulta.Temperatura = model.Temperatura;
                consulta.FrecuenciaCardiaca = model.FrecuenciaCardiaca;
                consulta.FrecuenciaRespiratoria = model.FrecuenciaRespiratoria;
                consulta.MasaCorporal = model.MasaCorporal;
                consulta.ModifiedBy = User.Identity.GetUserId();
                consulta.ModifiedAt = DateTime.Now;
                consulta.ModifiedFrom = RequestHelpers.GetClientIpAddress(Request);

                _db.DetallesSintomas
                    .Where(e => e.ConsultaId == consulta.Id)
                    .ForEach(e => _db.DetallesSintomas.Remove(e));
                model.DetallesSintomas.ForEach(sintoma =>
                {
                    sintoma.ConsultaId = consulta.Id;
                    _db.DetallesSintomas.Add(sintoma);
                });

                _db.DetallesAntecedentes
                    .Where(e => e.ConsultaId == consulta.Id)
                    .ForEach(e => _db.DetallesAntecedentes.Remove(e));
                //_db.SaveChanges();
                model.DetallesAntecedentes.ForEach(antecedente =>
                {
                    antecedente.ConsultaId = consulta.Id;
                    _db.DetallesAntecedentes.Add(antecedente);
                });

                _db.DetallesDiagnosticos
                    .Where(e => e.ConsultaId == consulta.Id)
                    .ForEach(e => _db.DetallesDiagnosticos.Remove(e));
                model.DetallesDiagnosticos.ForEach(diagnostico =>
                {
                    diagnostico.ConsultaId = consulta.Id;
                    _db.DetallesDiagnosticos.Add(diagnostico);
                });

                _db.DetallesTratamientos
                    .Where(e => e.ConsultaId == consulta.Id)
                    .ForEach(e => _db.DetallesTratamientos.Remove(e));
                model.DetallesTratamientos.ForEach(tratamiento =>
                {
                    tratamiento.ConsultaId = consulta.Id;
                    _db.DetallesTratamientos.Add(tratamiento);
                });

                _db.DetallesRecetarios
                    .Where(e => e.ConsultaId == consulta.Id)
                    .ForEach(e => _db.DetallesRecetarios.Remove(e));
                model.DetallesRecetarios.ForEach(recetario =>
                {
                    recetario.ConsultaId = consulta.Id;
                    _db.DetallesRecetarios.Add(recetario);
                });

                _db.DetallesExamen
                    .Where(e => e.ConsultaId == consulta.Id)
                    .ForEach(e => _db.DetallesExamen.Remove(e));
                model.DetallesExamenes.ForEach(examen =>
                {
                    examen.ConsultaId = consulta.Id;
                    _db.DetallesExamen.Add(examen);
                });

                _db.Entry(consulta).State = EntityState.Modified;
                _db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ClinicaId = new SelectList(_db.Clinicas, "Id", "Nombre", model.ClinicaId);
            ViewBag.EstadoId = new SelectList(_db.EstadoConsultas, "Id", "Nombre", model.EstadoId);
            return View(new ConsultaViewModel());
        }

        // GET: Consultas/Delete/5
        [Authorize(Roles = "Crear consultas,IT")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Consulta consulta = _db.Consultas.Find(id);
            if (consulta == null)
            {
                return HttpNotFound();
            }
            return View(consulta);
        }

        // POST: Consultas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Consulta consulta = _db.Consultas.Find(id);
            _db.Consultas.Remove(consulta);
            _db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult Attending(int id)
        {
            Consulta consulta = _db.Consultas.Find(id);
            var estado = _db.EstadoConsultas.First(e => e.Nombre.Equals("Atendiendo"));
            consulta.EstadoId = estado.Id;
            _db.Entry(consulta).State = EntityState.Modified;
            _db.SaveChanges();
            return new JsonResult();
        }

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult AddSintoma(ConsultaViewModel model)
        {
            model.DetallesSintomas.Add(new DetallesSintoma());
            return View(model);
        }

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult AddAntecedente(ConsultaViewModel model)
        {
            model.DetallesAntecedentes.Add(new DetallesAntecedente());
            return View(model);
        }

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult AddDiagnostico(ConsultaViewModel model)
        {
            model.DetallesDiagnosticos.Add(new DetallesDiagnostico());
            return View(model);
        }

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult AddTratamiento(ConsultaViewModel model)
        {
            model.DetallesTratamientos.Add(new DetallesTratamiento());
            return View(model);
        }

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult AddRecetario(ConsultaViewModel model)
        {
            model.DetallesRecetarios.Add(new DetallesRecetario());
            return View(model);
        }

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult AddExamen(ConsultaViewModel model)
        {
            model.DetallesExamenes.Add(new DetallesExamen());
            return View(model);
        }

        public ActionResult SintomaAutocomplete(string term)
        {
            var sintomas = _db.Sintomas.ToList().Select(
                sintoma => new SelectorModel
                {
                    Value = sintoma.Id,
                    Label = sintoma.Nombre
                });
            return Json(sintomas.Where(t => t.Label.ToLower().Contains(term.ToLower())),
                    JsonRequestBehavior.AllowGet);
        }

        public ActionResult AntecedenteAutocomplete(string term)
        {
            var antecedentes = _db.Antecedentes.ToList().Select(
                antecedente => new SelectorModel
                {
                    Value = antecedente.Id,
                    Label = antecedente.Nombre
                });
            return Json(antecedentes.Where(t => t.Label.ToLower().Contains(term.ToLower())),
                    JsonRequestBehavior.AllowGet);
        }

        public ActionResult DiagnosticoAutocomplete(string term)
        {
            var diagnosticos = _db.Diagnosticos.ToList().Select(
                diagnostico => new SelectorModel
                {
                    Value = diagnostico.Id,
                    Label = diagnostico.Nombre
                });
            return Json(diagnosticos.Where(t => t.Label.ToLower().Contains(term.ToLower())),
                    JsonRequestBehavior.AllowGet);
        }

        public ActionResult TratamientoAutocomplete(string term)
        {
            var tratamientos = _db.Tratamientos.ToList().Select(
                tratamiento => new SelectorModel
                {
                    Value = tratamiento.Id,
                    Label = tratamiento.Nombre
                });
            return Json(tratamientos.Where(t => t.Label.ToLower().Contains(term.ToLower())),
                    JsonRequestBehavior.AllowGet);
        }

        public ActionResult RecetarioAutocomplete(string term)
        {
            var recetarios = _db.Recetarios.ToList().Select(
                recetario => new SelectorModel
                {
                    Value = recetario.Id,
                    Label = recetario.Nombre
                });
            return Json(recetarios.Where(t => t.Label.ToLower().Contains(term.ToLower())),
                    JsonRequestBehavior.AllowGet);
        }

        public ActionResult ExamenAutocomplete(string term)
        {
            var examenes = _db.Examenes.ToList().Select(
                examen => new SelectorModel
                {
                    Value = examen.Id,
                    Label = examen.Nombre
                });
            return Json(examenes.Where(t => t.Label.ToLower().Contains(term.ToLower())),
                    JsonRequestBehavior.AllowGet);
        }

        public ActionResult PacienteAutocomplete(string term)
        {
            var pacientes = _db.Pacientes.ToList().Select(
                paciente => new SelectorModel
                {
                    Value = paciente.Id,
                    Label = string.Format("{0} {1} {2} {3}", paciente.PrimerNombre, paciente.SegundoNombre, paciente.PrimerApellido, paciente.SegundoApellido)
                });
            return Json(pacientes.Where(t => t.Label.ToLower().Contains(term.ToLower())),
                    JsonRequestBehavior.AllowGet);
        }

        public ActionResult PacienteIdAutocomplete(int id)
        {
            var pacientes = _db.Pacientes.ToList().Select(
                paciente => new SelectorModel
                {
                    Value = paciente.Id,
                    Label = string.Format("{0} {1} {2} {3}", paciente.PrimerNombre, paciente.SegundoNombre, paciente.PrimerApellido, paciente.SegundoApellido)
                });
            return Json(pacientes.Where(t => t.Value == id),
                    JsonRequestBehavior.AllowGet);
        }

        public ActionResult MedicoAutocomplete(string term)
        {
            var pacientes = _db.Medicos.ToList().Select(
                medico => new SelectorModel
                {
                    Value = medico.Id,
                    Label = string.Format("{0} {1}", medico.PrimerNombre, medico.PrimerApellido)
                });
            return Json(pacientes.Where(t => t.Label.ToLower().StartsWith(term.ToLower())),
                    JsonRequestBehavior.AllowGet);
        }

        public ActionResult MedicoIdAutocomplete(int id)
        {
            var pacientes = _db.Medicos.ToList().Select(
                medico => new SelectorModel
                {
                    Value = medico.Id,
                    Label = string.Format("{0} {1}", medico.PrimerNombre, medico.PrimerApellido)
                });
            return Json(pacientes.Where(t => t.Value == id),
                    JsonRequestBehavior.AllowGet);
        }

        [Authorize(Roles = "Llenar consultas,IT")]
        public ActionResult Attend()
        {
            var userId = User.Identity.GetUserId();
            var estadoActivo = _db.EstadoConsultas.First(e => e.Nombre.Equals("Activo")).Id;
            var estadoAtendiendo = _db.EstadoConsultas.First(e => e.Nombre.Equals("Atendiendo")).Id;
            var consultasModel = _db.Consultas.Include(c => c.Clinica).Include(c => c.EstadoConsulta).Include(c => c.Medico).Include(c => c.Paciente);
            var consultas = consultasModel
                .Where(c => c.Medico.ApplicationUser.Id.Equals(userId) && (c.EstadoId == estadoActivo || c.EstadoId == estadoAtendiendo))
                .Select(@consulta => new IndexConsultaViewModel
                {
                    Id = @consulta.Id,
                    CobroId = @consulta.CobroId,
                    PacienteId = @consulta.PacienteId,
                    Paciente = @consulta.Paciente.PrimerNombre + " "
                        + @consulta.Paciente.PrimerApellido,
                    FechaConsulta = @consulta.FechaConsulta,
                    Clinica = @consulta.Clinica.Nombre,
                    Medico = @consulta.Medico.PrimerNombre + " "
                        + @consulta.Medico.PrimerApellido,
                    Estado = @consulta.EstadoConsulta.Nombre,
                    ProximaCita = @consulta.ProximaCita
                });
            return View(consultas.ToList());
        }
        
        [Authorize(Roles = "Llenar consultas,IT")]
        public ActionResult EditAttend()
        {
            var userId = User.Identity.GetUserId();
            var estadoActivo = _db.EstadoConsultas.First(e => e.Nombre.Equals("Finalizado")).Id;
            var consultasModel = _db.Consultas.Include(c => c.Clinica).Include(c => c.EstadoConsulta).Include(c => c.Medico).Include(c => c.Paciente);
            var consultas = consultasModel
                .Where(c => c.Medico.ApplicationUser.Id.Equals(userId) && c.EstadoId == estadoActivo)
                .Select(@consulta => new IndexConsultaViewModel
                {
                    Id = @consulta.Id,
                    CobroId = @consulta.CobroId,
                    PacienteId = @consulta.PacienteId,
                    Paciente = @consulta.Paciente.PrimerNombre + " "
                        + @consulta.Paciente.PrimerApellido,
                    FechaConsulta = @consulta.FechaConsulta,
                    Clinica = @consulta.Clinica.Nombre,
                    Medico = @consulta.Medico.PrimerNombre + " "
                        + @consulta.Medico.PrimerApellido,
                    Estado = @consulta.EstadoConsulta.Nombre,
                    ProximaCita = @consulta.ProximaCita
                });
            return View(consultas.ToList());
        }

        [Authorize(Roles = "Administrar sintomas,IT")]
        public ActionResult CreateSintoma()
        {
            return PartialView();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrar sintomas,IT")]
        public void CreateSintoma([Bind(Include = "Id,Nombre,Descripcion")] Sintoma sintoma)
        {
            if (ModelState.IsValid)
            {
                _db.Sintomas.Add(sintoma);
                _db.SaveChanges();
            }
        }

        [Authorize(Roles = "Administrar antecedentes,IT")]
        public ActionResult CreateAntecedente()
        {
            ViewBag.CategoriaId = new SelectList(_db.CategoriaAntecedentes, "Id", "Nombre");
            return PartialView();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrar antecedentes,IT")]
        public void CreateAntecedente([Bind(Include = "Id,Nombre,Descripcion,CategoriaId")] Antecedente antecedente)
        {
            if (ModelState.IsValid)
            {
                _db.Antecedentes.Add(antecedente);
                _db.SaveChanges();
            }

            ViewBag.CategoriaId = new SelectList(_db.CategoriaAntecedentes, "Id", "Nombre", antecedente.CategoriaId);
        }

        [Authorize(Roles = "Administrar diagnosticos,IT")]
        public ActionResult CreateDiagnostico()
        {
            ViewBag.CategoriaId = new SelectList(_db.CategoriaDiagnosticos, "Id", "Nombre");
            return PartialView();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrar diagnosticos,IT")]
        public void CreateDiagnostico([Bind(Include = "Id,Nombre,Descripcion,CategoriaId")] Diagnostico diagnostico)
        {
            if (ModelState.IsValid)
            {
                _db.Diagnosticos.Add(diagnostico);
                _db.SaveChanges();
            }

            ViewBag.CategoriaId = new SelectList(_db.CategoriaDiagnosticos, "Id", "Nombre", diagnostico.CategoriaId);
        }

        [Authorize(Roles = "Administrar tratamiento,IT")]
        public ActionResult CreateTratamiento()
        {
            ViewBag.CategoriaId = new SelectList(_db.CategoriaTratamientos, "Id", "Nombre");
            return PartialView();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrar tratamiento,IT")]
        public void CreateTratamiento([Bind(Include = "Id,Nombre,Descripcion,CategoriaId")] Tratamiento tratamiento)
        {
            if (ModelState.IsValid)
            {
                _db.Tratamientos.Add(tratamiento);
                _db.SaveChanges();
            }

            ViewBag.CategoriaId = new SelectList(_db.CategoriaTratamientos, "Id", "Nombre", tratamiento.CategoriaId);
        }

        [Authorize(Roles = "Administrar recetario,IT")]
        public ActionResult CreateRecetario()
        {
            ViewBag.EspecialidadId = new SelectList(_db.Especialidades, "Id", "Nombre");
            ViewBag.AreaId = new SelectList(_db.AreaRecetarios, "Id", "Nombre");
            return PartialView();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrar recetario,IT")]
        public void CreateRecetario([Bind(Include = "Id,Nombre,Descripcion,EspecialidadId,AreaId,PrecioPublico,PrecioEspecial,FechaAlta,FechaBaja,Observacion")] Recetario recetario)
        {
            if (ModelState.IsValid)
            {
                _db.Recetarios.Add(recetario);
                _db.SaveChanges();
            }

            ViewBag.EspecialidadId = new SelectList(_db.Especialidades, "Id", "Nombre", recetario.EspecialidadId);
            ViewBag.AreaId = new SelectList(_db.AreaRecetarios, "Id", "Nombre", recetario.AreaId);
        }

        [Authorize(Roles = "Administrar examen,IT")]
        public ActionResult CreateExamen()
        {
            ViewBag.EspecialidadId = new SelectList(_db.Especialidades, "Id", "Nombre");
            return PartialView();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrar examen,IT")]
        public void CreateExamen([Bind(Include = "Id,Nombre,Descripcion,EspecialidadId")] Examen examen)
        {
            if (ModelState.IsValid)
            {
                _db.Examenes.Add(examen);
                _db.SaveChanges();
            }

            ViewBag.EspecialidadId = new SelectList(_db.Especialidades, "Id", "Nombre", examen.EspecialidadId);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}