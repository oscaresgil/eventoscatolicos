﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Win32.SafeHandles;

namespace EventosCatolicos.Models
{
    public class ApplicationGroupManager : IDisposable
    {
        private bool _disposed;
        // Instantiate a SafeHandle instance.
        private readonly SafeHandle _handle = new SafeFileHandle(IntPtr.Zero, true);

        private readonly ApplicationGroupStore _groupStore;
        private readonly ApplicationDbContext _db;
        private readonly ApplicationUserManager _userManager;
        private readonly ApplicationRoleManager _roleManager;

        public ApplicationGroupManager()
        {
            _db = HttpContext.Current
                .GetOwinContext().Get<ApplicationDbContext>();
            _userManager = HttpContext.Current
                .GetOwinContext().GetUserManager<ApplicationUserManager>();
            _roleManager = HttpContext.Current
                .GetOwinContext().Get<ApplicationRoleManager>();
            _groupStore = new ApplicationGroupStore(_db);
        }

        public static ApplicationGroupManager Create()
        {
            return new ApplicationGroupManager();
        }

        public IQueryable<ApplicationGroup> Groups
        {
            get
            {
                return _groupStore.Groups;
            }
        }



        public IdentityResult CreateGroup(ApplicationGroup group)
        {
            _groupStore.Create(group);
            return IdentityResult.Success;
        }


        public async Task<IdentityResult> CreateGroupAsync(ApplicationGroup group)
        {
            await _groupStore.CreateAsync(group);
            return IdentityResult.Success;
        }


        public IdentityResult SetGroupRoles(string groupId, params string[] roleNames)
        {
            // Clear all the roles associated with this group:
            var thisGroup = FindById(groupId);
            thisGroup.ApplicationRoles.Clear();
            _db.SaveChanges();

            // Add the new roles passed in:
            var newRoles = _roleManager.Roles.Where(r => roleNames.Any(n => n == r.Name));
            foreach (var role in newRoles)
            {
                thisGroup.ApplicationRoles.Add(new ApplicationGroupRole
                {
                    ApplicationGroupId = groupId,
                    ApplicationRoleId = role.Id
                });
            }
            _db.SaveChanges();

            // Reset the roles for all affected users:
            foreach (var groupUser in thisGroup.ApplicationUsers)
            {
                RefreshUserGroupRoles(groupUser.ApplicationUserId);
            }
            return IdentityResult.Success;
        }


        public async Task<IdentityResult> SetGroupRolesAsync(
            string groupId, params string[] roleNames)
        {
            // Clear all the roles associated with this group:
            var thisGroup = await FindByIdAsync(groupId);
            thisGroup.ApplicationRoles.Clear();
            await _db.SaveChangesAsync();

            // Add the new roles passed in:
            var newRoles = _roleManager.Roles
                            .Where(r => roleNames.Any(n => n == r.Name));
            foreach (var role in newRoles)
            {
                thisGroup.ApplicationRoles.Add(new ApplicationGroupRole
                {
                    ApplicationGroupId = groupId,
                    ApplicationRoleId = role.Id
                });
            }
            await _db.SaveChangesAsync();

            // Reset the roles for all affected users:
            foreach (var groupUser in thisGroup.ApplicationUsers)
            {
                await RefreshUserGroupRolesAsync(groupUser.ApplicationUserId);
            }
            return IdentityResult.Success;
        }


        public IdentityResult SetUserGroups(string userId, params string[] groupIds)
        {
            // Clear current group membership:
            var currentGroups = GetUserGroups(userId);
            foreach (var group in currentGroups)
            {
                group.ApplicationUsers
                    .Remove(group.ApplicationUsers
                    .FirstOrDefault(gr => gr.ApplicationUserId == userId
                ));
            }
            _db.SaveChanges();

            // Add the user to the new groups:
            foreach (string groupId in groupIds)
            {
                var newGroup = FindById(groupId);
                newGroup.ApplicationUsers.Add(new ApplicationUserGroup
                {
                    ApplicationUserId = userId,
                    ApplicationGroupId = groupId
                });
            }
            _db.SaveChanges();

            RefreshUserGroupRoles(userId);
            return IdentityResult.Success;
        }


        public async Task<IdentityResult> SetUserGroupsAsync(
            string userId, params string[] groupIds)
        {
            // Clear current group membership:
            var currentGroups = await GetUserGroupsAsync(userId);
            foreach (var group in currentGroups)
            {
                group.ApplicationUsers
                    .Remove(group.ApplicationUsers
                    .FirstOrDefault(gr => gr.ApplicationUserId == userId
                ));
            }
            await _db.SaveChangesAsync();

            // Add the user to the new groups:
            foreach (string groupId in groupIds)
            {
                var newGroup = await FindByIdAsync(groupId);
                newGroup.ApplicationUsers.Add(new ApplicationUserGroup
                {
                    ApplicationUserId = userId,
                    ApplicationGroupId = groupId
                });
            }
            await _db.SaveChangesAsync();

            await RefreshUserGroupRolesAsync(userId);
            return IdentityResult.Success;
        }


        public IdentityResult RefreshUserGroupRoles(string userId)
        {
            var user = _userManager.FindById(userId);
            if (user == null)
            {
                throw new ArgumentNullException("userId");
            }
            // Remove user from previous roles:
            var oldUserRoles = _userManager.GetRoles(userId);
            if (oldUserRoles.Count > 0)
            {
                _userManager.RemoveFromRoles(userId, oldUserRoles.ToArray());
            }

            // Find the roles this user is entitled to from group membership:
            var newGroupRoles = GetUserGroupRoles(userId);

            // Get the role names:
            var allRoles = _roleManager.Roles.ToList();
            var addTheseRoles = allRoles
                .Where(r => newGroupRoles.Any(gr => gr.ApplicationRoleId == r.Id
            ));
            var roleNames = addTheseRoles.Select(n => n.Name).ToArray();

            // Add the user to the proper roles
            _userManager.AddToRoles(userId, roleNames);

            return IdentityResult.Success;
        }


        public async Task<IdentityResult> RefreshUserGroupRolesAsync(string userId)
        {
            var user = await _userManager.FindByIdAsync(userId);
            if (user == null)
            {
                throw new ArgumentNullException("userId");
            }
            // Remove user from previous roles:
            var oldUserRoles = await _userManager.GetRolesAsync(userId);
            if (oldUserRoles.Count > 0)
            {
                await _userManager.RemoveFromRolesAsync(userId, oldUserRoles.ToArray());
            }

            // Find the roles this user is entitled to from group membership:
            var newGroupRoles = await GetUserGroupRolesAsync(userId);

            // Get the role names:
            var allRoles = await _roleManager.Roles.ToListAsync();
            var addTheseRoles = allRoles
                .Where(r => newGroupRoles.Any(gr => gr.ApplicationRoleId == r.Id
            ));
            var roleNames = addTheseRoles.Select(n => n.Name).ToArray();

            // Add the user to the proper roles
            await _userManager.AddToRolesAsync(userId, roleNames);

            return IdentityResult.Success;
        }


        public IdentityResult DeleteGroup(string groupId)
        {
            var group = FindById(groupId);
            if (group == null)
            {
                throw new ArgumentNullException("groupId");
            }

            var currentGroupMembers = GetGroupUsers(groupId).ToList();
            // remove the roles from the group:
            group.ApplicationRoles.Clear();

            // Remove all the users:
            group.ApplicationUsers.Clear();

            // Remove the group itself:
            _db.ApplicationGroups.Remove(group);

            _db.SaveChanges();

            // Reset all the user roles:
            foreach (var user in currentGroupMembers)
            {
                RefreshUserGroupRoles(user.Id);
            }
            return IdentityResult.Success;
        }


        public async Task<IdentityResult> DeleteGroupAsync(string groupId)
        {
            var group = await FindByIdAsync(groupId);
            if (group == null)
            {
                throw new ArgumentNullException("groupId");
            }

            var currentGroupMembers = (await GetGroupUsersAsync(groupId)).ToList();
            // remove the roles from the group:
            group.ApplicationRoles.Clear();

            // Remove all the users:
            group.ApplicationUsers.Clear();

            // Remove the group itself:
            _db.ApplicationGroups.Remove(group);

            await _db.SaveChangesAsync();

            // Reset all the user roles:
            foreach (var user in currentGroupMembers)
            {
                await RefreshUserGroupRolesAsync(user.Id);
            }
            return IdentityResult.Success;
        }


        public IdentityResult UpdateGroup(ApplicationGroup group)
        {
            _groupStore.Update(group);
            foreach (var groupUser in group.ApplicationUsers)
            {
                RefreshUserGroupRoles(groupUser.ApplicationUserId);
            }
            return IdentityResult.Success;
        }


        public async Task<IdentityResult> UpdateGroupAsync(ApplicationGroup group)
        {
            await _groupStore.UpdateAsync(group);
            foreach (var groupUser in group.ApplicationUsers)
            {
                await RefreshUserGroupRolesAsync(groupUser.ApplicationUserId);
            }
            return IdentityResult.Success;
        }


        public IdentityResult ClearUserGroups(string userId)
        {
            return SetUserGroups(userId);
        }


        public async Task<IdentityResult> ClearUserGroupsAsync(string userId)
        {
            return await SetUserGroupsAsync(userId);
        }


        public IEnumerable<ApplicationGroup> GetUserGroups(string userId)
        {
            var userGroups = (from g in Groups
                              where g.ApplicationUsers
                                .Any(u => u.ApplicationUserId == userId)
                              select g).ToList();
            return userGroups;
        }


        public async Task<IEnumerable<ApplicationGroup>> GetUserGroupsAsync(string userId)
        {
            var userGroups = (from g in Groups
                              where g.ApplicationUsers
                                .Any(u => u.ApplicationUserId == userId)
                              select g).ToListAsync();
            return await userGroups;
        }


        public IEnumerable<ApplicationRole> GetGroupRoles(string groupId)
        {
            var grp = FindById(groupId);
            var roles = _roleManager.Roles.ToList();
            var groupRoles = from r in roles
                             where grp.ApplicationRoles
                                .Any(ap => ap.ApplicationRoleId == r.Id)
                             select r;
            return groupRoles;
        }


        public async Task<IEnumerable<ApplicationRole>> GetGroupRolesAsync(
            string groupId)
        {
            var grp = await FindByIdAsync(groupId);
            var roles = await _roleManager.Roles.ToListAsync();
            var groupRoles = (from r in roles
                              where grp.ApplicationRoles
                                .Any(ap => ap.ApplicationRoleId == r.Id)
                              select r).ToList();
            return groupRoles;
        }


        public IEnumerable<ApplicationUser> GetGroupUsers(string groupId)
        {
            var group = FindById(groupId);
            return @group.ApplicationUsers.Select(groupUser => _userManager.FindById(groupUser.ApplicationUserId)).ToList();
        }


        public async Task<IEnumerable<ApplicationUser>> GetGroupUsersAsync(string groupId)
        {
            var group = await FindByIdAsync(groupId);
            var users = new List<ApplicationUser>();
            foreach (var groupUser in group.ApplicationUsers)
            {
                var user = await _userManager.FindByIdAsync(groupUser.ApplicationUserId);
                users.Add(user);
            }
            return users;
        }

        public IEnumerable<ApplicationGroup> GetGroupsInRole(
            string roleId)
        {
            var role = _roleManager.FindById(roleId);
            var allGroups = Groups.ToList();
            var groupsInRole = (from g in allGroups
                                where g.ApplicationRoles
                                  .Any(ap => ap.ApplicationRoleId == role.Id)
                                select g).ToList();
            return groupsInRole;
        }

        public async Task<IEnumerable<ApplicationGroup>> GetGroupsInRoleAsync(
            string roleId)
        {
            var role = await _roleManager.FindByIdAsync(roleId);
            var allGroups = await Groups.ToListAsync();
            var groupsInRole = (from g in allGroups
                              where g.ApplicationRoles
                                .Any(ap => ap.ApplicationRoleId == role.Id)
                              select g).ToList();
            return groupsInRole;
        }


        public IEnumerable<ApplicationGroupRole> GetUserGroupRoles(string userId)
        {
            var userGroups = GetUserGroups(userId);
            var userGroupRoles = new List<ApplicationGroupRole>();
            foreach (var group in userGroups)
            {
                userGroupRoles.AddRange(group.ApplicationRoles.ToArray());
            }
            return userGroupRoles;
        }


        public async Task<IEnumerable<ApplicationGroupRole>> GetUserGroupRolesAsync(
            string userId)
        {
            var userGroups = await GetUserGroupsAsync(userId);
            var userGroupRoles = new List<ApplicationGroupRole>();
            foreach (var group in userGroups)
            {
                userGroupRoles.AddRange(group.ApplicationRoles.ToArray());
            }
            return userGroupRoles;
        }


        public async Task<ApplicationGroup> FindByIdAsync(string id)
        {
            return await _groupStore.FindByIdAsync(id);
        }


        public ApplicationGroup FindById(string id)
        {
            return _groupStore.FindById(id);
        }

        

        // Public implementation of Dispose pattern callable by consumers.
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        // Protected implementation of Dispose pattern.
        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
            {
                _handle.Dispose();
                // Free any other managed objects here.
                _db.Dispose();
                _userManager.Dispose();
                _roleManager.Dispose();
                _groupStore.Dispose();
            }

            // Free any unmanaged objects here.
            //
            _disposed = true;
        }
    }
}