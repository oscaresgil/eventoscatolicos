// ReSharper disable VirtualMemberCallInContructor
namespace EventosCatolicos.Models
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("Especialidad")]
    public class Especialidad
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Especialidad()
        {
            Examen = new HashSet<Examen>();
            Recetarios = new HashSet<Recetario>();
            Medicos = new HashSet<Medico>();
        }

        public int Id { get; set; }

        [Required(ErrorMessage = "Por favor ingrese un nombre.")]
        [StringLength(256, ErrorMessage = "Sobrepas� el tama�o m�ximo de caracteres.")]
        public string Nombre { get; set; }

        [Display(Name = "C�digo Tipo de Especialidad")]
        public int TipoId { get; set; }

        [Display(Name = "Tipo Especialidad")]
        public virtual TipoEspecialidad TipoEspecialidad { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Examen> Examen { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Recetario> Recetarios { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Medico> Medicos { get; set; }
    }
}
