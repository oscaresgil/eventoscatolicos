﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using EventosCatolicos.Models;
using EventosCatolicos.ViewModels;
using System.Threading.Tasks;


namespace EventosCatolicos.Controllers
{
    public class MunicipiosController : Controller
    {
        private readonly ApplicationDbContext _db = new ApplicationDbContext();

        // GET: Municipios
        [Authorize(Roles = "Ver Municipios,IT")]
        public ActionResult Index()
        {
            var municipiosViewModel = _db.Municipios.Select(
                @municipio => new MunicipioInfoViewModel
                {
                    Id = @municipio.Id,
                    Nombre = @municipio.Nombre,
                    Departamento = _db.Departamentos.FirstOrDefault(d => d.Id == @municipio.DepartamentoId).Nombre
                }).ToList();
            return View(municipiosViewModel);
        }

        // GET: Municipios/Details/5
        [Authorize(Roles = "Ver Municipios,IT")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Municipio municipio = _db.Municipios.Find(id);
            if (municipio == null)
            {
                return HttpNotFound();
            }
            var municipioViewModel = new MunicipioInfoViewModel 
            {
                Id = municipio.Id,
                Nombre = municipio.Nombre,
                Departamento = _db.Departamentos.First(d => d.Id == municipio.DepartamentoId).Nombre
            };
            return View(municipioViewModel);
        }

        // GET: Municipios/Create
        [Authorize(Roles = "Administrar Municipios,IT")]
        public ActionResult Create()
        {
            ViewBag.DepartamentoId = new SelectList(_db.Departamentos, "Id", "Nombre", 1);
            return View();
        }

        // POST: Municipios/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrar Municipios,IT")]
        public ActionResult Create([Bind(Include = "Id,Nombre,DepartamentoId")] MunicipioModifyViewModel model)
        {
            if (ModelState.IsValid)
            {
                var municipio = new Municipio 
                {
                    Id = model.Id,
                    Nombre = model.Nombre,
                    DepartamentoId = model.DepartamentoId
                };
                
                _db.Municipios.Add(municipio);
                _db.SaveChanges();
                
                return RedirectToAction("Index");
            }

            ViewBag.DepartamentoId = new SelectList(_db.Departamentos, "Id", "Nombre", model.DepartamentoId);
            return View(model);
        }

        // GET: Municipios/Edit/5
        [Authorize(Roles = "Administrar Municipios,IT")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Municipio municipio = _db.Municipios.Find(id);
            if (municipio == null)
            {
                return HttpNotFound();
            }
            var municipioViewModel = new MunicipioModifyViewModel
            {
                Id = municipio.Id,
                Nombre = municipio.Nombre,
            };
            ViewBag.DepartamentoId = new SelectList(_db.Departamentos, "Id", "Nombre", 1);
            return View(municipioViewModel);
        }

        // POST: Municipios/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrar Municipios,IT")]
        public ActionResult Edit([Bind(Include = "Id,Nombre,DepartamentoId")] MunicipioModifyViewModel model)
        {
            if (ModelState.IsValid)
            {
                var municipio = new Municipio
                {
                    Id = model.Id,
                    Nombre = model.Nombre,
                    DepartamentoId = model.DepartamentoId
                };
                _db.Municipios.Add(municipio);
                _db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.DepartamentoId = new SelectList(_db.Departamentos, "Id", "Nombre", model.DepartamentoId);
            return View(model);
        }

        // GET: Municipios/Delete/5
        [Authorize(Roles = "Eliminar Municipios,IT")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Municipio municipio = _db.Municipios.Find(id);
            if (municipio == null)
            {
                return HttpNotFound();
            }
            return View(municipio);
        }

        // POST: Municipios/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Eliminar Municipios,IT")]
        public ActionResult DeleteConfirmed(int id)
        {
            Municipio municipio = _db.Municipios.Find(id);
            _db.Municipios.Remove(municipio);
            _db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
