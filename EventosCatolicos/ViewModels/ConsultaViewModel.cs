﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using EventosCatolicos.Models;

namespace EventosCatolicos.ViewModels
{
    public class IndexConsultaViewModel
    {
        public int Id { get; set; }

        [Display(Name = "Código Paciente")]
        [Required(ErrorMessage = "Por favor ingrese el código del paciente.")]
        public int PacienteId { get; set; }

        [Display(Name = "Paciente")]
        public string Paciente { get; set; }
        
        [Display(Name = "Médico")]
        public string Medico { get; set; }

        [Display(Name = "Estado")]
        public string Estado { get; set; }

        [Display(Name = "Clínica")]
        public string Clinica { get; set; }

        [Display(Name = "Fecha Consulta")]
        public string FechaConsultaView { get; set; }
    
        [Display(Name = "Próxima Cita")]
        public string ProximaCitaView { get; set; }

        [Display(Name = "Fecha Consulta")]
        public DateTime? FechaConsulta { get; set; }
    
        [Display(Name = "Próxima Cita")]
        public DateTime? ProximaCita { get; set; }

        [Display(Name = "Cobro")]
        public int CobroId { get; set; }
    }

    public class GenerateConsultaViewModel : IndexConsultaViewModel
    {

        [Display(Name = "Clínica")]
        public int? ClinicaId { get; set; }

        [Display(Name = "Código Médico")]
        public int? MedicoId { get; set; }

        [Display(Name = "Estado")]
        public int EstadoId { get; set; }
    }

    public class ConsultaViewModel : GenerateConsultaViewModel
    {
        public ConsultaViewModel()
        {
            DetallesAntecedentes = new List<DetallesAntecedente>();
            DetallesDiagnosticos = new List<DetallesDiagnostico>();
            DetallesRecetarios = new List<DetallesRecetario>();
            DetallesSintomas = new List<DetallesSintoma>();
            DetallesTratamientos = new List<DetallesTratamiento>();
            DetallesExamenes = new List<DetallesExamen>();
        }

        [Display(Name = "Motivo")]
        [StringLength(256, ErrorMessage = "Sobrepasó el tamaño máximo de caracteres.")]
        public string Motivo { get; set; }

        [Display(Name = "Observaciones")]
        public string Observaciones { get; set; }

        [Display(Name = "PresiónArterial")]
        public double? PresionArterial { get; set; }

        [Display(Name = "Peso")]
        public double? Peso { get; set; }

        [Display(Name = "Temperatura")]
        public double? Temperatura { get; set; }

        [Display(Name = "Frecuencia Cardiaca")]
        public double? FrecuenciaCardiaca { get; set; }

        [Display(Name = "Frecuencia Respiratoria")]
        public double? FrecuenciaRespiratoria { get; set; }

        [Display(Name = "Masa Corporal")]
        public double? MasaCorporal { get; set; }

        [Display(Name = "Antecedentes")]
        public IList<DetallesAntecedente> DetallesAntecedentes { get; set; }

        [Display(Name = "Diagnósticos")]
        public IList<DetallesDiagnostico> DetallesDiagnosticos { get; set; }

        [Display(Name = "Recetarios")]
        public IList<DetallesRecetario> DetallesRecetarios { get; set; }

        [Display(Name = "Síntomas")]
        public IList<DetallesSintoma> DetallesSintomas { get; set; }
        
        [Display(Name = "Tratamientos")]
        public IList<DetallesTratamiento> DetallesTratamientos { get; set; }
        
        [Display(Name = "Exámenes")]
        public IList<DetallesExamen> DetallesExamenes { get; set; }
    
    }
}