﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using EventosCatolicos.Models;
using EventosCatolicos.Servicios;
using EventosCatolicos.ViewModels;
using X.PagedList;

namespace EventosCatolicos.Controllers
{
    public class PacientesController : Controller
    {
        private readonly ApplicationDbContext _db = new ApplicationDbContext();

        // GET: Pacientes
        [Authorize(Roles = "Ver Pacientes,IT")]
        public ActionResult Index()
        {
            var pacientesModel = _db.Pacientes.Include(p => p.ApplicationUser).Include(p => p.ApplicationUser1).Include(p => p.EstadoCivil).Include(p => p.EstadoCivil1).Include(p => p.Municipio).Include(p => p.Municipio1).Include(p => p.Nacionalidad).Include(p => p.Nacionalidad1).Include(p => p.Profesion).Include(p => p.Sexo).Include(p => p.Sexo1).Include(p => p.Talla).Include(p => p.TipoDireccion).Include(p => p.TipoDireccion1).Include(p => p.TipoIdentificacion).Include(p => p.TipoIdentificacion1).Include(p => p.TipoSangre).Include(p => p.Archivo);
            var pacientes = pacientesModel.Select(
                @paciente => new PacientesViewModel
                {
                    Id = @paciente.Id,
                    PrimerNombre = @paciente.PrimerNombre,
                    SegundoNombre = @paciente.SegundoNombre,
                    PrimerApellido = @paciente.PrimerApellido,
                    SegundoApellido = @paciente.SegundoApellido,
                    ApellidoCasada = @paciente.ApellidoCasada,
                    Archivo = @paciente.Archivo.Nombre,
                    CorreoElectronico = @paciente.CorreoElectronico,
                    EstadoCivil = @paciente.EstadoCivil.Nombre,
                    FechaNacimiento = @paciente.FechaNacimiento,
                    TipoIdentificacion = @paciente.TipoIdentificacion.Nombre,
                    NoIdentificacion = @paciente.NoIdentificacion,
                    Nacionalidad = @paciente.Nacionalidad.Nombre,
                    Sexo = @paciente.Sexo.Nombre
                });
            return View(pacientes.ToList());
        }

        // GET: Pacientes/Details/5
        [Authorize(Roles = "Ver Pacientes,IT")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Paciente paciente = _db.Pacientes.Find(id);
            if (paciente == null)
            {
                return HttpNotFound();
            }
            return View(paciente);
        }



        [Authorize(Roles = "Ver Pacientes,IT")]
        public ActionResult Historial(int id, int? page)
        {
            Paciente paciente = _db.Pacientes.Find(id);
            if (paciente == null)
            {
                return HttpNotFound();
            }
            var historial = _db.Consultas
                .Where(c => c.PacienteId == paciente.Id)
                .OrderByDescending(c => c.FechaConsulta)
                .AsEnumerable()
                .Select(consulta => new ConsultaViewModel
                {
                    Id = consulta.Id,
                    CobroId = consulta.CobroId,
                    PacienteId = consulta.PacienteId,
                    Paciente = consulta.Paciente != null ?
                                consulta.Paciente.PrimerNombre + " " +
                                consulta.Paciente.SegundoNombre + " " +
                                consulta.Paciente.PrimerApellido + " " +
                                consulta.Paciente.SegundoApellido
                                : "No definido",
                    FechaConsultaView = Utils.GetDate(consulta.FechaConsulta),
                    ClinicaId = consulta.ClinicaId,
                    MedicoId = consulta.MedicoId,
                    Medico = consulta.Medico != null ?
                                consulta.Medico.PrimerNombre + " " +
                                consulta.Medico.PrimerApellido
                                : "No definido",
                    Estado = consulta.EstadoConsulta != null ? consulta.EstadoConsulta.Nombre : "",
                    Motivo = consulta.Motivo,
                    ProximaCitaView = Utils.GetDate(consulta.ProximaCita),
                    Observaciones = consulta.Observaciones,
                    Peso = consulta.Peso,
                    Temperatura = consulta.Temperatura,
                    FrecuenciaCardiaca = consulta.FrecuenciaCardiaca,
                    FrecuenciaRespiratoria = consulta.FrecuenciaRespiratoria,
                    MasaCorporal = consulta.MasaCorporal,
                    DetallesSintomas = consulta.DetallesSintomas.ToList(),
                    DetallesAntecedentes = consulta.DetallesAntecedentes.ToList(),
                    DetallesDiagnosticos = consulta.DetallesDiagnosticos.ToList(),
                    DetallesTratamientos = consulta.DetallesTratamientos.ToList(),
                    DetallesRecetarios = consulta.DetallesRecetarios.ToList(),
                    DetallesExamenes = consulta.DetallesExamenes.ToList()
                }).ToPagedList(page ?? 1, 1);
            return View(historial);
        }

        // GET: Pacientes/PantallaArchivo
        [Authorize]
        public ActionResult PantallaArchivo()
        {
            //Response.AddHeader("Refresh", "10");
            var estadoActivo = _db.EstadoConsultas.First(e => e.Nombre.Equals("Activo")).Id;
            var estadoAtendiendo = _db.EstadoConsultas.First(e => e.Nombre.Equals("Atendiendo")).Id;
            var pacientes = _db.Consultas
                .Where(c => c.FechaConsulta.Value == DateTime.Today && 
                    (c.EstadoId == estadoActivo || c.EstadoId == estadoAtendiendo) && 
                    c.EstadoArchivo == 0)
                .OrderBy(c => c.CreatedAt)
                .Take(10)
                .AsEnumerable()
                .Select(c =>
                {
                    var paciente = _db.Pacientes.Find(c.PacienteId);
                    var model = new PantallaArchivoViewModel
                    {
                        Id = paciente.Id,
                        IdConsulta = c.Id,
                        Nombre =    paciente.PrimerNombre + " " +
                                    paciente.SegundoNombre + " " +
                                    paciente.PrimerApellido + " " +
                                    paciente.SegundoApellido + " " +
                                    paciente.ApellidoCasada,
                        Archivo = paciente.Archivo != null ? paciente.Archivo.Nombre : "",
                        Clinica = c.Clinica.Nombre
                    };
                    return model;
                });

            return View(pacientes);
        }

        public ActionResult SiguientePaciente(int id)
        {
            
            Consulta consulta = _db.Consultas.Find(id);
            consulta.EstadoArchivo = 1;
            _db.Entry(consulta).State = EntityState.Modified;
            _db.SaveChanges();
            return RedirectToAction("PantallaArchivo");
        }

        // GET: Pacientes/Create
        [Authorize(Roles = "Administrar Pacientes,IT")]
        public ActionResult Create()
        {
            ViewBag.DepartamentoId = new SelectList(_db.Departamentos, "Id", "Nombre");
            ViewBag.RepDepartamentoId = new SelectList(_db.Departamentos, "Id", "Nombre");
            ViewBag.EstadoCivilId = new SelectList(_db.EstadosCiviles, "Id", "Nombre");
            ViewBag.RepEstadoCivilId = new SelectList(_db.EstadosCiviles, "Id", "Nombre");
            ViewBag.MunicipioId = new SelectList(_db.Municipios, "Id", "Nombre");
            ViewBag.RepMunicipioId = new SelectList(_db.Municipios, "Id", "Nombre");
            ViewBag.NacionalidadId = new SelectList(_db.Nacionalidades, "Id", "Nombre");
            ViewBag.RepNacionalidadId = new SelectList(_db.Nacionalidades, "Id", "Nombre");
            ViewBag.ProfesionId = new SelectList(_db.Profesiones, "Id", "Nombre");
            ViewBag.SexoId = new SelectList(_db.Sexos, "Id", "Nombre");
            ViewBag.RepSexoId = new SelectList(_db.Sexos, "Id", "Nombre");
            ViewBag.TallaId = new SelectList(_db.Tallas, "Id", "Nombre");
            ViewBag.TipoDireccionId = new SelectList(_db.TiposDireccion, "Id", "Nombre");
            ViewBag.RepTipoDireccionId = new SelectList(_db.TiposDireccion, "Id", "Nombre");
            ViewBag.TipoIdentificacionId = new SelectList(_db.TiposIdentificacion, "Id", "Nombre");
            ViewBag.RepTipoIdentificacionId = new SelectList(_db.TiposIdentificacion, "Id", "Nombre");
            ViewBag.TipoSangreId = new SelectList(_db.TiposSangre, "Id", "Nombre");
            ViewBag.ArchivoId = new SelectList(_db.Archivos, "Id", "Nombre");
            return View();
        }

        // POST: Pacientes/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrar Pacientes,IT")]
        public ActionResult Create([Bind(Include = "Id,PrimerNombre,SegundoNombre,PrimerApellido,SegundoApellido,ApellidoCasada,FechaNacimiento,TipoIdentificacionId,NoIdentificacion,NacionalidadId,SexoId,TallaId,TipoSangreId,EstadoCivilId,CorreoElectronico,TipoDireccionId,OtroTipoDireccion,NoCasa,Zona,MunicipioId,TelefonoCasa,TelefonoCelular,ProfesionId,Religion,ContactoNombre,ContactoTelefono,ContactoCelular,ContactoDireccion,ContactoCorreo,RepPrimerNombre,RepSegundoNombre,RepPrimerApellido,RepSegundoApellido,RepTipoIdentificacionId,RepNoIdentificacion,RepNacionalidadId,RepSexoId,RepFechaNacimiento,RepEstadoCivilId,RepCorreoElectronico,RepTipoDireccionId,RepOtroTipoDireccion,RepNoCasa,RepZona,RepMunicipioId,ArchivoId")] Paciente paciente)
        {
            if (ModelState.IsValid)
            {
                paciente.CreatedBy = User.Identity.GetUserId();
                paciente.ModifiedBy = User.Identity.GetUserId();
                paciente.CreatedAt = DateTime.Now;
                paciente.ModifiedAt = DateTime.Now;
                paciente.CreatedFrom = RequestHelpers.GetClientIpAddress(Request);
                paciente.ModifiedFrom = RequestHelpers.GetClientIpAddress(Request);
                _db.Pacientes.Add(paciente);
                _db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.EstadoCivilId = new SelectList(_db.EstadosCiviles, "Id", "Nombre", paciente.EstadoCivilId);
            ViewBag.RepEstadoCivilId = new SelectList(_db.EstadosCiviles, "Id", "Nombre", paciente.RepEstadoCivilId);
            ViewBag.MunicipioId = new SelectList(_db.Municipios, "Id", "Nombre", paciente.MunicipioId);
            ViewBag.RepMunicipioId = new SelectList(_db.Municipios, "Id", "Nombre", paciente.RepMunicipioId);
            ViewBag.NacionalidadId = new SelectList(_db.Nacionalidades, "Id", "Nombre", paciente.NacionalidadId);
            ViewBag.RepNacionalidadId = new SelectList(_db.Nacionalidades, "Id", "Nombre", paciente.RepNacionalidadId);
            ViewBag.ProfesionId = new SelectList(_db.Profesiones, "Id", "Nombre", paciente.ProfesionId);
            ViewBag.SexoId = new SelectList(_db.Sexos, "Id", "Nombre", paciente.SexoId);
            ViewBag.RepSexoId = new SelectList(_db.Sexos, "Id", "Nombre", paciente.RepSexoId);
            ViewBag.TallaId = new SelectList(_db.Tallas, "Id", "Nombre", paciente.TallaId);
            ViewBag.TipoDireccionId = new SelectList(_db.TiposDireccion, "Id", "Nombre", paciente.TipoDireccionId);
            ViewBag.RepTipoDireccionId = new SelectList(_db.TiposDireccion, "Id", "Nombre", paciente.RepTipoDireccionId);
            ViewBag.TipoIdentificacionId = new SelectList(_db.TiposIdentificacion, "Id", "Nombre", paciente.TipoIdentificacionId);
            ViewBag.RepTipoIdentificacionId = new SelectList(_db.TiposIdentificacion, "Id", "Nombre", paciente.RepTipoIdentificacionId);
            ViewBag.TipoSangreId = new SelectList(_db.TiposSangre, "Id", "Nombre", paciente.TipoSangreId);
            ViewBag.ArchivoId = new SelectList(_db.Archivos, "Id", "Nombre");
            return View(paciente);
        }

        // GET: Pacientes/Edit/5
        [Authorize(Roles = "Administrar Pacientes,IT")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Paciente paciente = _db.Pacientes.Find(id);
            if (paciente == null)
            {
                return HttpNotFound();
            }
            ViewBag.EstadoCivilId = new SelectList(_db.EstadosCiviles, "Id", "Nombre", paciente.EstadoCivilId);
            ViewBag.RepEstadoCivilId = new SelectList(_db.EstadosCiviles, "Id", "Nombre", paciente.RepEstadoCivilId);
            ViewBag.MunicipioId = new SelectList(_db.Municipios, "Id", "Nombre", paciente.MunicipioId);
            ViewBag.RepMunicipioId = new SelectList(_db.Municipios, "Id", "Nombre", paciente.RepMunicipioId);
            ViewBag.NacionalidadId = new SelectList(_db.Nacionalidades, "Id", "Nombre", paciente.NacionalidadId);
            ViewBag.RepNacionalidadId = new SelectList(_db.Nacionalidades, "Id", "Nombre", paciente.RepNacionalidadId);
            ViewBag.ProfesionId = new SelectList(_db.Profesiones, "Id", "Nombre", paciente.ProfesionId);
            ViewBag.SexoId = new SelectList(_db.Sexos, "Id", "Nombre", paciente.SexoId);
            ViewBag.RepSexoId = new SelectList(_db.Sexos, "Id", "Nombre", paciente.RepSexoId);
            ViewBag.TallaId = new SelectList(_db.Tallas, "Id", "Nombre", paciente.TallaId);
            ViewBag.TipoDireccionId = new SelectList(_db.TiposDireccion, "Id", "Nombre", paciente.TipoDireccionId);
            ViewBag.RepTipoDireccionId = new SelectList(_db.TiposDireccion, "Id", "Nombre", paciente.RepTipoDireccionId);
            ViewBag.TipoIdentificacionId = new SelectList(_db.TiposIdentificacion, "Id", "Nombre", paciente.TipoIdentificacionId);
            ViewBag.RepTipoIdentificacionId = new SelectList(_db.TiposIdentificacion, "Id", "Nombre", paciente.RepTipoIdentificacionId);
            ViewBag.TipoSangreId = new SelectList(_db.TiposSangre, "Id", "Nombre", paciente.TipoSangreId);
            ViewBag.ArchivoId = new SelectList(_db.Archivos, "Id", "Nombre");
            return View(paciente);
        }

        // POST: Pacientes/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrar Pacientes,IT")]
        public ActionResult Edit([Bind(Include = "Id,PrimerNombre,SegundoNombre,PrimerApellido,SegundoApellido,ApellidoCasada,FechaNacimiento,TipoIdentificacionId,NoIdentificacion,NacionalidadId,SexoId,TallaId,TipoSangreId,EstadoCivilId,CorreoElectronico,TipoDireccionId,OtroTipoDireccion,NoCasa,Zona,MunicipioId,TelefonoCasa,TelefonoCelular,ProfesionId,Religion,ContactoNombre,ContactoTelefono,ContactoCelular,ContactoDireccion,ContactoCorreo,RepPrimerNombre,RepSegundoNombre,RepPrimerApellido,RepSegundoApellido,RepTipoIdentificacionId,RepNoIdentificacion,RepNacionalidadId,RepSexoId,RepFechaNacimiento,RepEstadoCivilId,RepCorreoElectronico,RepTipoDireccionId,RepOtroTipoDireccion,RepNoCasa,RepZona,RepMunicipioId,RepDepartamentoId,ArchivoId")] Paciente paciente)
        {
            if (ModelState.IsValid)
            {
                paciente.ModifiedBy = User.Identity.GetUserId();
                paciente.ModifiedAt = DateTime.Now;
                paciente.ModifiedFrom = RequestHelpers.GetClientIpAddress(Request);
                _db.Entry(paciente).State = EntityState.Modified;
                _db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.EstadoCivilId = new SelectList(_db.EstadosCiviles, "Id", "Nombre", paciente.EstadoCivilId);
            ViewBag.RepEstadoCivilId = new SelectList(_db.EstadosCiviles, "Id", "Nombre", paciente.RepEstadoCivilId);
            ViewBag.MunicipioId = new SelectList(_db.Municipios, "Id", "Nombre", paciente.MunicipioId);
            ViewBag.RepMunicipioId = new SelectList(_db.Municipios, "Id", "Nombre", paciente.RepMunicipioId);
            ViewBag.NacionalidadId = new SelectList(_db.Nacionalidades, "Id", "Nombre", paciente.NacionalidadId);
            ViewBag.RepNacionalidadId = new SelectList(_db.Nacionalidades, "Id", "Nombre", paciente.RepNacionalidadId);
            ViewBag.ProfesionId = new SelectList(_db.Profesiones, "Id", "Nombre", paciente.ProfesionId);
            ViewBag.SexoId = new SelectList(_db.Sexos, "Id", "Nombre", paciente.SexoId);
            ViewBag.RepSexoId = new SelectList(_db.Sexos, "Id", "Nombre", paciente.RepSexoId);
            ViewBag.TallaId = new SelectList(_db.Tallas, "Id", "Nombre", paciente.TallaId);
            ViewBag.TipoDireccionId = new SelectList(_db.TiposDireccion, "Id", "Nombre", paciente.TipoDireccionId);
            ViewBag.RepTipoDireccionId = new SelectList(_db.TiposDireccion, "Id", "Nombre", paciente.RepTipoDireccionId);
            ViewBag.TipoIdentificacionId = new SelectList(_db.TiposIdentificacion, "Id", "Nombre", paciente.TipoIdentificacionId);
            ViewBag.RepTipoIdentificacionId = new SelectList(_db.TiposIdentificacion, "Id", "Nombre", paciente.RepTipoIdentificacionId);
            ViewBag.TipoSangreId = new SelectList(_db.TiposSangre, "Id", "Nombre", paciente.TipoSangreId);
            ViewBag.ArchivoId = new SelectList(_db.Archivos, "Id", "Nombre");
            return View(paciente);
        }

        // GET: Pacientes/Delete/5
        [Authorize(Roles = "Eliminar Pacientes,IT")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Paciente paciente = _db.Pacientes.Find(id);
            if (paciente == null)
            {
                return HttpNotFound();
            }
            return View(paciente);
        }

        // POST: Pacientes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Eliminar Pacientes,IT")]
        public ActionResult DeleteConfirmed(int id)
        {
            Paciente paciente = _db.Pacientes.Find(id);
            _db.Pacientes.Remove(paciente);
            _db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}
