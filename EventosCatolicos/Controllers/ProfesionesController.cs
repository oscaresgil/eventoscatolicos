﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using EventosCatolicos.Models;

namespace EventosCatolicos.Controllers
{
    public class ProfesionesController : Controller
    {
        private readonly ApplicationDbContext _db = new ApplicationDbContext();

        // GET: Profesiones
        [Authorize(Roles = "Ver Profesiones,IT")]
        public ActionResult Index()
        {
            return View(_db.Profesiones.ToList());
        }

        // GET: Profesiones/Details/5
        [Authorize(Roles = "Ver Profesiones,IT")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Profesion profesion = _db.Profesiones.Find(id);
            if (profesion == null)
            {
                return HttpNotFound();
            }
            return View(profesion);
        }

        // GET: Profesiones/Create
        [Authorize(Roles = "Administrar Profesiones,IT")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Profesiones/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrar Profesiones,IT")]
        public ActionResult Create([Bind(Include = "Id,Nombre")] Profesion profesion)
        {
            if (ModelState.IsValid)
            {
                _db.Profesiones.Add(profesion);
                _db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(profesion);
        }

        // GET: Profesiones/Edit/5
        [Authorize(Roles = "Administrar Profesiones,IT")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Profesion profesion = _db.Profesiones.Find(id);
            if (profesion == null)
            {
                return HttpNotFound();
            }
            return View(profesion);
        }

        // POST: Profesiones/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrar Profesiones,IT")]
        public ActionResult Edit([Bind(Include = "Id,Nombre")] Profesion profesion)
        {
            if (ModelState.IsValid)
            {
                _db.Entry(profesion).State = EntityState.Modified;
                _db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(profesion);
        }

        // GET: Profesiones/Delete/5
        [Authorize(Roles = "Eliminar Profesiones,IT")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Profesion profesion = _db.Profesiones.Find(id);
            if (profesion == null)
            {
                return HttpNotFound();
            }
            return View(profesion);
        }

        // POST: Profesiones/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Eliminar Profesiones,IT")]
        public ActionResult DeleteConfirmed(int id)
        {
            Profesion profesion = _db.Profesiones.Find(id);
            _db.Profesiones.Remove(profesion);
            _db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
