// ReSharper disable VirtualMemberCallInContructor
namespace EventosCatolicos.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("Medico")]
    public class Medico
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Medico()
        {
            Consultas = new HashSet<Consulta>();
            Especialidades = new HashSet<Especialidad>();
        }

        [Display(Name = "C�digo M�dico")]
        public int Id { get; set; }

        [StringLength(128, ErrorMessage = "Sobrepas� el tama�o m�ximo de caracteres.")]
        [Display(Name = "Usuario")]
        public string UsuarioId { get; set; }

        [Required(ErrorMessage = "Por favor ingrese un nombre.")]
        [StringLength(256, ErrorMessage = "Sobrepas� el tama�o m�ximo de caracteres.")]
        [Display(Name = "Primer Nombre")]
        public string PrimerNombre { get; set; }

        [StringLength(256, ErrorMessage = "Sobrepas� el tama�o m�ximo de caracteres.")]
        [Display(Name = "Segundo Nombre")]
        public string SegundoNombre { get; set; }

        [Required(ErrorMessage = "Por favor ingrese un apellido")]
        [StringLength(256, ErrorMessage = "Sobrepas� el tama�o m�ximo de caracteres.")]
        [Display(Name = "Primer Apellido")]
        public string PrimerApellido { get; set; }

        [StringLength(256, ErrorMessage = "Sobrepas� el tama�o m�ximo de caracteres.")]
        [Display(Name = "Segundo Apellido")]
        public string SegundoApellido { get; set; }

        [StringLength(256, ErrorMessage = "Sobrepas� el tama�o m�ximo de caracteres.")]
        [Display(Name = "Apellido de Casada")]
        public string ApellidoCasada { get; set; }

        [StringLength(500, ErrorMessage = "Sobrepas� el tama�o m�ximo de caracteres.")]
        [Display(Name = "Direcci�n")]
        public string Direccion { get; set; }

        [StringLength(50, ErrorMessage = "Sobrepas� el tama�o m�ximo de caracteres.")]
        [Display(Name = "Tel�fono de Casa")]
        public string TelefonoCasa { get; set; }

        [StringLength(256, ErrorMessage = "Sobrepas� el tama�o m�ximo de caracteres.")]
        [Display(Name = "Tel�fono Celular")]
        public string TelefonoCelular { get; set; }

        [StringLength(256, ErrorMessage = "Sobrepas� el tama�o m�ximo de caracteres.")]
        [Display(Name = "Correo Electr�nico")]
        [EmailAddress(ErrorMessage = "Ingrese un formato de correo.")]
        public string CorreoElectronico { get; set; }

        [Display(Name = "Hora Entrada")]
        public TimeSpan? HoraEntrada { get; set; }

        [Display(Name = "Hora Salida")]
        public TimeSpan? HoraSalida { get; set; }

        [Column(TypeName = "datetime2")]
        [Display(Name = "Fecha de Alta")]
        public DateTime? FechaAlta { get; set; }

        [StringLength(500, ErrorMessage = "Sobrepas� el tama�o m�ximo de caracteres.")]
        public string Observaciones { get; set; }

        [StringLength(128)]
        [Display(Name = "Creado Por")]
        public string CreatedBy { get; set; }

        [Column(TypeName = "datetime2")]
        [Display(Name = "Fecha de Creacion")]
        public DateTime CreatedAt { get; set; }

        [StringLength(50)]
        [Display(Name = "Creado Desde")]
        public string CreatedFrom { get; set; }

        [StringLength(128)]
        [Display(Name = "Modificado Por")]
        public string ModifiedBy { get; set; }

        [Column(TypeName = "datetime2")]
        [Display(Name = "Fecha de Modificaci�n")]
        public DateTime ModifiedAt { get; set; }

        [StringLength(50)]
        [Display(Name = "Modificado Desde")]
        public string ModifiedFrom { get; set; }

        public virtual ApplicationUser ApplicationUser { get; set; }

        public virtual ApplicationUser ApplicationUser1 { get; set; }

        public virtual ApplicationUser ApplicationUser2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Consulta> Consultas { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Especialidad> Especialidades { get; set; }
    }
}
