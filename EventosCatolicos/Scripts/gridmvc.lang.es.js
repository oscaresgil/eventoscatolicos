﻿/***
* Grid.Mvc spanish language (es-ES) http://gridmvc.codeplex.com/
*/
window.GridMvc = window.GridMvc || {};
window.GridMvc.lang = window.GridMvc.lang || {};
GridMvc.lang.es = {
    filterTypeLabel: "Tipo de filtro: ",
    filterValueLabel: "Filtro:",
    applyFilterButtonText: "Aplicar filtro",
    filterSelectTypes: {
        Equals: "Igual a",
        StartsWith: "Empieza con",
        Contains: "Contiene",
        EndsWith: "Termina con",
        GreaterThan: "Mayor que",
        LessThan: "Menor que"
    },
    code: 'es',
    boolTrueLabel: "Verdadero",
    boolFalseLabel: "Falso",
    clearFilterLabel: "Limpiar filtro"
};