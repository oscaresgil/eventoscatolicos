﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using EventosCatolicos.Models;
using EventosCatolicos.Servicios;

namespace EventosCatolicos.Controllers
{
    public class RecetariosController : Controller
    {
        private readonly ApplicationDbContext _db = new ApplicationDbContext();

        // GET: Recetarios
        [Authorize(Roles = "Ver Recetarios,IT")]
        public ActionResult Index()
        {
            var recetarios = _db.Recetarios.Include(p => p.ApplicationUser).Include(p => p.ApplicationUser1).Include(p => p.AreaRecetario).Include(p => p.Especialidad);
            return View(recetarios.ToList());
        }

        // GET: Recetarios/Details/5
        [Authorize(Roles = "Ver Recetarios,IT")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Recetario recetario = _db.Recetarios.Find(id);
            if (recetario == null)
            {
                return HttpNotFound();
            }
            return View(recetario);
        }

        // GET: Recetarios/Create
        [Authorize(Roles = "Administrar Recetarios,IT")]
        public ActionResult Create()
        {
            ViewBag.AreaId = new SelectList(_db.AreaRecetarios, "Id", "Nombre");
            ViewBag.EspecialidadId = new SelectList(_db.Especialidades, "Id", "Nombre");
            return View();
        }

        // POST: Recetarios/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrar Recetarios,IT")]
        public ActionResult Create([Bind(Include = "Id,Nombre,Descripcion,EspecialidadId,AreaId,PrecioPublico,PrecioEspecial,FechaAlta,FechaBaja,Observacion")] Recetario recetario)
        {
            if (ModelState.IsValid)
            {
                recetario.CreatedBy = User.Identity.GetUserId();
                recetario.ModifiedBy = User.Identity.GetUserId();
                recetario.CreatedAt = System.DateTime.Now;
                recetario.ModifiedAt = System.DateTime.Now;
                recetario.CreatedFrom = RequestHelpers.GetClientIpAddress(Request);
                recetario.ModifiedFrom = RequestHelpers.GetClientIpAddress(Request);
                _db.Recetarios.Add(recetario);
                _db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.AreaId = new SelectList(_db.AreaRecetarios, "Id", "Nombre", recetario.AreaId);
            ViewBag.EspecialidadId = new SelectList(_db.Especialidades, "Id", "Nombre", recetario.EspecialidadId);
            return View(recetario);
        }

        // GET: Recetarios/Edit/5
        [Authorize(Roles = "Administrar Recetarios,IT")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Recetario recetario = _db.Recetarios.Find(id);
            if (recetario == null)
            {
                return HttpNotFound();
            }
            ViewBag.AreaId = new SelectList(_db.AreaRecetarios, "Id", "Nombre", recetario.AreaId);
            ViewBag.EspecialidadId = new SelectList(_db.Especialidades, "Id", "Nombre", recetario.EspecialidadId);
            return View(recetario);
        }

        // POST: Recetarios/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrar Recetarios,IT")]
        public ActionResult Edit([Bind(Include = "Id,Nombre,Descripcion,EspecialidadId,AreaId,PrecioPublico,PrecioEspecial,FechaAlta,FechaBaja,Observacion")] Recetario recetario)
        {
            if (ModelState.IsValid)
            {

                recetario.ModifiedBy = User.Identity.GetUserId();
                recetario.ModifiedAt = System.DateTime.Now;
                recetario.ModifiedFrom = RequestHelpers.GetClientIpAddress(Request);
                _db.Entry(recetario).State = EntityState.Modified;
                _db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.AreaId = new SelectList(_db.AreaRecetarios, "Id", "Nombre", recetario.AreaId);
            ViewBag.EspecialidadId = new SelectList(_db.Especialidades, "Id", "Nombre", recetario.EspecialidadId);
            return View(recetario);
        }

        // GET: Recetarios/Delete/5
        [Authorize(Roles = "Eliminar Recetarios,IT")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Recetario recetario = _db.Recetarios.Find(id);
            if (recetario == null)
            {
                return HttpNotFound();
            }
            return View(recetario);
        }

        // POST: Recetarios/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Eliminar Recetarios,IT")]
        public ActionResult DeleteConfirmed(int id)
        {
            Recetario recetario = _db.Recetarios.Find(id);
            _db.Recetarios.Remove(recetario);
            _db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
