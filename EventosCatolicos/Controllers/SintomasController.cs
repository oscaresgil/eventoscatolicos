﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using EventosCatolicos.Models;

namespace EventosCatolicos.Controllers
{
    public class SintomasController : Controller
    {
        private readonly ApplicationDbContext _db = new ApplicationDbContext();

        // GET: Sintomas
        [Authorize(Roles = "Ver Sintomas,IT")]
        public ActionResult Index()
        {
            return View(_db.Sintomas.ToList());
        }

        // GET: Sintomas/Details/5
        [Authorize(Roles = "Ver Sintomas,IT")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Sintoma sintoma = _db.Sintomas.Find(id);
            if (sintoma == null)
            {
                return HttpNotFound();
            }
            return View(sintoma);
        }

        // GET: Sintomas/Create
        [Authorize(Roles = "Administrar Sintomas,IT")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Sintomas/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrar Sintomas,IT")]
        public ActionResult Create([Bind(Include = "Id,Nombre,Descripcion")] Sintoma sintoma)
        {
            if (ModelState.IsValid)
            {
                _db.Sintomas.Add(sintoma);
                _db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(sintoma);
        }

        // GET: Sintomas/Edit/5
        [Authorize(Roles = "Administrar Sintomas,IT")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Sintoma sintoma = _db.Sintomas.Find(id);
            if (sintoma == null)
            {
                return HttpNotFound();
            }
            return View(sintoma);
        }

        // POST: Sintomas/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrar Sintomas,IT")]
        public ActionResult Edit([Bind(Include = "Id,Nombre,Descripcion")] Sintoma sintoma)
        {
            if (ModelState.IsValid)
            {
                _db.Entry(sintoma).State = EntityState.Modified;
                _db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(sintoma);
        }

        // GET: Sintomas/Delete/5
        [Authorize(Roles = "Eliminar Sintomas,IT")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Sintoma sintoma = _db.Sintomas.Find(id);
            if (sintoma == null)
            {
                return HttpNotFound();
            }
            return View(sintoma);
        }

        // POST: Sintomas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Eliminar Sintomas,IT")]
        public ActionResult DeleteConfirmed(int id)
        {
            Sintoma sintoma = _db.Sintomas.Find(id);
            _db.Sintomas.Remove(sintoma);
            _db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
