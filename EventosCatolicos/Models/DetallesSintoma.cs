namespace EventosCatolicos.Models
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("DetallesSintoma")]
    public class DetallesSintoma
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Display(Name = "Consulta")]
        public int ConsultaId { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Display(Name = "S�ntoma")]
        public int SintomaId { get; set; }

        [StringLength(256, ErrorMessage = "Sobrepas� el tama�o m�ximo de caracteres.")]
        public string Observacion { get; set; }

        public virtual Consulta Consulta { get; set; }

        public virtual Sintoma Sintoma { get; set; }
    }
}
