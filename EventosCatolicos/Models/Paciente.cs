// ReSharper disable VirtualMemberCallInContructor
namespace EventosCatolicos.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("Paciente")]
    public class Paciente
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Paciente()
        {
            Consultas = new HashSet<Consulta>();
        }

        // TODO: auto incremental iniciando desde 500,000
        [Display(Name = "C�digo Paciente")]
        public int Id { get; set; }

        [Required(ErrorMessage = "Por favor ingrese un nombre.")]
        [StringLength(256, ErrorMessage = "Sobrepas� el tama�o m�ximo de caracteres.")]
        [Display(Name = "Primer Nombre")]
        public string PrimerNombre { get; set; }

        [StringLength(256, ErrorMessage = "Sobrepas� el tama�o m�ximo de caracteres.")]
        [Display(Name = "Segundo Nombre")]
        public string SegundoNombre { get; set; }

        [Required(ErrorMessage = "Por favor ingrese un apellido.")]
        [StringLength(256, ErrorMessage = "Sobrepas� el tama�o m�ximo de caracteres.")]
        [Display(Name = "Primer Apellido")]
        public string PrimerApellido { get; set; }

        [StringLength(256, ErrorMessage = "Sobrepas� el tama�o m�ximo de caracteres.")]
        [Display(Name = "Segundo Apellido")]
        public string SegundoApellido { get; set; }

        [StringLength(256, ErrorMessage = "Sobrepas� el tama�o m�ximo de caracteres.")]
        [Display(Name = "Apellido Casada")]
        public string ApellidoCasada { get; set; }

        [Required(ErrorMessage = "Por favor ingrese una fecha de nacimiento.")]
        [Display(Name = "Fecha de Nacimiento")]
        [Column(TypeName = "datetime2")]
        public DateTime FechaNacimiento { get; set; }

        [Display(Name = "C�digo Tipo de Identificaci�n")]
        public int? TipoIdentificacionId { get; set; }

        [StringLength(50, ErrorMessage = "Sobrepas� el tama�o m�ximo de caracteres.")]
        [Display(Name = "No. Identificaci�n")]
        public string NoIdentificacion { get; set; }

        [Display(Name = "Nacionalidad")]
        public int? NacionalidadId { get; set; }

        [Display(Name = "Archivo")]
        public int? ArchivoId { get; set; }

        [Display(Name = "Sexo")]
        public int? SexoId { get; set; }

        [Display(Name = "Talla")]
        public int? TallaId { get; set; }

        [Display(Name = "Tipo de Sangre")]
        public int? TipoSangreId { get; set; }

        [Display(Name = "Estado Civil")]
        public int? EstadoCivilId { get; set; }

        [StringLength(256, ErrorMessage = "Sobrepas� el tama�o m�ximo de caracteres.")]
        [Display(Name = "Correo Electr�nico")]
        [EmailAddress(ErrorMessage = "Ingrese un formato de correo.")]
        public string CorreoElectronico { get; set; }

        [Display(Name = "Tipo de Direcci�n")]
        public int? TipoDireccionId { get; set; }

        [StringLength(256, ErrorMessage = "Sobrepas� el tama�o m�ximo de caracteres.")]
        [Display(Name = "Otro Tipo de Direcci�n")]
        public string OtroTipoDireccion { get; set; }

        [StringLength(50, ErrorMessage = "Sobrepas� el tama�o m�ximo de caracteres.")]
        [Display(Name = "N�mero de Casa")]
        public string NoCasa { get; set; }

        [Display(Name = "Zona")]
        public int? Zona { get; set; }

        [Display(Name = "Municipio")]
        public int? MunicipioId { get; set; }

        [StringLength(50, ErrorMessage = "Sobrepas� el tama�o m�ximo de caracteres.")]
        [Display(Name = "Tel�fono de Casa")]
        public string TelefonoCasa { get; set; }

        [StringLength(50, ErrorMessage = "Sobrepas� el tama�o m�ximo de caracteres.")]
        [Display(Name = "Tel�fono Celular")]
        public string TelefonoCelular { get; set; }

        [Display(Name = "Profesi�n")]
        public int? ProfesionId { get; set; }

        [StringLength(50, ErrorMessage = "Sobrepas� el tama�o m�ximo de caracteres.")]
        [Display(Name = "Religi�n")]
        public string Religion { get; set; }

        [StringLength(256, ErrorMessage = "Sobrepas� el tama�o m�ximo de caracteres.")]
        [Display(Name = "Nombre de Contacto")]
        public string ContactoNombre { get; set; }

        [StringLength(50, ErrorMessage = "Sobrepas� el tama�o m�ximo de caracteres.")]
        [Display(Name = "Tel�fono de Contacto")]
        public string ContactoTelefono { get; set; }

        [StringLength(50, ErrorMessage = "Sobrepas� el tama�o m�ximo de caracteres.")]
        [Display(Name = "Celular de Contacto")]
        public string ContactoCelular { get; set; }

        [StringLength(256, ErrorMessage = "Sobrepas� el tama�o m�ximo de caracteres.")]
        [Display(Name = "Direcci�n de Contacto")]
        public string ContactoDireccion { get; set; }

        [StringLength(256, ErrorMessage = "Sobrepas� el tama�o m�ximo de caracteres.")]
        [Display(Name = "Correo de Contacto")]
        public string ContactoCorreo { get; set; }

        [StringLength(256, ErrorMessage = "Sobrepas� el tama�o m�ximo de caracteres.")]
        [Display(Name = "Primer Nombre ")]
        public string RepPrimerNombre { get; set; }

        [StringLength(256, ErrorMessage = "Sobrepas� el tama�o m�ximo de caracteres.")]
        [Display(Name = "Segundo Nombre ")]
        public string RepSegundoNombre { get; set; }

        [StringLength(256, ErrorMessage = "Sobrepas� el tama�o m�ximo de caracteres.")]
        [Display(Name = "Primer Apellido")]
        public string RepPrimerApellido { get; set; }

        [StringLength(256, ErrorMessage = "Sobrepas� el tama�o m�ximo de caracteres.")]
        [Display(Name = "Segundo Apellido")]
        public string RepSegundoApellido { get; set; }

        [Display(Name = "Tipo de ID")]
        public int? RepTipoIdentificacionId { get; set; }

        [StringLength(50, ErrorMessage = "Sobrepas� el tama�o m�ximo de caracteres.")]
        [Display(Name = "No. ID")]
        public string RepNoIdentificacion { get; set; }

        [Display(Name = "Nacionalidad")]
        public int? RepNacionalidadId { get; set; }

        [Display(Name = "Sexo")]
        public int? RepSexoId { get; set; }

        [Column(TypeName = "datetime2")]
        [Display(Name = "Fecha de Nacimiento")]
        public DateTime? RepFechaNacimiento { get; set; }

        [Display(Name = "Estado Civil")]
        public int? RepEstadoCivilId { get; set; }

        [StringLength(256, ErrorMessage = "Sobrepas� el tama�o m�ximo de caracteres.")]
        [Display(Name = "Correo ")]
        [EmailAddress(ErrorMessage = "Ingrese un formato de correo.")]
        public string RepCorreoElectronico { get; set; }

        [Display(Name = "Tipo de Direcci�n ")]
        public int? RepTipoDireccionId { get; set; }

        [StringLength(256, ErrorMessage = "Sobrepas� el tama�o m�ximo de caracteres.")]
        [Display(Name = "Otro Tipo de Direcci�n")]
        public string RepOtroTipoDireccion { get; set; }

        [StringLength(50, ErrorMessage = "Sobrepas� el tama�o m�ximo de caracteres.")]
        [Display(Name = "No. de Casa ")]
        public string RepNoCasa { get; set; }

        [Display(Name = "Zona")]
        public int? RepZona { get; set; }

        [Display(Name = "Municipio")]
        public int? RepMunicipioId { get; set; }

        [StringLength(128)]
        [Display(Name = "Creado Por")]
        public string CreatedBy { get; set; }

        [Column(TypeName = "datetime2")]
        [Display(Name = "Creado En")]
        public DateTime CreatedAt { get; set; }

        [StringLength(50)]
        [Display(Name = "Creado Desde")]
        public string CreatedFrom { get; set; }

        [StringLength(128)]
        [Display(Name = "Modificado Por")]
        public string ModifiedBy { get; set; }

        [Column(TypeName = "datetime2")]
        [Display(Name = "Modificado En")]
        public DateTime ModifiedAt { get; set; }

        [StringLength(50)]
        [Display(Name = "Modificado Desde")]
        public string ModifiedFrom { get; set; }

        public virtual ApplicationUser ApplicationUser { get; set; }

        public virtual ApplicationUser ApplicationUser1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Consulta> Consultas { get; set; }

        public virtual EstadoCivil EstadoCivil { get; set; }

        public virtual EstadoCivil EstadoCivil1 { get; set; }

        public virtual Municipio Municipio { get; set; }

        public virtual Municipio Municipio1 { get; set; }

        public virtual Nacionalidad Nacionalidad { get; set; }

        public virtual Nacionalidad Nacionalidad1 { get; set; }

        public virtual Archivo Archivo { get; set; }

        public virtual Profesion Profesion { get; set; }

        public virtual Sexo Sexo { get; set; }

        public virtual Sexo Sexo1 { get; set; }

        public virtual Talla Talla { get; set; }

        public virtual TipoDireccion TipoDireccion { get; set; }

        public virtual TipoDireccion TipoDireccion1 { get; set; }

        public virtual TipoIdentificacion TipoIdentificacion { get; set; }

        public virtual TipoIdentificacion TipoIdentificacion1 { get; set; }

        public virtual TipoSangre TipoSangre { get; set; }
    }
}
