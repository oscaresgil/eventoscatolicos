﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using EventosCatolicos.Models;
using EventosCatolicos.Servicios;
using EventosCatolicos.ViewModels;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace EventosCatolicos.Controllers
{
    [Authorize(Roles = "IT,Admin")]
    public class UsersAdminController : Controller
    {

        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext()
                    .GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        // Add the Group Manager (NOTE: only access through the public
        // Property, not by the instance variable!)
        private ApplicationGroupManager _groupManager;
        public ApplicationGroupManager GroupManager
        {
            get
            {
                return _groupManager ?? HttpContext.GetOwinContext()
                    .Get<ApplicationGroupManager>();
            }
            private set
            {
                _groupManager = value;
            }
        }

        private ApplicationRoleManager _roleManager;
        public ApplicationRoleManager RoleManager
        {
            get
            {
                return _roleManager ?? HttpContext.GetOwinContext()
                    .Get<ApplicationRoleManager>();
            }
            private set
            {
                _roleManager = value;
            }
        }

        public ActionResult Index()
        {
            var usersList = UserManager.Users.Select(
                @user => new ApplicationUserViewModel
                {
                    Id = @user.Id,
                    PrimerNombre = @user.PrimerNombre,
                    SegundoNombre = @user.SegundoNombre,
                    PrimerApellido = @user.PrimerApellido,
                    SegundoApellido = @user.SegundoApellido,
                    UserName = @user.UserName,
                    Email = @user.Email
                });
            return View(usersList);

        }

        public async Task<ActionResult> Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var user = await UserManager.FindByIdAsync(id);
            if (user == null)
            {
                return HttpNotFound("No se encontró el usuario");
            }
            var createdBy = await UserManager.FindByIdAsync(user.CreatedBy);
            var modifiedBy = await UserManager.FindByIdAsync(user.ModifiedBy);
            if (createdBy == null || modifiedBy == null)
            {
                user.CreatedBy = "";
                user.ModifiedBy = "";
            }
            else
            {
                user.CreatedBy = createdBy.UserName;
                user.ModifiedBy = modifiedBy.UserName;
            }
            

            var userViewModel = new DetailsApplicationUserViewModel
            {
                Id = id,
                PrimerNombre = user.PrimerNombre,
                SegundoNombre = user.SegundoNombre,
                PrimerApellido = user.PrimerApellido,
                SegundoApellido = user.SegundoApellido,
                UserName = user.UserName,
                Email = user.Email,
                CreatedBy = user.CreatedBy,
                CreatedAt = user.CreatedAt,
                CreatedFrom = user.CreatedFrom,
                ModifiedBy = user.ModifiedBy,
                ModifiedAt = user.ModifiedAt,
                ModifiedFrom = user.ModifiedFrom,
                GroupsList = await GroupManager.GetUserGroupsAsync(id)
            };
            userViewModel.GroupsCount = userViewModel.GroupsList.Count();
            return View(userViewModel);
        }


        // Create user
        public async Task<ActionResult> Create()
        {
            ViewBag.GroupsList = new SelectList(await GroupManager.Groups.ToListAsync(), "Id", "Name");
            return View();
        }


        [HttpPost]
        public async Task<ActionResult> Create(ApplicationUserViewModel model, params string[] selectedGroups)
        {
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser
                {
                    PrimerNombre = model.PrimerNombre,
                    SegundoNombre = model.SegundoNombre,
                    PrimerApellido = model.PrimerApellido,
                    SegundoApellido = model.SegundoApellido,
                    UserName = model.UserName,
                    Email = model.Email,
                    CreatedBy = User.Identity.GetUserId(),
                    ModifiedBy = User.Identity.GetUserId(),
                    CreatedAt = DateTime.Now,
                    ModifiedAt = DateTime.Now,
                    CreatedFrom = RequestHelpers.GetClientIpAddress(Request),
                    ModifiedFrom = RequestHelpers.GetClientIpAddress(Request)
                };
                var result = await UserManager.CreateAsync(user, model.Password);

                //Add User to the selected Groups 
                if (result.Succeeded)
                {
                    if (selectedGroups != null)
                    {
                        await GroupManager.SetUserGroupsAsync(user.Id, selectedGroups);
                    }
                    return RedirectToAction("Index");
                }
            }
            ViewBag.GroupsList = new SelectList(await GroupManager.Groups.ToListAsync(), "Id", "Name");
            model.Password = "";
            model.ConfirmPassword = "";
            return View(model);
        }


        public async Task<ActionResult> Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var user = await UserManager.FindByIdAsync(id);
            if (user == null)
            {
                return HttpNotFound();
            }

            // Display a list of available Groups:
            var allGroups = GroupManager.Groups;
            var userGroups = await GroupManager.GetUserGroupsAsync(id);

            var model = new EditApplicationUserViewModel
            {
                Id = id,
                PrimerNombre = user.PrimerNombre,
                SegundoNombre = user.SegundoNombre,
                PrimerApellido = user.PrimerApellido,
                SegundoApellido = user.SegundoApellido,
                UserName = user.UserName,
                Email = user.Email,
            };

            var applicationGroups = userGroups as IList<ApplicationGroup> ?? userGroups.ToList();
            foreach (var group in allGroups)
            {
                var listItem = new SelectListItem
                {
                    Text = group.Name,
                    Value = group.Id,
                    Selected = applicationGroups.Any(g => g.Id == group.Id)
                };
                model.GroupsList.Add(listItem);
            }
            return View(model);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(EditApplicationUserViewModel model, 
            params string[] selectedGroups)
        {
            if (ModelState.IsValid)
            {
                var user = await UserManager.FindByIdAsync(model.Id);
                if (user == null)
                {
                    return HttpNotFound();
                }

                // Update the User:
                user.PrimerNombre = model.PrimerNombre;
                user.SegundoNombre = model.SegundoNombre;
                user.PrimerApellido = model.PrimerApellido;
                user.SegundoApellido = model.SegundoApellido;
                user.UserName = model.UserName;
                user.Email = model.Email;
                user.ModifiedBy = User.Identity.GetUserId();
                user.ModifiedAt = DateTime.Now;
                user.ModifiedFrom = RequestHelpers.GetClientIpAddress(Request);
                await UserManager.UpdateAsync(user);

                // Update the Groups:
                if (selectedGroups != null)
                {
                    await GroupManager.SetUserGroupsAsync(user.Id, selectedGroups);
                }
                return RedirectToAction("Index");
            }
            ModelState.AddModelError("", "Algo falló.");
            return View();
        }


        public async Task<ActionResult> Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var user = await UserManager.FindByIdAsync(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            var userViewModel = new ApplicationUserViewModel
            {
                PrimerNombre = user.PrimerNombre,
                PrimerApellido = user.PrimerApellido,
                UserName = user.UserName
            };
            return View(userViewModel);
        }


        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(string id)
        {
            if (ModelState.IsValid)
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }

                var user = await UserManager.FindByIdAsync(id);
                if (user == null)
                {
                    return HttpNotFound();
                }

                // Remove all the User Group references:
                await GroupManager.ClearUserGroupsAsync(id);

                // Then Delete the User:
                var result = await UserManager.DeleteAsync(user);
                if (!result.Succeeded)
                {
                    ModelState.AddModelError("", result.Errors.First());
                    return View();
                }
                return RedirectToAction("Index");
            }
            return View();
        }
    }
}