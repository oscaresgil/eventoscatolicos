﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using EventosCatolicos.Models;

namespace EventosCatolicos.Controllers
{
    public class TiposIdentificacionController : Controller
    {
        private readonly ApplicationDbContext _db = new ApplicationDbContext();

        // GET: TiposIdentificacion
        [Authorize(Roles = "Ver TiposIdentificacion,IT")]
        public ActionResult Index()
        {
            return View(_db.TiposIdentificacion.ToList());
        }

        // GET: TiposIdentificacion/Details/5
        [Authorize(Roles = "Ver TiposIdentificacion,IT")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TipoIdentificacion tipoIdentificacion = _db.TiposIdentificacion.Find(id);
            if (tipoIdentificacion == null)
            {
                return HttpNotFound();
            }
            return View(tipoIdentificacion);
        }

        // GET: TiposIdentificacion/Create
        [Authorize(Roles = "Administrar TiposIdentificacion,IT")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: TiposIdentificacion/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Ver TiposIdentificacion,IT")]
        public ActionResult Create([Bind(Include = "Id,Nombre")] TipoIdentificacion tipoIdentificacion)
        {
            if (ModelState.IsValid)
            {
                _db.TiposIdentificacion.Add(tipoIdentificacion);
                _db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tipoIdentificacion);
        }

        // GET: TiposIdentificacion/Edit/5
        [Authorize(Roles = "Ver TiposIdentificacion,IT")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TipoIdentificacion tipoIdentificacion = _db.TiposIdentificacion.Find(id);
            if (tipoIdentificacion == null)
            {
                return HttpNotFound();
            }
            return View(tipoIdentificacion);
        }

        // POST: TiposIdentificacion/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Ver TiposIdentificacion,IT")]
        public ActionResult Edit([Bind(Include = "Id,Nombre")] TipoIdentificacion tipoIdentificacion)
        {
            if (ModelState.IsValid)
            {
                _db.Entry(tipoIdentificacion).State = EntityState.Modified;
                _db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tipoIdentificacion);
        }

        // GET: TiposIdentificacion/Delete/5
        [Authorize(Roles = "Eliminar TiposIdentificacion,IT")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TipoIdentificacion tipoIdentificacion = _db.TiposIdentificacion.Find(id);
            if (tipoIdentificacion == null)
            {
                return HttpNotFound();
            }
            return View(tipoIdentificacion);
        }

        // POST: TiposIdentificacion/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Eliminar TiposIdentificacion,IT")]
        public ActionResult DeleteConfirmed(int id)
        {
            TipoIdentificacion tipoIdentificacion = _db.TiposIdentificacion.Find(id);
            _db.TiposIdentificacion.Remove(tipoIdentificacion);
            _db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
