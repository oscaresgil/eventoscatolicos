﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using EventosCatolicos.Models;

namespace EventosCatolicos.ViewModels
{
    public class ApplicationRoleViewModel
    {
        public string Id { get; set; }
        [Required(AllowEmptyStrings = false)]
        [Display(Name = "Permiso")]
        public string Name { get; set; }
    }

    public class GroupsInRoleViewModel : ApplicationRoleViewModel
    {
        public int Count { get; set; }
        public IEnumerable<ApplicationGroup> GroupsList { get; set; }
    }

    public class ApplicationUserViewModel
    {
        public string Id { get; set; }
        [StringLength(256, ErrorMessage = "Sobrepasó el tamaño máximo de caracteres.")]
        [Required(ErrorMessage = "Por favor ingrese su nombre.")]
        [Display(Name = "Primer Nombre")]
        public string PrimerNombre { get; set; }

        [StringLength(256, ErrorMessage = "Sobrepasó el tamaño máximo de caracteres.")]
        [Display(Name = "Segundo Nombre")]
        public string SegundoNombre { get; set; }

        [StringLength(256,ErrorMessage = "Sobrepasó el tamaño máximo de caracteres.")]
        [Required(ErrorMessage = "Por favor ingrese su apellido")]
        [Display(Name = "Primer Apellido")]
        public string PrimerApellido { get; set; }

        [StringLength(256,ErrorMessage = "Sobrepasó el tamaño máximo de caracteres.")]
        [Display(Name = "Segundo Apellido")]
        public string SegundoApellido { get; set; }

        [Required(ErrorMessage = "Debe ingresar un nombre de usuario")]
        [Display(Name = "Usuario")]
        [RegularExpression("[a-z][a-z0-9_-]{3,16}$",
            ErrorMessage = "El formato de usuario no es válido: debe iniciar con una letra, contener sólo minúsculas y números, con longitud de 4 a 16 caracteres.")]
        public string UserName { get; set; }

        [EmailAddress(ErrorMessage = "Por favor ingrese el correo en formato correcto.")]
        [Display(Name = "Correo electrónico")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Debe ingresar una contraseña")]
        [StringLength(100, ErrorMessage = "El número de caracteres de {0} debe ser al menos {2}.", MinimumLength = 8)]
        [DataType(DataType.Password)]
        [Display(Name = "Contraseña")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirmar contraseña")]
        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage = "La contraseña y la contraseña de confirmación no coinciden.")]
        public string ConfirmPassword { get; set; }
    }

    public class EditApplicationUserViewModel : ApplicationUserViewModel
    {
        public EditApplicationUserViewModel()
        {
            GroupsList = new List<SelectListItem>();
        }
        public ICollection<SelectListItem> GroupsList { get; set; }
    }

    public class DetailsApplicationUserViewModel : ApplicationUserViewModel
    {
        [Display(Name = "Creado Por")]
        [StringLength(128, ErrorMessage = "Sobrepasó el tamaño máximo de caracteres.")]
        public string CreatedBy { get; set; }

        [Display(Name = "Creado En")]
        public DateTime? CreatedAt { get; set; }

        [Display(Name = "Creado Desde")]
        [StringLength(50, ErrorMessage = "Sobrepasó el tamaño máximo de caracteres.")]
        public string CreatedFrom { get; set; }

        [Display(Name = "Modificado Por")]
        [StringLength(128, ErrorMessage = "Sobrepasó el tamaño máximo de caracteres.")]
        public string ModifiedBy { get; set; }

        [Display(Name = "Modificado En")]
        public DateTime? ModifiedAt { get; set; }

        [Display(Name = "Modificado Desde")]
        [StringLength(50, ErrorMessage = "Sobrepasó el tamaño máximo de caracteres.")]
        public string ModifiedFrom { get; set; }

        public IEnumerable<ApplicationGroup> GroupsList { get; set; }
        public int GroupsCount { get; set; }
    }

    public class ApplicationGroupViewModel
    {
        public string Id { get; set; }
        [Required(AllowEmptyStrings = false)]
        [Display(Name = "Nombre")]
        public string Name { get; set; }
        [Display(Name = "Descripción")]
        public string Description { get; set; }
    }

    public class ManageGroupViewModel : ApplicationGroupViewModel
    {
        public ManageGroupViewModel()
        {
            RolesList = new List<SelectListItem>();
        }

        public ICollection<SelectListItem> RolesList { get; set; }
    }

    public class DetailsGroupViewModel : ApplicationGroupViewModel
    {
        public DetailsGroupViewModel()
        {
            RolesList = new List<ApplicationRole>();
            UsersList = new List<ApplicationUser>();
        }

        public ICollection<ApplicationRole> RolesList { get; set; }
        public ICollection<ApplicationUser> UsersList { get; set; }
        public int CountRoles { get; set; }
        public int CountUsers { get; set; }
    }
}