﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using EventosCatolicos.Models;

namespace EventosCatolicos.Controllers
{
    public class CategoriasDiagnosticoController : Controller
    {
        private readonly ApplicationDbContext _db = new ApplicationDbContext();

        // GET: CategoriasDiagnostico
        [Authorize(Roles = "Ver CategoriaDiagnostico,IT")]
        public ActionResult Index()
        {
            return View(_db.CategoriaDiagnosticos.ToList());
        }

        // GET: CategoriasDiagnostico/Details/5
        [Authorize(Roles = "Ver CategoriaDiagnostico,IT")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CategoriaDiagnostico categoriaDiagnostico = _db.CategoriaDiagnosticos.Find(id);
            if (categoriaDiagnostico == null)
            {
                return HttpNotFound();
            }
            return View(categoriaDiagnostico);
        }

        // GET: CategoriasDiagnostico/Create
        [Authorize(Roles = "Administrar CategoriaDiagnostico,IT")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: CategoriasDiagnostico/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrar CategoriaDiagnostico,IT")]
        public ActionResult Create([Bind(Include = "Id,Nombre")] CategoriaDiagnostico categoriaDiagnostico)
        {
            if (ModelState.IsValid)
            {
                _db.CategoriaDiagnosticos.Add(categoriaDiagnostico);
                _db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(categoriaDiagnostico);
        }

        // GET: CategoriasDiagnostico/Edit/5
        [Authorize(Roles = "Administrar CategoriaDiagnostico,IT")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CategoriaDiagnostico categoriaDiagnostico = _db.CategoriaDiagnosticos.Find(id);
            if (categoriaDiagnostico == null)
            {
                return HttpNotFound();
            }
            return View(categoriaDiagnostico);
        }

        // POST: CategoriasDiagnostico/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrar CategoriaDiagnostico,IT")]
        public ActionResult Edit([Bind(Include = "Id,Nombre")] CategoriaDiagnostico categoriaDiagnostico)
        {
            if (ModelState.IsValid)
            {
                _db.Entry(categoriaDiagnostico).State = EntityState.Modified;
                _db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(categoriaDiagnostico);
        }

        // GET: CategoriasDiagnostico/Delete/5
        [Authorize(Roles = "Eliminar CategoriaDiagnostico,IT")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CategoriaDiagnostico categoriaDiagnostico = _db.CategoriaDiagnosticos.Find(id);
            if (categoriaDiagnostico == null)
            {
                return HttpNotFound();
            }
            return View(categoriaDiagnostico);
        }

        // POST: CategoriasDiagnostico/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Eliminar CategoriaDiagnostico,IT")]
        public ActionResult DeleteConfirmed(int id)
        {
            CategoriaDiagnostico categoriaDiagnostico = _db.CategoriaDiagnosticos.Find(id);
            _db.CategoriaDiagnosticos.Remove(categoriaDiagnostico);
            _db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
