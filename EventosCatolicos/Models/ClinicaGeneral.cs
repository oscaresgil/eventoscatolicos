﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace EventosCatolicos.Models
{
    [Table("ClinicaGeneral")]
    public class ClinicaGeneral
    {
        [Display(Name = "Código")]
        public int Id { get; set; }
        
        [Display(Name = "Nombre")]
        public string Nombre { get; set; }

        [Display(Name = "Dirección")]
        public string Direccion { get; set; }

        [Display(Name = "Teléfono")]
        public string Telefono { get; set; }

        [Display(Name = "Correo Electrónico")]
        [EmailAddress(ErrorMessage = "Ingrese un formato de correo.")]
        public string CorreoElectronico { get; set; }
    }
}