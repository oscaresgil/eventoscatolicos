﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventosCatolicos.Servicios
{
    public class SelectorModel
    {
        public string Label { get; set; }
        public int Value { get; set; }
     
    }
}
