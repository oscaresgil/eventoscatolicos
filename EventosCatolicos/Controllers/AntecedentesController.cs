﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using EventosCatolicos.Models;

namespace EventosCatolicos.Controllers
{
    public class AntecedentesController : Controller
    {
        private readonly ApplicationDbContext _db = new ApplicationDbContext();

        // GET: Antecedentes
         [Authorize(Roles = "Ver Antecedentes,IT")]
        public ActionResult Index()
        {
            var antecedentes = _db.Antecedentes.Include(a => a.CategoriaAntecedente);
            return View(antecedentes.ToList());
        }

        // GET: Antecedentes/Details/5
         [Authorize(Roles = "Ver Antecedentes,IT")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Antecedente antecedente = _db.Antecedentes.Find(id);
            if (antecedente == null)
            {
                return HttpNotFound();
            }
            return View(antecedente);
        }

        // GET: Antecedentes/Create
         [Authorize(Roles = "Administrar Antecedentes,IT")]
        public ActionResult Create()
        {
            ViewBag.CategoriaId = new SelectList(_db.CategoriaAntecedentes, "Id", "Nombre");
            return View();
        }

        // POST: Antecedentes/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrar Antecedentes,IT")]
        public ActionResult Create([Bind(Include = "Id,Nombre,Descripcion,CategoriaId")] Antecedente antecedente)
        {
            if (ModelState.IsValid)
            {
                _db.Antecedentes.Add(antecedente);
                _db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CategoriaId = new SelectList(_db.CategoriaAntecedentes, "Id", "Nombre", antecedente.CategoriaId);
            return View(antecedente);
        }

        // GET: Antecedentes/Edit/5
         [Authorize(Roles = "Administrar Antecedentes,IT")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Antecedente antecedente = _db.Antecedentes.Find(id);
            if (antecedente == null)
            {
                return HttpNotFound();
            }
            ViewBag.CategoriaId = new SelectList(_db.CategoriaAntecedentes, "Id", "Nombre", antecedente.CategoriaId);
            return View(antecedente);
        }

        // POST: Antecedentes/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrar Antecedentes,IT")]
        public ActionResult Edit([Bind(Include = "Id,Nombre,Descripcion,CategoriaId")] Antecedente antecedente)
        {
            if (ModelState.IsValid)
            {
                _db.Entry(antecedente).State = EntityState.Modified;
                _db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CategoriaId = new SelectList(_db.CategoriaAntecedentes, "Id", "Nombre", antecedente.CategoriaId);
            return View(antecedente);
        }

        // GET: Antecedentes/Delete/5
         [Authorize(Roles = "Eliminar Antecedentes,IT")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Antecedente antecedente = _db.Antecedentes.Find(id);
            if (antecedente == null)
            {
                return HttpNotFound();
            }
            return View(antecedente);
        }

        // POST: Antecedentes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Eliminar Antecedentes,IT")]
        public ActionResult DeleteConfirmed(int id)
        {
            Antecedente antecedente = _db.Antecedentes.Find(id);
            _db.Antecedentes.Remove(antecedente);
            _db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
