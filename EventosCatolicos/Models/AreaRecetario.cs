// ReSharper disable VirtualMemberCallInContructor
namespace EventosCatolicos.Models
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("AreaRecetario")]
    public class AreaRecetario
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public AreaRecetario()
        {
            Recetarios = new HashSet<Recetario>();
        }
        [Display(Name = "C�digo")]
        public int Id { get; set; }

        [Required(ErrorMessage = "Por favor ingrese un nombre.")]
        [StringLength(256, ErrorMessage = "Sobrepas� el tama�o m�ximo de caracteres.")]
        public string Nombre { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Recetario> Recetarios { get; set; }
    }
}
