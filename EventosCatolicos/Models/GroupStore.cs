﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace EventosCatolicos.Models
{
    public class GroupStoreBase<TGroup>
        where TGroup : class 
    {
        public DbContext Context { get; private set; }
        public DbSet<TGroup> DbEntitySet { get; private set; }


        public IQueryable<TGroup> EntitySet
        {
            get
            {
                return DbEntitySet;
            }
        }


        public GroupStoreBase(DbContext context)
        {
            Context = context;
            DbEntitySet = context.Set<TGroup>();
        }


        public void Create(TGroup entity)
        {
            DbEntitySet.Add(entity);
        }


        public void Delete(TGroup entity)
        {
            DbEntitySet.Remove(entity);
        }


        public virtual Task<TGroup> GetByIdAsync(object id)
        {
            return DbEntitySet.FindAsync(id);
        }


        public virtual TGroup GetById(object id)
        {
            return DbEntitySet.Find(id);
        }


        public virtual void Update(TGroup entity)
        {
            if (entity != null)
            {
                Context.Entry(entity).State = EntityState.Modified;
            }
        }
    }

    public class ApplicationGroupStore : IDisposable
    {
        private bool _disposed;
        private GroupStoreBase<ApplicationGroup> _groupStore;


        public ApplicationGroupStore(DbContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException("context");
            }
            Context = context;
            _groupStore = new GroupStoreBase<ApplicationGroup>(context);
        }


        public IQueryable<ApplicationGroup> Groups
        {
            get
            {
                return _groupStore.EntitySet;
            }
        }

        public DbContext Context
        {
            get;
            private set;
        }


        public virtual void Create(ApplicationGroup group)
        {
            ThrowIfDisposed();
            if (group == null)
            {
                throw new ArgumentNullException("group");
            }
            _groupStore.Create(group);
            Context.SaveChanges();
        }


        public virtual async Task CreateAsync(ApplicationGroup group)
        {
            ThrowIfDisposed();
            if (group == null)
            {
                throw new ArgumentNullException("group");
            }
            _groupStore.Create(group);
            await Context.SaveChangesAsync();
        }


        public virtual async Task DeleteAsync(ApplicationGroup group)
        {
            ThrowIfDisposed();
            if (group == null)
            {
                throw new ArgumentNullException("group");
            }
            _groupStore.Delete(group);
            await Context.SaveChangesAsync();
        }


        public virtual void Delete(ApplicationGroup group)
        {
            ThrowIfDisposed();
            if (group == null)
            {
                throw new ArgumentNullException("group");
            }
            _groupStore.Delete(group);
            Context.SaveChanges();
        }


        public Task<ApplicationGroup> FindByIdAsync(string roleId)
        {
            ThrowIfDisposed();
            return _groupStore.GetByIdAsync(roleId);
        }


        public ApplicationGroup FindById(string roleId)
        {
            ThrowIfDisposed();
            return _groupStore.GetById(roleId);
        }


        public Task<ApplicationGroup> FindByNameAsync(string groupName)
        {
            ThrowIfDisposed();
            return _groupStore.EntitySet
                .FirstOrDefaultAsync(u => String.Equals(u.Name, groupName, StringComparison.CurrentCultureIgnoreCase));
        }


        public virtual async Task UpdateAsync(ApplicationGroup group)
        {
            ThrowIfDisposed();
            if (group == null)
            {
                throw new ArgumentNullException("group");
            }
            _groupStore.Update(group);
            await Context.SaveChangesAsync();
        }


        public virtual void Update(ApplicationGroup group)
        {
            ThrowIfDisposed();
            if (group == null)
            {
                throw new ArgumentNullException("group");
            }
            _groupStore.Update(group);
            Context.SaveChanges();
        }


        // DISPOSE STUFF: ===============================================

        public bool DisposeContext
        {
            get;
            set;
        }


        private void ThrowIfDisposed()
        {
            if (_disposed)
            {
                throw new ObjectDisposedException(GetType().Name);
            }
        }


        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }


        protected virtual void Dispose(bool disposing)
        {
            if (DisposeContext && disposing && Context != null)
            {
                Context.Dispose();
            }
            _disposed = true;
            Context = null;
            _groupStore = null;
        }
    }
}