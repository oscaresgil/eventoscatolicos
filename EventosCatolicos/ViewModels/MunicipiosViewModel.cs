﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EventosCatolicos.ViewModels
{
    public class MunicipioViewModel
    {
        public int Id { get; set; }

        [Required]
        [StringLength(256)]
        public string Nombre { get; set; }

    }

    public class MunicipioInfoViewModel : MunicipioViewModel
    {
        [Required]
        public string Departamento { get; set; }
    }

    public class MunicipioModifyViewModel : MunicipioViewModel
    {
        public int DepartamentoId { get; set; }
    }
}